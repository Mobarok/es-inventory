<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Auth::routes(['verify' => true]);

// Admin Routes
Route::get('admin', function(){
    return redirect()->route('admin.signin');
});

Route::get('/', ['as' => 'admin.signin', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::get('/verify/registration/{id}', ['as' => 'verify.registration', 'uses' => 'Auth\VerificationController@verifyUser']);
Route::post('login', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
Route::post('set_password', ['as' => 'password.set', 'uses' => 'Auth\SetPasswordController@set_password']);


Route::post('item-details', ['as' => 'search.item_details', 'uses' => 'SearchController@item_details']);
Route::post('sale-item-details', ['as' => 'search.sale_item_details', 'uses' => 'SearchController@sale_item_details']);
Route::post('sale-bags', ['as' => 'search.sale_bags', 'uses' => 'SearchController@sale_bags']);
Route::post('supplier-info', ['as' => 'search.supplier_info', 'uses' => 'SearchController@supplier_info']);
Route::post('customer-info', ['as' => 'search.customer_info', 'uses' => 'SearchController@customer_info']);

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'as' => 'admin.'
], function () {

    Route::group([
        'middleware' => ['auth', 'role:developer|admin|super-admin'],
    ], function () {

        Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
        Route::resource('memberships','MembershipController');
        Route::resource('roles','RoleController');
        Route::resource('permissions','PermissionController');

        Route::resource('payment_methods','PaymentMethodController');

        Route::resource('members','MemberController');
        Route::resource('users','UserController');
        Route::resource('units','UnitController');

    });
});


Route::group([
    'prefix' => 'member',
    'namespace' => 'Member',
    'as' => 'member.'
], function () {

    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);

    Route::group([
        'middleware' => ['auth', 'role:master-member|developer|admin|super-admin'],
    ], function () {

        Route::resource('settings','SettingController');
        Route::resource('users','UserController');
        Route::get('set-users-company', ['as' => 'users.set_users_company', 'uses' => 'UserController@set_users_company']);
        Route::post('set-users-company', ['as' => 'users.save_users_company', 'uses' => 'UserController@save_users_company']);
        Route::get('set-users-branch', ['as' => 'users.set_users_branch', 'uses' => 'UserController@set_users_branch']);
        Route::post('set-users-branch', ['as' => 'users.save_users_branch', 'uses' => 'UserController@save_users_branch']);
        Route::get('general-settings', ['as' => 'settings.general_settings', 'uses' => 'SettingController@general_settings']);
        Route::post('set-print-page-setup', ['as' => 'settings.set_print_page_setup', 'uses' => 'SettingController@set_print_page_setup']);

        Route::resource('branch','BranchController');
        Route::resource('categories','CategoryController');

    });
//
//    Route::group([
//        'middleware' => ['auth', 'role:user|accountant|master-member|developer|admin|super-admin'],
//    ], function () {


        Route::resource('items','ItemController');

        Route::get('print-barcode-form', ['as' => 'items.print_barcode_form', 'uses' => 'ItemController@print_barcode_form']);
        Route::get('print-barcode', ['as' => 'items.print_barcode', 'uses' => 'ItemController@print_barcode']);

        Route::resource('cash_or_bank_accounts','CashBankAccountController');
        Route::resource('company','CompanyController');

        Route::resource('sharer','SharerController');
        Route::get('sharer/create/{type}', ['as' => 'sharer.create', 'uses' => 'SharerController@create']);
        Route::get('customer-list', ['as' => 'sharer.customer_list', 'uses' => 'SharerController@customer_list']);
        Route::get('supplier-list', ['as' => 'sharer.supplier_list', 'uses' => 'SharerController@supplier_list']);

        Route::post('account-type-save', ['as' => 'account_type.save', 'uses' => 'CommonController@saveAccountType']);
        Route::post('customer-save', ['as' => 'customer.save', 'uses' => 'CommonController@saveCustomer']);
        Route::post('payer-search', ['as' => 'payer.search', 'uses' => 'CommonController@payerSearch']);

        Route::get('report/list', ['as' => 'report.list', 'uses' => 'ReportController@report_list']);
        Route::get('report/daily-stocks', ['as' => 'report.daily_stocks', 'uses' => 'ReportController@daily_stocks']);
        Route::get('report/stocks', ['as' => 'report.stocks', 'uses' => 'ReportController@stocks']);
        Route::get('report/total_stocks', ['as' => 'report.total_stocks', 'uses' => 'ReportController@total_stocks']);
        Route::get('report/cost/{type}', ['as' => 'report.cost', 'uses' => 'ReportController@cost_report']);
        Route::get('report/daily-sheet', ['as' => 'report.daily_sheet', 'uses' => 'ReportController@daily_sheet']);
        Route::get('report/sharer-due/{type}', ['as' => 'report.sharer_due', 'uses' => 'ReportController@sharer_due_report']);
        Route::get('report/sharer-due-collection/{type}', ['as' => 'report.sharer_due_collection', 'uses' => 'ReportController@sharer_due_collection_report']);
        Route::get('report/inventory-due/{type}', ['as' => 'report.inventory_due', 'uses' => 'ReportController@inventory_due_report']);
        Route::get('report/inventory/{type}', ['as' => 'report.inventory', 'uses' => 'ReportController@inventory_report']);
        Route::get('report/balance-sheet', ['as' => 'report.balance_sheet', 'uses' => 'ReportController@balance_sheet']);
        Route::get('report/product-purchase', ['as' => 'report.product_purchase_report', 'uses' => 'ReportController@product_purchase_report']);
        Route::get('report/supplier-purchase', ['as' => 'report.supplier_purchase', 'uses' => 'ReportController@supplier_purchase_report']);
        Route::get('report/sale', ['as' => 'report.sale', 'uses' => 'ReportController@sale_report']);
        Route::get('report/purchase', ['as' => 'report.purchase', 'uses' => 'ReportController@purchase_report']);

        Route::resource('purchase','PurchaseController');
        Route::get('purchase-due-list', [ 'as'=>'purchase.due_list','uses'=>'PurchaseController@due_list']);
        Route::get('purchase-due-payment/{id}', [ 'as'=>'purchase.due_payment','uses'=>'PurchaseController@due_payment']);
        Route::post('purchase-due-receive/{id}', [ 'as'=>'purchase.receive_due_payment','uses'=>'PurchaseController@receive_due_payment']);
        Route::resource('purchase_return','PurchaseReturnController');
        Route::get('purchase_return/view_returns/{id}/{code}', [ 'as'=>'purchase_return.view_returns','uses'=>'PurchaseReturnController@view_returns']);


        Route::resource('sales','SalesController');
        Route::get('sales-due-list', [ 'as'=>'sales.due_list','uses'=>'SalesController@due_list']);
        Route::get('sales-return-list', [ 'as'=>'sales.sales_return_list','uses'=>'SalesController@sales_return_list']);
        Route::get('sales-return/{id}', [ 'as'=>'sales.sales_return','uses'=>'SalesController@sales_return']);
        Route::put('sales-return-update/{id}', [ 'as'=>'sales.sales_return_update','uses'=>'SalesController@sales_return_update']);
        Route::get('sales/view_return/{id}/{code}', [ 'as'=>'sales.view_return','uses'=>'SalesController@sale_return_view']);
        Route::get('sales-due-payment/{id}', [ 'as'=>'sales.due_payment','uses'=>'SalesController@due_payment']);
        Route::post('sales-due-receive/{id}', [ 'as'=>'sales.receive_due_payment','uses'=>'SalesController@receive_due_payment']);
        Route::get('print-sale/{id}', [ 'as'=>'sales.print_sale','uses'=>'SalesController@print_sale']);

//    });

});

