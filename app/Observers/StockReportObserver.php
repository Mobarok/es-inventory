<?php

namespace App\Observers;

use App\Models\StockReport;
use Illuminate\Support\Facades\Auth;

class StockReportObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param  \App\Models\StockReport  $stock
     * @return void
     */
    public function creating(StockReport $stock)
    {
        if (Auth::check()) {
            $stock->member_id = Auth::user()->member_id;
            $stock->company_id = Auth::user()->company_id;
            $stock->branch_id = Auth::user()->branch_id;
        }
    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\Models\StockReport  $stock
     * @return void
     */
    public function updating(StockReport $stock)
    {

    }
}
