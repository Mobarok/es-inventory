<?php

namespace App\Observers;

use App\Models\PurchaseDetail;
use Illuminate\Support\Facades\Auth;

class PurchaseDetailsObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param  \App\Models\PurchaseDetail  $purchase
     * @return void
     */
    public function creating(PurchaseDetail $purchase)
    {
        if (Auth::check()) {
            $purchase->branch_id = Auth::user()->branch_id;
        }
    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\Models\PurchaseDetail  $purchase
     * @return void
     */
    public function updating(PurchaseDetail $purchase)
    {

    }
}
