<?php

namespace App\Observers;

use App\Models\SaleDetails;
use Illuminate\Support\Facades\Auth;

class SaleDetailsObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param  \App\Models\SaleDetails  $sale
     * @return void
     */
    public function creating(SaleDetails $sale)
    {
        if (Auth::check()) {
            $sale->branch_id = Auth::user()->branch_id;
        }
    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\Models\SaleDetails  $sale
     * @return void
     */
    public function updating(SaleDetails $sale)
    {

    }
}
