<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SupplierOrCustomer extends Model
{
    protected $table = 'suppliers_or_customers';

    // Must be Assignable
    protected $fillable = [
        'name', 'phone', 'customer_type', 'status' , 'sale_limit', 'purchase_limit',
        'balance_limit', 'address', 'email', 'cash_or_bank_id', 'account_head_id'
    ];

    // Not Required Fill
    protected $guarded = [ 'initial_balance', 'current_balance'];

    protected $appends = ['format_initial_balance', 'format_current_balance', 'name_phone'];


    public function getNamePhoneAttribute()
    {
        return $this->name." (".$this->phone.")";
    }
    /**
     * Scope a query to only include active models
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }


    /**
     * Get the Initial balance Format.
     *
     * @return string
     */
    public function getFormatInitialBalanceAttribute()
    {
        return create_money_format($this->initial_balance);
    }

    /**
     * Get the Current balance Format.
     *
     * @return string
     */
    public function getFormatCurrentBalanceAttribute()
    {
        return create_money_format($this->current_balance);
    }

    /**
     * Scope a query to only include Supplier models
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOnlySuppliers($query)
    {
        return $query->where('customer_type', '!=', 'customer');
    }

    /**
     * Scope a query to only include Customer models
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOnlyCustomers($query)
    {
        return $query->where('customer_type', '!=', 'supplier');
    }


    /**
     * Scope a query to only include Customer models
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAuthMember($query)
    {
        return $query->where('member_id', Auth::user()->member_id);
    }

    /**
     * Get the Created by User.
     */
    public function created_by()
    {
        return $this->belongsTo(User::class, 'id');
    }

    /**
     * Get the Updated by User.
     */
    public function updated_by()
    {
        return $this->belongsTo(User::class, 'id');
    }

    /**
     * Get the member.
     */
    public function member()
    {
        return $this->belongsTo(Member::class);

    }
}
