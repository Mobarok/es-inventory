<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Company extends Model
{
	protected $fillable = [
		'company_name', 'fiscal_year_id', 'logo', 'phone', 'address',
        'email', 'member_id', 'country_id', 'city', 'status'
	];

	protected $appends = ['company_logo_path'];

	public function fiscal_year()
	{
		return $this->belongsTo(FiscalYear::class, 'fiscal_year_id');
	}

    public function user()
    {
        return $this->belongsToMany(User::class);
    }
    /**
     * Scope a query to only include active models
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }

    /**
     * Get the Company Logo path.
     *
     * @return string
     */
    public function getCompanyLogoPathAttribute()
    {
        return asset('public/storage/company_logo/'. $this->logo);
    }

    /**
     * Get the country.
     */
    public function country()
    {
    	return $this->belongsTo(Country::class);
    }

    public function scopeAuthMember($query)
    {
    	return $query->where('member_id', Auth::user()->member_id);
    }
}
