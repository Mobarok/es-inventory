<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryType extends Model
{
    protected $fillable = [
        'display_name', 'name', 'status', 'member_id', 'company_id'
    ];


    /**
     * Get the members.
     */
    public function member()
    {
        return $this->belongsTo(Member::class);
    }


    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Scope a query to only include active models
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }
}
