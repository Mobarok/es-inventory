<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaleReturn extends Model
{
    protected $table = 'sales_return';

    protected $fillable = [
        'sale_id', 'item_id', 'qty', 'unit', 'price','return_qty', 'return_price', 'return_code', 'description',
        'notation', 'branch_id'
    ];


    public function sales()
    {
        return $this->hasMany(Sale::class, 'sale_id');
    }

    public function item()
    {
        return $this->hasOne( Item::class, 'id', 'item_id');
    }
}
