<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GLAccountClass extends Model
{
    protected $table = 'gl_account_classes';

    protected $fillable = ['name', 'class_type', 'status'];

    /**
     * Scope a query to only include active models
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }
}
