<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Purchase extends Model
{
    protected $fillable = [
        'date','chalan','memo_no','total_price','due_amount','advance_amount','paid_amount',
        'total_discount','amt_to_pay','cash_or_bank_id','supplier_id','transport_cost',
        'unload_cost','discount_type','discount','total_amount','notation','file',
        'payment_method_id','invoice_no', 'vehicle_number', 'file', 'branch_id'
    ];
    protected $guarded = [];
    protected $appends = ['date_format','attach_file_path'];

    public function getDateFormatAttribute()
    {
        return db_date_month_year_format($this->date);
    }


    public function supplier()
    {
        return $this->hasOne(SupplierOrCustomer::class, 'id','supplier_id');
    }

    public function cash_or_bank()
    {
        return $this->hasOne(CashOrBankAccount::class, 'id','cash_or_bank_id');
    }

    public function payment_method()
    {
        return $this->hasOne(PaymentMethod::class, 'id','payment_method_id');
    }

    public function purchase_details()
    {
        return $this->hasMany(PurchaseDetail::class, 'purchase_id');
    }

    public function purchase_returns()
    {
        return $this->hasMany( ReturnPurchase::class, 'purchase_id');
    }

    /**
     * Scope a query to only Member
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAuthMember($query)
    {
        return $query->where('member_id', Auth::user()->member_id);
    }

    /**
     * Scope a query to only Company
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAuthCompany($query)
    {
        return $query->where('company_id', Auth::user()->company_id);
    }

    public function scopeAuthUser($query)
    {
         if(Auth::user()->hasRole(['user'])){

            $query = $query->where('branch_id', Auth::user()->branch_id);
        }

        return $query;
    }
    /**
     * Get the Company Logo path.
     *
     * @return string
     */
    public function getAttachFilePathAttribute()
    {
        return asset('public/storage/file/'. $this->file);
    }

}
