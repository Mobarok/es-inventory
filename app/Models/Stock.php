<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = ['item_id', 'stock', 'branch_id'];


    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id','id');
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id','id');
    }
}
