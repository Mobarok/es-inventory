<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $fillable = ['name', 'display_name', 'status'];

    protected $guarded = ['description'];

    /**
     * Scope a query to only include active models
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }

    public function scopeFilter($query)
    {
       return $query->where('name','!=', 'super-admin')
                    ->where('name','!=', 'developer')
                    ->where('name','!=', 'master-member');
    }
}
