<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseDetail extends Model
{
    protected $guarded = [];
    protected $appends = ['created_date'];

    public function purchases()
    {
        return $this->hasOne(Purchase::class, 'id', 'purchase_id');
    }

    public function item()
    {
        return $this->hasOne( Item::class, 'id', 'item_id');
    }

    public function getCreatedDateAttribute()
    {
        return db_date_month_year_format($this->created_at);
    }

}
