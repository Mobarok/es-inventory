<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrackShoppingBags extends Model
{
    protected $table = 'track_shopping_bags';

    protected $guarded = [];

    public function sale()
    {
        return $this->hasMany(Sale::class, 'id', 'sale_id');
    }
}
