<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReturnPurchase extends Model
{
    protected $fillable = [
        'purchase_id', 'item_id', 'qty', 'unit', 'price', 'fine_price', 'return_code', 'return_qty',
        'return_price', 'description'
    ];


    public function purchases()
    {
        return $this->hasMany(Purchase::class, 'purchase_id');
    }

    public function item()
    {
        return $this->hasOne( Item::class, 'id', 'item_id');
    }

}
