<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CashOrBankAccount extends Model
{
    protected $fillable = [
        'title', 'phone', 'contact_person', 'member_id', 'created_by', 'status', 'account_type_id'
    ];

    protected $guarded = [
       'bank_charge_account_id', 'description', 'internet_banking_url', 'account_number', 'initial_balance', 'updated_by'
    ];

    protected $appends = ['format_initial_balance', 'format_current_balance'];

    /**
     * Scope a query to Auth Member
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAuthMember($query)
    {
        return $query->where('member_id', Auth::user()->member_id);
    }

    /**
     * Scope a query to only include active models
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }

    /**
     * Get the Initial balance Format.
     *
     * @return string
     */
    public function getFormatInitialBalanceAttribute()
    {
        return create_money_format($this->initial_balance);
    }

    /**
     * Get the Current balance Format.
     *
     * @return string
     */
    public function getFormatCurrentBalanceAttribute()
    {
        return create_money_format($this->current_balance);
    }

    /**
     * Relation with Account Type
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account_type()
    {
        return $this->belongsTo(AccountType::class,'account_type_id');
    }

    /**
     * Relation with Account Type
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bank_charge()
    {
        return $this->belongsTo(AccountType::class,'account_type_id');
    }
}
