<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AccountType extends Model
{
    protected $fillable= ['display_name', 'name', 'parent_id', 'class_id'];

    protected $appends = ['account_code', 'account_code_name'];


    public function getAccountCodeNameAttribute()
    {
        return $this->account_code." ".$this->display_name;
    }

    public function getAccountCodeAttribute()
    {
        return format_number_digit($this->id);
    }

    public function parent_name(){

        return $this->belongsTo(AccountType::class, 'parent_id');
    }

    public function gl_class(){

        return $this->belongsTo(GLAccountClass::class, 'class_id');
    }

    /**
     * Scope a query to only Member
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAuthMember($query)
    {
        return $query->where('member_id', Auth::user()->member_id);
    }

    /**
     * Scope a query to only include active models
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }

    /**
     * Account Group Find
     * @param $query
     * @return mixed
     */
    public function scopeGroup($query)
    {
        return $query->whereNull('parent_id');
    }


}
