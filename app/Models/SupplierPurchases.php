<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SupplierPurchases extends Model
{
    protected $table = 'supplier_purchases';

    protected $fillable = [ 'item_id', 'qty', 'member_id', 'company_id', 'supplier_id' ];


    public function scopeAuthMember($query)
    {
        return $query->where('member_id', Auth::user()->member_id);
    }


    public function scopeAuthCompany($query)
    {
        return $query->where('company_id', Auth::user()->company_id);
    }

    public function items()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }

    public function member()
    {
        return $this->belongsTo(Member::class,'member_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class,'company_id');
    }


    public function supplier()
    {
        return $this->belongsTo(SupplierOrCustomer::class, 'supplier_id');
    }
}
