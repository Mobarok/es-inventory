<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentDetail extends Model
{
    protected $fillable = ['transaction_details_id'];

    protected $guarded = [
        'number', 'date', 'pass_date', 'provide_date', 'issuer_name', 'email', 'receiver_name', 'short_description'
    ];
}
