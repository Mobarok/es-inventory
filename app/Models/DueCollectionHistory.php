<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DueCollectionHistory extends Model
{
    protected $table = 'due_collection_histories';

    protected $guarded = [];

    protected $appends = ['date_format'];

    public function sale()
    {
        return $this->hasMany(Sale::class, 'inventory_type_id');
    }

    public function purchase()
    {
        return $this->hasMany(Purchase::class, 'inventory_type_id');
    }

    public function getDateFormatAttribute()
    {
        return db_date_month_year_format($this->collection_date);
    }

    public function sharer()
    {
        return $this->hasOne(SupplierOrCustomer::class, 'id','sharer_id');
    }
}
