<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaleDetails extends Model
{
    protected $table = 'sales_details';

    protected $guarded = [];

    public function sale()
    {
        return $this->hasMany(Sale::class, 'sale_id');
    }

    public function item()
    {
        return $this->hasOne( Item::class, 'id', 'item_id');
    }
}
