<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Item extends Model
{
    protected $fillable = [
      'item_name', 'unit', 'product_image', 'category_id', 'description', 'status', 'price', 'guarantee', 'warranty',
        'skuCode', 'productCode'
    ];


    /**
     * Scope a query to only include active models
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'id','category_id');
    }

    public function branch()
    {
        return $this->hasOne(Branch::class, 'id','branch_id');
    }

    protected $appends = ['product_image_path'];

    /**
     * Get the product image path.
     *
     * @return string
     */
    public function getProductImagePathAttribute()
    {
        return asset('public/storage/product_image/'. $this->product_image);
    }

    public function stock_details(){

        return $this->hasOne(Stock::class);
    }

    public function sale(){

        return $this->hasMany(SaleDetails::class);
    }

    public function purchase(){

        return $this->hasMany(PurchaseDetail::class);
    }

    public function latest_purchase(){

        return $this->hasOne(PurchaseDetail::class)->latest();
    }

    public function scopeOrderDesc($query)
    {
        return $query->orderBy('item_name', 'DESC');
    }


    public function scopeOrderAsc($query)
    {
        return $query->orderBy('item_name', 'ASC');
    }


    /**
     * Scope a query to only Member
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAuthMember($query)
    {
        return $query->where('member_id', Auth::user()->member_id);
    }

    /**
     * Scope a query to only Company
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAuthCompany($query)
    {
        return $query->where('company_id', Auth::user()->company_id);
    }
}
