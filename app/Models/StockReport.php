<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockReport extends Model
{
    protected $table = 'stock_reports';

    protected $guarded = [];


    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id','id');
    }
}
