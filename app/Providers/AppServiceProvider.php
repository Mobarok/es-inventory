<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Http\Traits\ObserverTrait;

class AppServiceProvider extends ServiceProvider
{
    use ObserverTrait;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(255);
        $this->getObservers();

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {}
}
