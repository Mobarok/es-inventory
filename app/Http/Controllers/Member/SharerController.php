<?php

namespace App\Http\Controllers\Member;

use App\DataTables\CustomersDataTable;
use App\DataTables\SuppliersDataTable;
use App\Models\AccountType;
use App\Models\CashOrBankAccount;
use App\Models\SupplierOrCustomer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SharerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of the Customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function customer_list( CustomersDataTable $dataTable)
    {
        return $dataTable->render('member.suppliers_or_customers.customers');
    }

    /**
     * Display a listing of the Supplier.
     *
     * @return \Illuminate\Http\Response
     */
    public function supplier_list( SuppliersDataTable $dataTable)
    {
        return $dataTable->render('member.suppliers_or_customers.suppliers');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( $type )
    {
        $data['sharer_type'] = ucfirst($type);

        return view('member.suppliers_or_customers.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->getValidationRules());
//        print_r($request->all()); exit;
        $inputs = $request->all();
        $type = $request->customer_type;
        $inputs['customer_type'] = strtolower($type);
        $request->both == 1 ? $inputs['customer_type'] = 'both' : '';

        DB::beginTransaction();
        try{

            $sharer = SupplierOrCustomer::create($inputs);
            $sharer->name = $request->name;
            $sharer->phone = $request->phone;
            $sharer->email = $request->email;
            $sharer->address = $request->address;

            if($type=="customer")
                $sharer->customer_initial_balance = $request->initial_balance;
            elseif($type=="supplier")
                $sharer->supplier_initial_balance = $request->initial_balance;
            else{
                $sharer->customer_initial_balance = $request->initial_balance;
                $sharer->supplier_initial_balance = $request->initial_balance;
            }


            if($request->account_head)
            {
                $this->set_account_head($inputs);
            }

            if($request->cash_bank)
            {
                $this->set_cash_or_bank($inputs);
            }

            $sharer->save();
            $status = ['type' => 'success', 'message' => $type.' Added Successfully'];

        }catch (\Exception $e){

            $status = ['type' => 'danger', 'message' => 'Unable to Add '.$type];
            DB::rollBack();
        }

        DB::commit();

        return back()->with('status', $status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['model'] = $sharer = SupplierOrCustomer::find($id);

        if(!$sharer)
        {
            $status = ['type' => 'danger', 'message' => 'Don\'t have any data'];
            return back()->with('status', $status);
        }
        $data['sharer_type'] = ucfirst($sharer->customer_type);
        return view('member.suppliers_or_customers.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->getValidationRules());

        $type = $request->customer_type;

        $sharer =  SupplierOrCustomer::find($id);
        $inputs = $request->all();

        $sharer->name = $request->name;
        $sharer->phone = $request->phone;
        $sharer->email = $request->email;
        $sharer->address = $request->address;
        $sharer->status = $request->status;

        ($request->customer == 1 && $request->supplier == 1) ? $sharer->customer_type = 'both' : '';
        ($request->customer == 1 && $request->supplier == 0) ? $sharer->customer_type = 'customer' : '';
        ($request->customer == 0 && $request->supplier == 1) ? $sharer->customer_type = 'supplier' : '';

        DB::beginTransaction();
        try{

            $sharer->update($inputs);
            $sharer->save();
            $status = ['type' => 'success', 'message' => ' updated Successfully'];

        }catch (\Exception $e){

            $status = ['type' => 'danger', 'message' => 'Unable to Update '];
            DB::rollBack();
        }

        DB::commit();

        return back()->with('status', $status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getValidationRules($id='')
    {
        $rules = [
            'name' => 'required',
            'phone' => 'required',
            'customer_type' => 'required',
            'status' => 'required',
        ];

        return $rules;
    }


    private function set_cash_or_bank($inputs)
    {
        $account_type = AccountType::where('display_name', $inputs['name'])->first();
        $data = [];
        $data['title'] = $inputs['name'];
        $data['contact_person'] = $inputs['name'];
        $data['phone'] = $inputs['phone'];
        $data['account_type_id'] = $account_type->id;

        CashOrBankAccount::create($data);
    }

    private function set_account_head($inputs)
    {
        $data = [];
        $data['name'] = snake_case($inputs['name']);
        $data['display_name'] = $inputs['name'];

        AccountType::create($data);
    }


}
