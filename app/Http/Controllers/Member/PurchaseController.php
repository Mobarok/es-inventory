<?php

namespace App\Http\Controllers\Member;

use App\DataTables\PurchaseReturnDataTable;
use App\DataTables\PurchasesDataTable;
use App\Http\Traits\BranchTrait;
use App\Http\Traits\FileUploadTrait;
use App\Http\Traits\StockTrait;
use App\Http\Traits\TransactionTrait;
use App\Models\AccountType;
use App\Models\CashOrBankAccount;
use App\Models\DueCollectionHistory;
use App\Models\Item;
use App\Models\PaymentMethod;
use App\Models\Purchase;
use App\Models\PurchaseDetail;
use App\Models\ReturnPurchase;
use App\Models\Stock;
use App\Models\SupplierOrCustomer;
use App\Models\SupplierPurchases;
use App\Models\TransactionDetail;
use App\Models\Transactions;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Milon\Barcode\DNS1D;

class PurchaseController extends Controller
{
    use FileUploadTrait, TransactionTrait, StockTrait, BranchTrait;

    public function __construct()
    {
//        if( $this->branch_assign_check() == 0)
//        {
//
//            $status = [
//                'type' => 'danger',
//                'message' => 'You are not assign for any branch. please confirm branch'
//            ];
////            if(Auth::user()->hasRole(['admin','super-admin','developer','master-member'])) {
////                $route = 'admin';
////            }else{
//                $route = 'member';
////            }
//
//            return redirect()->route($route.'.dashboard')->with('status', $status)->send();
//        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PurchasesDataTable $dataTable)
    {
        return $dataTable->render('member.purchase.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lastId = Purchase::latest()->pluck('id')->first();
        $data['suppliers'] = SupplierOrCustomer::onlySuppliers()->latest()->pluck('name', 'id');
        $data['products'] = Item::latest()->pluck('item_name', 'id');
        $data['payment_methods'] = PaymentMethod::active()->get()->pluck('name', 'id');
        $data['banks'] = CashOrBankAccount::latest()->pluck('title', 'id');
        $data['memo_no'] = $data['chalan_no'] = memo_generate("MC-", $lastId+1);
        return view('member.purchase.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $purchase = [];
        $purchase['memo_no'] = $inputs['memo_no'];
        $purchase['chalan'] = $inputs['chalan'];
        $purchase['supplier_id'] = $inputs['supplier_id'];
        $purchase['cash_or_bank_id'] = $inputs['cash_or_bank_id'];
        $purchase['payment_method_id'] = $inputs['payment_method_id'];
        $purchase['date'] = db_date_format($inputs['date']);
        $purchase['total_price'] = $inputs['total_bill'];
        $purchase['transport_cost'] = $inputs['transport_cost'];
        $purchase['unload_cost'] = $inputs['unload_cost'];
        $purchase['discount_type'] = $inputs['discount_type'];
        $purchase['discount'] = $inputs['discount'];
        $purchase['total_discount'] = $inputs['discount'];
        $purchase['due_amount'] = 0;
        $purchase['advance_amount'] = 0;
        $purchase['paid_amount'] = $inputs['paid_amount'];
        $purchase['total_amount'] = $inputs['total_amount'];
        $purchase['amt_to_pay'] = $inputs['amount_to_pay'];
        $purchase['notation'] = $inputs['notation'];
        $purchase['invoice_no'] = $inputs['invoice_no'];
        $purchase['vehicle_number'] = $inputs['vehicle_number'];


        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $upload = $this->fileUpload($file, '/file/', null);

            if (!$upload)
            {
                $status = ['type' => 'danger', 'message' => 'Image Must be JPG, JPEG, PNG, GIF'];
                return back()->with('status', $status);
            }
            $purchase['file'] = $upload;

        }

//        DB::beginTransaction();
//        try{

            $purchaseId = Purchase::create($purchase);

        $purchaseDetails = [];
        $purchaseDetails['purchase_id'] = $purchaseId->id;

        $product = $request->product_id;
        $unit = $request->unit;
        $qty = $request->qty;
        $price = $request->price;
        $description = $request->description;
        $sales_price = $request->sales_price;
        $last_purchase_qty = $request->last_purchase_qty;
        $available_stock = $request->available_stock;

        $total_amount = 0;
        for($i=0; $i<count($product); $i++){

            if(!isset($product[$i]) || !isset($qty[$i]))
                break;


            if($qty[$i] < 1 || $price[$i] <1 )
            {
                break;
            }


            $item = Item::find($product[$i]);
            $purchaseDetails['item_id'] = $item_id = $product[$i];
            $purchaseDetails['unit'] = $item->unit;
            $purchaseDetails['qty'] = $quantity = $qty[$i];
            $purchaseDetails['price'] = $price[$i];
            $purchaseDetails['description'] = $description[$i];
            $purchaseDetails['sales_price'] = $sales_price[$i];
            $purchaseDetails['last_purchase_qty'] = $last_purchase_qty[$i];
            $purchaseDetails['available_stock'] = $available_stock[$i];
            if($item->warranty>0){
                $purchaseDetails['warranty'] = $item->warranty;
                $purchaseDetails['warranty_start_date'] = $today = Carbon::today();
                $purchaseDetails['warranty_end_date'] = $today->addMonths($item->warranty);
            }


            $total_price = $qty[$i]*$price[$i];
            $total_amount +=$total_price;

            PurchaseDetail::create($purchaseDetails);

            $data = [];
            $data['item_id'] = $item_id;
            $data['qty'] = $quantity;
            $data['supplier_id'] = $inputs['supplier_id'];
            SupplierPurchases::create($data);

            $this->stock_report($product[$i], $qty[$i], 'purchase');
            $this->stockIn($product[$i], $qty[$i]);
            $this->createStockHistory($product[$i], $qty[$i]);

        }


        $purchase = [];
        $purchase['total_price'] = $total_amount;
        $purchase['total_amount'] = $total_amount+$inputs['transport_cost']+$inputs['unload_cost'];

        if($inputs['discount_type']=="Fixed")
        {
          $discount = $inputs['discount'];
        }else{
            $discount = $total_amount*$inputs['discount']/100;
        }

        $purchase['paid_amount'] = $inputs['paid_amount'];
        $purchase['total_discount'] = $discount;
        $purchase['amt_to_pay'] = $purchase['total_amount']-$discount;
        $lastAmount = $purchase['amt_to_pay']-$inputs['paid_amount'];

        $purchase['advance_amount'] = $purchase['due_amount'] = 0;
        if($lastAmount<0)
            $purchase['advance_amount'] = -1*$lastAmount;
        else
            $purchase['due_amount'] = $lastAmount;

        $purchaseId->update($purchase);


//        $inputs['transaction_code'] = transaction_code_generate();
//
//        $save_transaction = new Transactions();
//        $save_transaction->transaction_code = $inputs['transaction_code'];
//        $save_transaction->supplier_id = $inputs['supplier_id'];
//        $save_transaction->purchase_id = $purchaseId->id;
//        $save_transaction->cash_or_bank_id = $inputs['cash_or_bank_id'];
//        $save_transaction->date = db_date_format($inputs['date']);
//        $save_transaction->amount = $purchase['amt_to_pay'];
//        $save_transaction->notation = $inputs['notation'];
//        $save_transaction->transaction_method = $inputs['transaction_method'] = "Purchases";
//        $save_transaction->save();
//
//
//        $account = CashOrBankAccount::find($request->cash_or_bank_id);
//
//        if(isset($request->supplier_id)) {
//            $sharer = SupplierOrCustomer::find($request->supplier_id);
//            $inputs['sharer_name'] = $sharer->name;
//        }else{
//            $inputs['sharer_name'] = '';
//        }
//
//        // Update Cash and Bank Account Balance
//        $this->bankAccountBalanceUpdate($inputs['transaction_method'], $account, $inputs['paid_amount']);
//
//
//        if(isset($request->supplier_id)) {
//            $this->sharerBalanceUpdate($inputs['transaction_method'], $sharer, $inputs['paid_amount']);
//        }
//
//
////        $account_type = AccountType::where('display_name', 'Expenses')->first();
//        $inputs['account_type_id'] = $account->id;
//        $inputs['account_name'] = $account->title;
//        $inputs['to_account_name'] = '';
//        $inputs['transaction_id'] = $save_transaction->id;
//        $inputs['amount'] = $inputs['paid_amount'];
//        $inputs['transaction_type'] = 'dr';
//        $inputs['description'] = "Purchase product"." Purchase Id : ".$purchaseId->id;
//        $this->createDebitAmount($inputs);
//
//        if($purchase['due_amount']>0)
//        {
//            $account_type = AccountType::where('display_name', 'Accounts Payable')->first();
//            $inputs['account_type_id'] = $account_type->id;
//            $inputs['account_name'] = $account->title;
//            $inputs['to_account_name'] = '';
//            $inputs['transaction_id'] = $save_transaction->id;
//            $inputs['amount'] = $purchase['due_amount'];
//            $inputs['transaction_type'] = 'dr';
//            $inputs['description'] = "Purchase product";
//            $this->createDebitAmount($inputs);
//        }
//
//        if($purchase['advance_amount']>0)
//        {
//            $account_type = AccountType::where('display_name', 'Accounts Receivable')->first();
//            $inputs['account_type_id'] = $account_type->id;
//            $inputs['account_name'] = $account->title;
//            $inputs['to_account_name'] = '';
//            $inputs['transaction_id'] = $save_transaction->id;
//            $inputs['amount'] = $purchase['advance_amount'];
//            $inputs['transaction_type'] = 'dr';
//            $inputs['description'] = "Purchase product";
//            $this->createDebitAmount($inputs);
//        }
//
//        $account_type = AccountType::where('display_name', 'Purchase')->first();
//        $inputs['account_type_id'] = $account_type->id;
//        $inputs['amount'] = $purchase['amt_to_pay'];
//        $inputs['transaction_type'] = 'cr';
//        $this->createCreditAmount($inputs);

        /*
         * TODO: Create Transaction History for Purchases
         */

        $status = ['type' => 'success', 'message' => 'Purchase done Successfully'];


//        }catch (\Exception $e){
//
//            $status = ['type' => 'danger', 'message' => 'Unable to save'];
//            DB::rollBack();
//        }
//
//        DB::commit();

        if($status['type'] == 'success')
        {
            return redirect()->route('member.purchase.show', $purchaseId->id)->with('status', $status);
        }else{

            return redirect()->back()->with('status', $status);
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['purchase'] = $purchase = Purchase::findorFail($id);

        $d = new DNS1D();
        $d->setStorPath(__DIR__."/cache/");
        $data['purchase_barcode'] = '<img src="data:image/png;base64,' . $d->getBarcodePNG($purchase->memo_no, "C128A", 2, 50) . '" alt="' . $purchase->memo_no . '"   />';

        return view('member.purchase.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['modal'] = $purchase = Purchase::find($id);
        $data['memo_no'] = $purchase->memo_no;
        $data['chalan_no'] = $purchase->chalan;
        $data['suppliers'] = SupplierOrCustomer::onlySuppliers()->latest()->pluck('name', 'id');
        $data['products'] = Item::latest()->pluck('item_name', 'id');
        $data['purchase_products'] = Item::whereNotIn('id', $purchase->purchase_details()->pluck('item_id')->toArray())->latest()->pluck('item_name', 'id');
        $data['payment_methods'] = PaymentMethod::active()->get()->pluck('name', 'id');
        $data['banks'] = CashOrBankAccount::latest()->pluck('title', 'id');

        return view('member.purchase.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $purchaseId = Purchase::find($id);

        $inputs = $request->all();

        $purchase = [];
//        $purchase['memo_no'] = $inputs['memo_no'];
//        $purchase['chalan'] = $inputs['chalan'];
        $purchase['supplier_id'] = $inputs['supplier_id'];
        $purchase['cash_or_bank_id'] = $inputs['cash_or_bank_id'];
        $purchase['payment_method_id'] = $inputs['payment_method_id'];
        $purchase['date'] = db_date_format($inputs['date']);
        $purchase['total_price'] = $inputs['total_bill'];
        $purchase['transport_cost'] = $inputs['transport_cost'];
        $purchase['unload_cost'] = $inputs['unload_cost'];
        $purchase['discount_type'] = $inputs['discount_type'];
        $purchase['discount'] = $inputs['discount'];
        $purchase['total_discount'] = $inputs['discount'];
        $purchase['due_amount'] = 0;
        $purchase['advance_amount'] = 0;
        $purchase['paid_amount'] = $inputs['paid_amount'];
        $purchase['total_amount'] = $inputs['total_amount'];
        $purchase['amt_to_pay'] = $inputs['amount_to_pay'];
        $purchase['notation'] = $inputs['notation'];
        $purchase['vehicle_number'] = $inputs['vehicle_number'];


        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $upload = $this->fileUpload($file, '/file/', null);

            if (!$upload)
            {
                $status = ['type' => 'danger', 'message' => 'Image Must be JPG, JPEG, PNG, GIF'];
                return back()->with('status', $status);
            }
            $purchase['file'] = $upload;

        }

        DB::beginTransaction();
        try{

        $purchaseId->update($purchase);
        $purchaseDetails = [];
        $purchaseDetails['purchase_id'] = $id;

        $product = $request->product_id;
        $unit = $request->unit;
        $qty = $request->qty;
        $price = $request->price;
        $description = $request->description;
        $sales_price = $request->sales_price;
        $available_stock = $request->available_stock;
        $last_purchase_qty = $request->last_purchase_qty;

        $purchase_details_id = $request->purchaseDetails;


        $deletePurchaseDetail = PurchaseDetail::where('purchase_id', $id)->whereNotIn('id', $purchase_details_id)->get();

//        dd($deletePurchaseDetail);
        foreach ($deletePurchaseDetail as $value)
        {
            $purchase_detail_id = PurchaseDetail::find($value->id);
            $this->stockOut($value->item_id, $purchase_detail_id->qty);
            $value->delete();
        }

        $purchaseDetails = [];
        $total_amount = 0;
        for($i=0; $i<count($product); $i++){
            $item = Item::find($product[$i]);

            if(!isset($product[$i]) || !isset($qty[$i]))
                break;


            if($qty[$i] < 1 || $price[$i] <1 )
            {
                break;
            }

            if($purchase_details_id[$i] != "new"){
                $purchase_detail_id = PurchaseDetail::find($purchase_details_id[$i]);
            }else{
                $purchaseDetails['purchase_id'] = $purchaseId->id;
            }

            $purchaseDetails['item_id'] = $item_id = $product[$i];
            $purchaseDetails['unit'] = $item->unit;
            $purchaseDetails['qty'] = $quantity = $qty[$i];
            $purchaseDetails['price'] = $price[$i];
            $purchaseDetails['description'] = $description[$i];
            $purchaseDetails['sales_price'] = $sales_price[$i];
            $purchaseDetails['available_stock'] = $available_stock[$i];
            $purchaseDetails['last_purchase_qty'] = $last_purchase_qty[$i];

            if($item->warranty>0){
                $purchaseDetails['warranty'] = $item->warranty;
                $purchaseDetails['warranty_start_date'] = $today = Carbon::today();
                $purchaseDetails['warranty_end_date'] = $today->addMonths($item->warranty);
            }

            $total_price = $qty[$i]*$price[$i];
            $total_amount +=$total_price;

            $data = [];
            $data['item_id'] = $item_id;
            $data['qty'] = $quantity;
            $data['supplier_id'] = $inputs['supplier_id'];
            SupplierPurchases::create($data);

            if($purchase_details_id[$i] != "new") {
                $this->stock_report($product[$i], $purchase_detail_id->qty, 'purchase return');
                $this->stockOut($product[$i], $purchase_detail_id->qty);
                $purchase_detail_id->update($purchaseDetails);
            }else{
                PurchaseDetail::create($purchaseDetails);
            }

            $this->stock_report($product[$i], $qty[$i], 'purchase');
            $this->stockIn($product[$i], $qty[$i]);
            $this->createStockHistory($product[$i], $qty[$i],'Purchase Edit');
        }


        $purchase = [];
        $purchase['total_price'] = $total_amount;
        $purchase['total_amount'] = $total_amount+$inputs['transport_cost']+$inputs['unload_cost'];

        if($inputs['discount_type']=="Fixed")
        {
            $discount = $inputs['discount'];
        }else{
            $discount = $total_amount*$inputs['discount']/100;
        }

        $purchase['paid_amount'] = $inputs['paid_amount'];
        $purchase['total_discount'] = $discount;
        $purchase['amt_to_pay'] = $purchase['total_amount']-$discount;

        $lastAmount = $purchase['amt_to_pay']-$inputs['paid_amount'];

        $purchase['advance_amount'] = $purchase['due_amount'] = 0;
        if($lastAmount<0)
            $purchase['advance_amount'] = -1*$lastAmount;
        else
            $purchase['due_amount']= $lastAmount;

        $purchaseId->update($purchase);

//        $save_transaction = Transactions::where('purchase_id', $purchaseId->id)->first();
//        $save_transaction->amount = $purchase['amt_to_pay'];
//        $save_transaction->notation = $inputs['notation'];
//        $save_transaction->supplier_id = $inputs['supplier_id'];
//        $save_transaction->cash_or_bank_id = $inputs['cash_or_bank_id'];
//        $save_transaction->save();
//        $inputs['transaction_method'] = "Purchases";
//
//        // Edit is not complete yet here to bottom
//
//        $account = CashOrBankAccount::find($request->cash_or_bank_id);
//
//        if(isset($request->supplier_id)) {
//            $sharer = SupplierOrCustomer::find($request->supplier_id);
//            $inputs['sharer_name'] = $sharer->name;
//        }else{
//            $inputs['sharer_name'] = '';
//        }
//
//        // Update Cash and Bank Account Balance
//        $this->bankAccountBalanceUpdate("Purchase Update", $account, $purchaseId->paid_amount);
//        $this->bankAccountBalanceUpdate($inputs['transaction_method'], $account, $inputs['paid_amount']);
//
//
//        if(isset($request->supplier_id)) {
//            $this->sharerBalanceUpdate("Purchase Update", $sharer, $purchaseId->paid_amount);
//            $this->sharerBalanceUpdate($inputs['transaction_method'], $sharer, $inputs['paid_amount']);
//        }
//
//
//        $account_type = AccountType::where('display_name', 'Expenses')->first();
//
//        $trans_details = TransactionDetail::where('transaction_id', $save_transaction->id)->get();
//        foreach ($trans_details as $value)
//        {
//            $value->delete();
//        }
//
//
//
//        $inputs['account_type_id'] = $account_type->id;
//        $inputs['account_name'] = $account->title;
//        $inputs['to_account_name'] = '';
//        $inputs['transaction_id'] = $save_transaction->id;
//        $inputs['amount'] = $inputs['paid_amount'];
//        $inputs['transaction_type'] = 'dr';
//        $inputs['description'] = "Purchase product";
//        $this->createDebitAmount($inputs);
//
//        if($purchase['due_amount']>0)
//        {
//            $account_type = AccountType::where('display_name', 'Accounts Payable')->first();
//            $inputs['account_type_id'] = $account_type->id;
//            $inputs['account_name'] = $account->title;
//            $inputs['to_account_name'] = '';
//            $inputs['transaction_id'] = $save_transaction->id;
//            $inputs['amount'] = $purchase['due_amount'];
//            $inputs['transaction_type'] = 'dr';
//            $inputs['description'] = "Purchase product";
//            $this->createDebitAmount($inputs);
//        }
//
//        if($purchase['advance_amount']>0)
//        {
//            $account_type = AccountType::where('display_name', 'Accounts Receivable')->first();
//            $inputs['account_type_id'] = $account_type->id;
//            $inputs['account_name'] = $account->title;
//            $inputs['to_account_name'] = '';
//            $inputs['transaction_id'] = $save_transaction->id;
//            $inputs['amount'] = $purchase['advance_amount'];
//            $inputs['transaction_type'] = 'dr';
//            $inputs['description'] = "Purchase product";
//            $this->createDebitAmount($inputs);
//        }
//
//        $account_type = AccountType::where('display_name', 'Cash')->first();
//        $inputs['account_type_id'] = $account_type->id;
//        $inputs['amount'] = $purchase['amt_to_pay'];
//        $inputs['transaction_type'] = 'cr';
//        $this->createCreditAmount($inputs);

        /*
         * TODO: Create Transaction History for Purchases
         */


        $status = ['type' => 'success', 'message' => 'Purchase update Successfully'];


        }catch (\Exception $e){

        $status = ['type' => 'danger', 'message' => 'Unable to update'];
        DB::rollBack();
        }

        DB::commit();

        if($status['type'] == 'success')
        {
            return redirect()->route('member.purchase.show', $purchaseId->id)->with('status', $status);
        }else{

            return redirect()->back()->with('status', $status);
        }
    }


    public function due_list()
    {
        $data['modal'] = Purchase::authMember()->authCompany()->where('due_amount', '>', 0)->paginate(15);

        return view('member.purchase.due_list', $data);
    }


    public function due_payment($id)
    {
        $purchase = $data['purchase'] = Purchase::where('id', $id)->where('due_amount','>',0)->first();
        if($purchase)
        {
            return view('member.purchase.due_payment', $data);
        }else{
            $status = [
                'type' => 'danger',
                'message' => "Purchase ID: ".$id.", This Purchase don't have any due"
            ];
            return redirect()->back()->with('status', $status);
        }

    }

    public function receive_due_payment(Request $request, $id)
    {
        $purchaseId = Purchase::find($id);

        if($purchaseId)
        {
            $amount = $purchaseId->due_amount;

            $save = new DueCollectionHistory();
            $inputs = [];
            $inputs['inventory_type'] = 'Purchase';
            $inputs['inventory_type_id'] = $purchaseId->id;
            $inputs['sharer_id'] = $purchaseId->supplier_id;
            $inputs['amount'] = $purchaseId->due_amount;
            $inputs['collection_date'] = Carbon::today();
            $save->create($inputs);

            $purchase = [];
            $purchase['due_amount'] = 0;
            $purchase['paid_amount'] = $purchaseId->paid_amount+$purchaseId->due_amount;
            $purchaseId->update($purchase);


            $account = CashOrBankAccount::find($purchaseId->cash_or_bank_id);
            $sharer = SupplierOrCustomer::find($purchaseId->supplier_id);
            $inputs['sharer_name'] = $sharer->name;

            // Update Cash and Bank Account Balance
            $this->bankAccountBalanceUpdate("Purchases", $account, $amount);

            if(isset($request->supplier_id)) {
                $this->sharerBalanceUpdate("Purchases", $sharer, $amount);
            }

            $inputs['transaction_code'] = transaction_code_generate();
            $save_transaction = new Transactions();
            $save_transaction->transaction_code = $inputs['transaction_code'];
            $save_transaction->supplier_id = $purchaseId->supplier_id;
            $save_transaction->purchase_id = $purchaseId->id;
            $save_transaction->cash_or_bank_id = $purchaseId->cash_or_bank_id;
            $save_transaction->date = $inputs['date'] = Carbon::today();
            $save_transaction->amount = $amount;
            $save_transaction->notation = "";
            $save_transaction->transaction_method = $inputs['transaction_method'] = "Purchases";
            $save_transaction->save();

            $account_type = AccountType::where('display_name', 'Expenses')->first();
            $inputs['account_name'] = $account->title;
            $inputs['to_account_name'] = '';
            $inputs['transaction_id'] = $save_transaction->id;
            $inputs['amount'] = $amount;
            $inputs['account_type_id'] = $account_type->id;
            $inputs['transaction_type'] = 'dr';
            $inputs['payment_method_id'] = $purchaseId->payment_method_id;
            $inputs['description'] = "Purchase due payment"." Purchase Id : ".$purchaseId->id;
            $this->createDebitAmount($inputs);

            $account_type = AccountType::where('display_name', 'Cash')->first();
            $inputs['account_type_id'] = $account_type->id;
            $inputs['transaction_type'] = 'cr';
            $this->createCreditAmount($inputs);

            $status = ['type' => 'success', 'message' => 'Purchase Due Payment done Successfully'];

        }else{

            $status = ['type' => 'danger', 'message' => 'Unable to find due payment purchase Id'];
        }


        return redirect()->route('member.purchase.due_list')->with('status', $status);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
