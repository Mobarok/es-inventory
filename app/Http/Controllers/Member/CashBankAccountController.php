<?php

namespace App\Http\Controllers\Member;

use App\DataTables\CashAndBankAccountDataTable;
use App\Models\AccountType;
use App\Models\CashOrBankAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CashBankAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( CashAndBankAccountDataTable $dataTable)
    {
        return $dataTable->render('member.cash_or_bank_account.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['account_types'] = AccountType::active()->get()->pluck('account_code_name','id');
        return view('member.cash_or_bank_account.create', $data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, $this->getValidationRules());

        $inputs = $request->all();

        DB::beginTransaction();
        try{

            $save = CashOrBankAccount::create($inputs);
            $save->description = $request->description;
            $save->internet_banking_url = $request->internet_banking_url;
            $save->initial_balance = $request->initial_balance;
            $save->current_balance = $request->initial_balance;
            $save->account_number = $request->account_number;
            $save->bank_charge_account_id = $request->bank_charge_account_id;
            $save->save();

            $status = ['type' => 'success', 'message' => 'Cash & Bank Added Successfully'];

        }catch (\Exception $e){

            $status = ['type' => 'danger', 'message' => 'Unable to Add Cash & Bank'];
            DB::rollBack();
        }

        DB::commit();

        return back()->with('status', $status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data['account_types'] = AccountType::active()->get()->pluck('account_code_name','id');
        $data['model'] = $model = CashOrBankAccount::find($id);

        if(!$model)
        {
            $status = ['type' => 'danger', 'message' => 'Don\'t have any Cash and Bank'];
            return back()->with('status', $status);
        }

        return view('member.cash_or_bank_account.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->getValidationRules());

        $model = CashOrBankAccount::find($id);
        $inputs = $request->all();

        DB::beginTransaction();
        try{
            $model->update($inputs);

            $model->description = $request->description;
            $model->internet_banking_url = $request->internet_banking_url;
//            $model->initial_balance = $request->initial_balance;
//            $model->current_balance = $request->initial_balance;
            $model->account_number = $request->account_number;
            $model->bank_charge_account_id = $request->bank_charge_account_id;
            $model->save();

            $status = ['type' => 'success', 'message' => 'Cash & Bank updated Successfully'];

        }catch (\Exception $e){

            $status = ['type' => 'danger', 'message' => 'Unable to update Cash & Bank'];
            DB::rollBack();
        }

        DB::commit();

        return back()->with('status', $status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getValidationRules($id='')
    {
        $rules = [
            'title' => 'required',
            'contact_person' => 'required',
            'phone' => 'required'
        ];

        return $rules;
    }
}
