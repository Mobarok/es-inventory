<?php

namespace App\Http\Controllers\Member;

use App\DataTables\SaleReturnDataTable;
use App\DataTables\SalesDataTable;
use App\Http\Traits\BranchTrait;
use App\Http\Traits\CompanyInfoTrait;
use App\Http\Traits\StockTrait;
use App\Http\Traits\TransactionTrait;
use App\Models\AccountType;
use App\Models\CashOrBankAccount;
use App\Models\DeliveryType;
use App\Models\DueCollectionHistory;
use App\Models\Item;
use App\Models\PaymentMethod;
use App\Models\Sale;
use App\Models\SaleDetails;
use App\Models\SaleReturn;
use App\Models\Setting;
use App\Models\Stock;
use App\Models\SupplierOrCustomer;
use App\Models\TrackShoppingBags;
use App\Models\TransactionDetail;
use App\Models\Transactions;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Milon\Barcode\DNS1D;
use Milon\Barcode\DNS2D;

class SalesController extends Controller
{
    use StockTrait, CompanyInfoTrait, BranchTrait;

    private $print_system;

    public function __construct()
    {

        $this->check_print_system();
//        if( $this->branch_assign_check() == 0)
//        {
//
//            $status = [
//                'type' => 'danger',
//                'message' => 'You are not assign for any branch. please confirm branch'
//            ];
//
//            return redirect()->route('member.dashboard')->with('status', $status)->send();
//        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SalesDataTable $dataTable)
    {
        return $dataTable->render('member.sales.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['delivery_types'] = DeliveryType::active()->get()->pluck('display_name', 'id');
        $data['customers'] = SupplierOrCustomer::onlyCustomers()->latest()->get()->pluck('name_phone', 'id')->toArray();
        $data['products'] = Item::whereHas('category', function ($query){
            $query->where('name','!=', 'shopping_bags');
        })->orderAsc()->latest()->pluck('item_name', 'id');
        $data['payment_methods'] = PaymentMethod::active()->get()->pluck('name', 'id');
        $data['banks'] = CashOrBankAccount::latest()->pluck('title', 'id');

        $data['bags'] = Item::whereHas('category', function ($query){
            $query->where('name', 'shopping_bags');
        })->latest()->get();

        return view('member.sales.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        $sale = [];
        $sale['memo_no'] = $inputs['memo_no'];
        $sale['sale_code'] = sale_code_generate();
        $sale['chalan_no'] = $inputs['chalan_no'];
        $sale['customer_id'] = $inputs['customer_id'];
        $sale['cash_or_bank_id'] = $inputs['cash_or_bank_id'];
        $sale['payment_method_id'] = $inputs['payment_method_id'];
        $sale['delivery_type_id'] = $inputs['delivery_type_id'];
        $sale['date'] = db_date_format($inputs['date']);
        $sale['total_price'] = $inputs['sub_total'];
        $sale['discount_type'] = $inputs['discount_type'];
        $sale['discount'] = $inputs['discount'];
        $sale['total_discount'] = $inputs['discount'];
        $sale['paid_amount'] = $inputs['paid_amount'];
        $sale['shipping_charge'] = $inputs['shipping_charge'];
        $sale['total_amount'] = $inputs['total_amount'];
        $sale['amount_to_pay'] = $inputs['amount_to_pay'];
        $sale['notation'] = $inputs['notation'];
        $sale['membership_card'] = $inputs['membership_card'];
//
//        DB::beginTransaction();
//        try{

        $saleInsert = Sale::create($sale);

        $saleDetails = [];
        $saleDetails['sale_id'] = $saleInsert->id;

        $product = $request->product_id;
        $unit = $request->unit;
        $qty = $request->qty;
        $price = $request->price;
        $description = $request->description;
        $last_sale_qty = $request->last_sale_qty;
        $available_stock = $request->available_stock;

        $total_amount = $insert = 0;
        $count_product = 0;
        for($i=0; $i<count($product); $i++){

            if(!isset($product[$i]) || !isset($qty[$i]))
                break;
            else
                $count_product++;


            if($qty[$i] < 1 || $price[$i] <1 )
            {
                break;
            }


            $item = Item::find($product[$i]);
            $saleDetails['item_id'] = $item_id = $product[$i];
            $saleDetails['unit'] = $item->unit;
            $saleDetails['qty'] = $quantity = $qty[$i];
            $saleDetails['price'] = $price[$i];
            $saleDetails['description'] = $description[$i];
            $saleDetails['last_sale_qty'] = $last_sale_qty[$i];
            $saleDetails['available_stock'] = $available_stock[$i];
            if($item->warranty>0){
                $saleDetails['warranty'] = $item->warranty;
                $saleDetails['warranty_start_date'] = $today = Carbon::today();
                $saleDetails['warranty_end_date'] = $today->addMonths($item->warranty);
            }


            $total_price = $qty[$i]*$price[$i];
            $total_amount +=$total_price;

            $stockCheck = Stock::where('item_id', $item_id)->where('branch_id', Auth::user()->branch_id)->first();
            if($stockCheck->stock < 1 || $stockCheck->stock < $quantity)
                break;

            $sale = SaleDetails::create($saleDetails);
            if($sale)
                $insert++;

            $this->stock_report($product[$i], $qty[$i], 'sale');
            $this->stockOut($product[$i], $qty[$i]);
            $this->createStockHistory($product[$i], $qty[$i], 'Stock Out');

        }


        if($insert == $count_product)
        {
            $bags = Item::whereHas('category', function ($query){
                $query->where('name', 'shopping_bags');
            })->latest()->get();

            foreach ($bags as $value)
            {
                if(!empty($inputs['shopping_bags_'.$value->id]))
                {
                    $saleBag = [];
                    $saleBag['sale_id'] = $saleInsert->id;
                    $saleBag['bag_id'] = $value->id;
                    $saleBag['qty'] = $qt = $inputs['shopping_bags_'.$value->id];
                    TrackShoppingBags::create($saleBag);

                    $this->stock_report($value->id,  $qt, 'sale');
                    $this->stockOut($value->id, $qt);
                    $this->createStockHistory($value->id, $qt, 'Sale');
                }

            }

            $sale = [];
//            $sale['total_price'] = $total_amount;
            $sale['total_price'] = $total_amount;

            if($inputs['discount_type']=="fixed")
            {
                $discount = $inputs['discount'];
            }else{
                $discount = $total_amount*$inputs['discount']/100;
            }

            $sale['paid_amount'] = $inputs['paid_amount'];
            $sale['total_discount'] = $discount;
            $sale['amount_to_pay'] = $total_amount-$discount+$inputs['shipping_charge'];
            $sale['grand_total'] = $total_amount+$inputs['shipping_charge'];
            $sale['due'] = $sale['amount_to_pay']-$inputs['paid_amount'];

            $saleInsert->update($sale);

//            $inputs['transaction_code'] = transaction_code_generate();
//
//            $save_transaction = new Transactions();
//            $save_transaction->transaction_code = $inputs['transaction_code'];
//            $save_transaction->supplier_id = $inputs['customer_id'];
//            $save_transaction->sale_id = $saleInsert->id;
//            $save_transaction->cash_or_bank_id = $inputs['cash_or_bank_id'];
//            $save_transaction->date = db_date_format($inputs['date']);
//            $save_transaction->amount = $sale['amount_to_pay'];
//            $save_transaction->notation = $inputs['notation'];
//            $save_transaction->transaction_method = $inputs['transaction_method'] = "Sales";
//            $save_transaction->save();
//
//            $account = CashOrBankAccount::find($request->cash_or_bank_id);
//
//            if(isset($request->customer_id)) {
//                $sharer = SupplierOrCustomer::find($request->customer_id);
//                $inputs['sharer_name'] = $sharer ? $sharer->name : '';
//            }else{
//                $inputs['sharer_name'] = '';
//            }
//
//            // Update Cash and Bank Account Balance
//            $this->bankAccountBalanceUpdate($inputs['transaction_method'], $account, $inputs['paid_amount']);
//
//
//            if(isset($request->customer_id)) {
//                $this->sharerBalanceUpdate($inputs['transaction_method'], $sharer, $inputs['paid_amount']);
//            }
//
//
////                $account_type = AccountType::find($account->account_type_id);
//            $inputs['account_type_id'] = $account->id;
//            $inputs['account_name'] = $account->title;
//            $inputs['to_account_name'] = '';
//            $inputs['transaction_id'] = $save_transaction->id;
//            $inputs['amount'] = $inputs['paid_amount'];
//            $inputs['transaction_type'] = 'cr';
//            $inputs['description'] = "Sale product"." Sale Id : ".$saleInsert->id;
//            $this->createCreditAmount($inputs);
//
//            if($sale['due']>0)
//            {
//                $account_type = AccountType::where('display_name', 'Accounts Receivable')->first();
//                $inputs['account_type_id'] = $account_type->id;
//                $inputs['account_name'] = $account->title;
//                $inputs['to_account_name'] = '';
//                $inputs['transaction_id'] = $save_transaction->id;
//                $inputs['amount'] = $sale['due'];
//                $inputs['transaction_type'] = 'cr';
//                $inputs['description'] = "Sale product"." Sale Id : ".$saleInsert->id;
//                $this->createCreditAmount($inputs);
//            }
//
//
//            $account_type = AccountType::where('display_name', 'Sales')->first();
//            $inputs['account_type_id'] = $account_type->id;
//            $inputs['amount'] = $sale['amount_to_pay'];
//            $inputs['transaction_type'] = 'dr';
//            $this->createDebitAmount($inputs);

            /*
             * TODO: Create Transaction History for Sales
             */

            $status = ['type' => 'success', 'message' => 'Sales done Successfully'];
        }else{

            $status = ['type' => 'danger', 'message' => 'Sales product out of stock'];
            $saleInsert->delete();
        }


//        }catch (\Exception $e){
//
//            $status = ['type' => 'danger', 'message' => 'Unable to save'];
//            DB::rollBack();
//        }
//
//        DB::commit();

        if($status['type'] == 'success')
        {
            return redirect()->route('member.sales.show', $saleInsert->id)->with('status', $status);
        }else{

            return redirect()->back()->with('status', $status);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['sales'] = $sale = Sale::findOrFail($id);

        $d = new DNS1D();
        $d->setStorPath(__DIR__."/cache/");
        $barcode = $sale->sale_code;
        $data['sale_barcode'] = '<img src="data:image/png;base64,' . $d->getBarcodePNG($barcode, "C128", 1, 50) . '" alt="' . $sale->sale_code . '"   />';
        return view('member.sales.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['model'] = Sale::findOrFail($id);

        $data['delivery_types'] = DeliveryType::active()->get()->pluck('display_name', 'id');
        $data['customers'] = SupplierOrCustomer::onlyCustomers()->latest()->get()->pluck('name_phone', 'id')->toArray();
        $data['products'] = Item::whereHas('category', function ($query){
            $query->where('name','!=', 'shopping_bags');
        })->orderAsc()->latest()->pluck('item_name', 'id');
        $data['payment_methods'] = PaymentMethod::active()->get()->pluck('name', 'id');
        $data['banks'] = CashOrBankAccount::latest()->pluck('title', 'id');

        $data['bags'] = Item::whereHas('category', function ($query){
            $query->where('name', 'shopping_bags');
        })->select('*', 'items.id as item_id')->latest()->get();


        return view('member.sales.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $saleFind = Sale::findOrFail($id);

        $inputs = $request->all();

        $sale = [];
        $sale['memo_no'] = $inputs['memo_no'];
        $sale['chalan_no'] = $inputs['chalan_no'];
        $sale['customer_id'] = $inputs['customer_id'];
        $sale['cash_or_bank_id'] = $inputs['cash_or_bank_id'];
        $sale['payment_method_id'] = $inputs['payment_method_id'];
        $sale['delivery_type_id'] = $inputs['delivery_type_id'];
        $sale['date'] = db_date_format($inputs['date']);
        $sale['total_price'] = $inputs['sub_total'];
        $sale['discount_type'] = $inputs['discount_type'];
        $sale['discount'] = $inputs['discount'];
        $sale['total_discount'] = $inputs['discount'];
        $sale['paid_amount'] = $inputs['paid_amount'];
        $sale['shipping_charge'] = $inputs['shipping_charge'];
        $sale['total_amount'] = $inputs['grand_total'];
        $sale['amount_to_pay'] = $inputs['amount_to_pay'];
        $sale['notation'] = $inputs['notation'];
        $sale['membership_card'] = $inputs['membership_card'];
//
//        DB::beginTransaction();
//        try{

        $saleFind->update($sale);

        $saleDetails = [];
        $saleDetails['sale_id'] = $saleFind->id;

        $product = $request->product_id;
        $unit = $request->unit;
        $qty = $request->qty;
        $price = $request->price;
        $description = $request->description;
        $available_stock = $request->available_stock;
        $last_sale_qty = $request->last_sale_qty;
        $sale_details_id = $request->sale_details_id;


        $deleteSaleDetails = SaleDetails::where('sale_id', $id)->whereNotIn('id', $sale_details_id)->get();

//        dd($deleteSaleDetail);
        foreach ($deleteSaleDetails as $value)
        {
            $sale_detail_id = SaleDetails::find($value->id);
            $this->stock_report($value->item_id, $sale_detail_id->qty, 'sale return');
            $this->stockIn($value->item_id, $sale_detail_id->qty);
            $value->delete();
        }


        $total_amount = $insert = 0;
        $count_product = 0;
        for($i=0; $i<count($product); $i++){

            if(!isset($product[$i]) || !isset($qty[$i]))
                break;
            else
                $count_product++;


            if($qty[$i] < 1 || $price[$i] <1 )
            {
                break;
            }


            if($sale_details_id[$i] != "new"){
                $sale_detail_id = SaleDetails::find($sale_details_id[$i]);
            }else{
                $saleDetails['sale_id'] = $saleFind->id;
            }

            $item = Item::find($product[$i]);
            $saleDetails['item_id'] = $item_id = $product[$i];
            $saleDetails['unit'] = $item->unit;
            $saleDetails['qty'] = $quantity = $qty[$i];
            $saleDetails['price'] = $price[$i];
            $saleDetails['description'] = $description[$i];
            $saleDetails['last_sale_qty'] = $last_sale_qty[$i];
            $saleDetails['available_stock'] = $available_stock[$i];

//            dd($saleDetails);
            if($item->warranty>0){
                $saleDetails['warranty'] = $item->warranty;
                $saleDetails['warranty_start_date'] = $today = Carbon::today();
                $saleDetails['warranty_end_date'] = $today->addMonths($item->warranty);
            }


            $total_price = $qty[$i]*$price[$i];
            $total_amount +=$total_price;

            $stockCheck = Stock::where('item_id', $item_id)->where('branch_id', Auth::user()->branch_id)->first();
            if($stockCheck->stock < 1 || $stockCheck->stock < $quantity)
                break;

            if($sale_details_id[$i] != "new") {
                $this->stockIn($product[$i], $sale_detail_id->qty);
                $this->stock_report($product[$i],  $sale_detail_id->qty, 'sale return');
                $sale = $sale_detail_id->update($saleDetails);
            }else{
                $sale = SaleDetails::create($saleDetails);
            }

            if($sale)
                $insert++;

            $this->stock_report($product[$i],  $qty[$i], 'sale');
            $this->stockOut($product[$i], $qty[$i]);
            $this->createStockHistory($product[$i], $qty[$i], 'Sale Edit');

        }


        if($insert == $count_product)
        {
            $bags = Item::whereHas('category', function ($query){
                $query->where('name', 'shopping_bags');
            })->latest()->get();

            $trackBags = TrackShoppingBags::where('sale_id', $id)->get();
//            dd($trackBags);
            foreach ($trackBags as $bag)
            {
                $this->stock_report($bag->bag_id, $bag->qty, 'sale return');
                $this->stockIn($bag->bag_id, $bag->qty);
                $bag->delete();
            }

            foreach ($bags as $value)
            {
                if(!empty($inputs['shopping_bags_'.$value->id]))
                {
                    $saleBag = [];
                    $saleBag['sale_id'] = $saleFind->id;
                    $saleBag['bag_id'] = $value->id;
                    $saleBag['qty'] = $qt = $inputs['shopping_bags_'.$value->id];
                    TrackShoppingBags::create($saleBag);

                    $this->stock_report($value->id,  $qt, 'sale');
                    $this->stockOut($value->id, $qt);
                    $this->createStockHistory($value->id, $qt, 'Sale Edit');
                }

            }

            $sale = [];
//            $sale['total_price'] = $total_amount;
            $sale['total_price'] = $total_amount;

            if($inputs['discount_type']=="fixed")
            {
                $discount = $inputs['discount'];
            }else{
                $discount = $total_amount*$inputs['discount']/100;
            }

            $sale['paid_amount'] = $inputs['paid_amount'];
            $sale['total_discount'] = $discount;
            $sale['amount_to_pay'] = $total_amount-$discount+$inputs['shipping_charge'];
            $sale['grand_total'] = $total_amount+$inputs['shipping_charge'];
            $sale['due'] = $sale['amount_to_pay']-$inputs['paid_amount'];

            $saleFind->update($sale);

//            $save_transaction = Transactions::where('sale_id', $saleFind->id)->first();
//            $save_transaction->supplier_id = $inputs['customer_id'];
//            $save_transaction->cash_or_bank_id = $inputs['cash_or_bank_id'];
//            $save_transaction->date = db_date_format($inputs['date']);
//            $save_transaction->amount = $sale['amount_to_pay'];
//            $save_transaction->notation = $inputs['notation'];
//            $save_transaction->save();
//            $inputs['transaction_method'] = "Sales";
//
//            $account = CashOrBankAccount::find($request->cash_or_bank_id);
//
//            if(isset($request->customer_id)) {
//                $sharer = SupplierOrCustomer::find($request->customer_id);
//                $inputs['sharer_name'] = $sharer ? $sharer->name : '';
//            }else{
//                $inputs['sharer_name'] = '';
//            }
//
//            // Update Cash and Bank Account Balance
//            $this->bankAccountBalanceUpdate("Sale Update", $account, $saleFind->paid_amount);
//            $this->bankAccountBalanceUpdate($inputs['transaction_method'], $account, $inputs['paid_amount']);
//
//
//            if(isset($request->customer_id)) {
//                $this->sharerBalanceUpdate("Sale Update", $sharer, $saleFind->paid_amount);
//                $this->sharerBalanceUpdate($inputs['transaction_method'], $sharer, $inputs['paid_amount']);
//            }
//
//
////            $account_type = AccountType::where('display_name', 'Cash')->first();
//
//            $trans_details = TransactionDetail::where('transaction_id', $save_transaction->id)->get();
//            foreach ($trans_details as $value)
//            {
//                $value->delete();
//            }
//
//            $inputs['account_type_id'] = $account->id;
//            $inputs['account_name'] = $account->title;
//            $inputs['to_account_name'] = '';
//            $inputs['transaction_id'] = $save_transaction->id;
//            $inputs['amount'] = $inputs['paid_amount'];
//            $inputs['transaction_type'] = 'cr';
//            $inputs['description'] = "Sale product"." Sale Id : ".$saleFind->id;
//            $this->createCreditAmount($inputs);
//
//            if($sale['due']>0)
//            {
//                $account_type = AccountType::where('display_name', 'Accounts Receivable')->first();
//                $inputs['account_type_id'] = $account_type->id;
//                $inputs['account_name'] = $account->title;
//                $inputs['to_account_name'] = '';
//                $inputs['transaction_id'] = $save_transaction->id;
//                $inputs['amount'] = $sale['due'];
//                $inputs['transaction_type'] = 'cr';
//                $inputs['description'] = "Sale product"." Sale Id : ".$saleFind->id;
//                $this->createCreditAmount($inputs);
//            }
//
//
//            $account_type = AccountType::where('display_name', 'Expenses')->first();
//            $inputs['account_type_id'] = $account_type->id;
//            $inputs['amount'] = $sale['amount_to_pay'];
//            $inputs['transaction_type'] = 'dr';
//            $this->createDebitAmount($inputs);

            /*
             * TODO: Create Transaction History for Sales
             */

            $status = ['type' => 'success', 'message' => 'Sales update Successfully'];
        }else{

            $status = ['type' => 'danger', 'message' => 'Sales product out of stock'];
//            $saleFind->delete();
        }


//        }catch (\Exception $e){
//
//            $status = ['type' => 'danger', 'message' => 'Unable to save'];
//            DB::rollBack();
//        }
//
//        DB::commit();

        if($status['type'] == 'success')
        {
            return redirect()->route('member.sales.show', $saleFind->id)->with('status', $status);
        }else{

            return redirect()->back()->with('status', $status);
        }
    }

    public function sales_return($id)
    {
        $data['model'] = Sale::findOrFail($id);

        $data['delivery_types'] = DeliveryType::active()->get()->pluck('display_name', 'id');
        $data['customers'] = SupplierOrCustomer::onlyCustomers()->latest()->get()->pluck('name_phone', 'id')->toArray();
        $data['products'] = Item::whereHas('category', function ($query){
            $query->where('name','!=', 'shopping_bags');
        })->orderAsc()->latest()->pluck('item_name', 'id');
        $data['payment_methods'] = PaymentMethod::active()->get()->pluck('name', 'id');
        $data['banks'] = CashOrBankAccount::latest()->pluck('title', 'id');

        $data['bags'] = Item::whereHas('category', function ($query){
            $query->where('name', 'shopping_bags');
        })->latest()->get();

        return view('member.sales.return', $data);
    }

    public function sales_return_update(Request $request, $id)
    {
        $sale = Sale::findOrFail($id);

        $product = $request->product_id;
        $unit = $request->unit;
        $qty = $request->qty;
        $price = $request->price;
        $return_qty = $request->return_qty;
        $return_price = $request->return_price;
        $description = $request->description;

        $msg = $failure ='';
        $saleReturn = [];
        $saleReturn['sale_id'] = $id;
        $saleReturn['return_code'] = $code = return_code_generate("S");

        $total_return_price = 0;
        $return_count = 0;
        for($i=0; $i<count($product); $i++){

            $saleDetails = SaleDetails::where('sale_id', $id)
                ->where('item_id', $product[$i])
                ->first();

            $item = Item::find($product[$i]);
            $saleReturn['item_id'] = $item_id = $product[$i];

            $return_item = SaleReturn::where('item_id', $item_id)
                ->where('sale_id', $id)
                ->sum('return_qty');

            $saleReturn['unit'] = $item->unit;
            $saleReturn['qty'] = $qty[$i];
            $saleReturn['return_qty'] = $return_quantity = $return_qty[$i];
            $saleReturn['price'] = $price[$i];
            $saleReturn['return_price'] = $return_price[$i];

            if( ($saleDetails->qty >= ($return_item+$return_qty[$i])) &&  isset($return_quantity) && $return_quantity>0)
            {
                if($saleDetails->price >= $return_price[$i])
                {
                    SaleReturn::create($saleReturn);
                    $this->stock_report($product[$i], $return_qty[$i], 'sale return');
                    $this->stockIn($product[$i], $return_qty[$i]);
                    $this->createStockHistory($product[$i], $return_qty[$i],'sale Return');
                    $msg = true;
                    $return_count++;
                }else{
                    $msg = false;
                    $failure .= " Product: ".$item_id." price can't be bigger than sale price";
                }

            }else{
                $msg = false;
                $failure .= " Product: ".$item_id." already return complete";
            }

            $total_return_price += ($return_quantity*$return_price[$i]);
        }

        if($total_return_price > $sale->grand_total)
        {
            $msg = false;
            $failure = "Sale Return amount can't be bigger than Bill Amount";
        }

        if($msg == true || $return_count > 0)
        {
            $saleUpdate = [];
            if( $sale->due > 0 && $sale->due >= $total_return_price)
            {
                $saleUpdate['due'] = $sale->due-$total_return_price;
            }

            $sale->update($saleUpdate);


            $status = ['type' => 'success', 'message' => 'Sale Return done Successfully'];

            return redirect()->route('member.sales.view_return', [ 'id'=>$id, 'code'=>$code] )->with('status', $status);
        }else{
            $status = ['type' => 'danger', 'message' => 'Unable to product return '.$failure];
            return redirect()->back()->with('status', $status);
        }
    }

    public function sale_return_view($id, $code)
    {
        $data['sale'] = Sale::join('sales_return','sales_return.sale_id','=','sales.id')
            ->join('items','items.id','=','sales_return.item_id')
            ->where('return_code', '=',$code)
            ->where('sale_id', '=',$id)->get();

        if(count($data['sale'])>0)
            return view('member.sales.view_return', $data);
        else
            return redirect()->route('member.sales.sales_return_list');
    }


    public function sales_return_list(SaleReturnDataTable $dataTable)
    {
        return $dataTable->render('member.sales.sale_return_list');
    }


    public function due_list()
    {
        $data['modal'] = Sale::authMember()->authCompany()->where('due', '>', 0)->paginate(15);

        return view('member.sales.due_list', $data);
    }


    public function due_payment($id)
    {
        $sale = $data['sales'] = Sale::where('id', $id)->where('due','>',0)->first();
        $data = $this->company($data);
        if($sale)
        {
            return view('member.sales.due_payment', $data);
        }else{
            $status = [
                'type' => 'danger',
                'message' => "Sale ID: ".$id.", This Sale don't have any due"
            ];
            return redirect()->back()->with('status', $status);
        }

    }

    public function receive_due_payment(Request $request, $id)
    {
        $saleId = Sale::find($id);

        if($saleId)
        {
            $amount = $saleId->due;

            $save = new DueCollectionHistory();
            $inputs = [];
            $inputs['inventory_type'] = 'Sale';
            $inputs['inventory_type_id'] = $saleId->id;
            $inputs['sharer_id'] = $saleId->customer_id;
            $inputs['amount'] = $saleId->due;
            $inputs['collection_date'] = Carbon::today();
            $save->create($inputs);

            $sale = [];
            $sale['due'] = 0;
            $sale['paid_amount'] = $saleId->paid_amount+$saleId->due;
            $saleId->update($sale);

            $status = ['type' => 'success', 'message' => 'Sale Due Payment done Successfully'];
        }else{
            $status = ['type' => 'danger', 'message' => 'Unable to find due payment sale Id'];
        }

        return redirect()->route('member.sales.due_list')->with('status', $status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function print_sale($id)
    {
        $data['sales'] = $sale = Sale::findOrFail($id);

        $d = new DNS1D();
        $d->setStorPath(__DIR__."/cache/");
        $barcode = $sale->sale_code;
        $data['sale_barcode'] = '<img id="barcode" src="data:image/png;base64,' . $d->getBarcodePNG($barcode, "C128", 1, 50) . '" alt="' . $sale->sale_code . '"   />';
        $data = $this->company($data);

        return view('member.sales.'.$this->print_system.'_print_sales', $data);
    }


    // Whole Sale End

    private function check_print_system()
    {
        $settings = Setting::where('key', '=', 'print_page_setup')->first();

        if($settings)
        {
            $this->print_system = $settings->value;
        }else{
            $this->print_system = '';
        }

        $this->print_system = $this->print_system == "default" ? '' : $this->print_system;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
