<?php

namespace App\Http\Controllers\Member;

use App\DataTables\ItemsDataTable;
use App\Http\Traits\CompanyInfoTrait;
use App\Http\Traits\FileUploadTrait;
use App\Models\Category;
use App\Models\Item;
use App\Models\Unit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Milon\Barcode\DNS1D;

class ItemController extends Controller
{
    use FileUploadTrait, CompanyInfoTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ItemsDataTable $dataTable)
    {
        return $dataTable->render('member.items.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['categories'] = Category::all()->pluck('display_name', 'id');
        $data['units'] = Unit::all()->pluck('name', 'name');
        return view('member.items.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $inputs = $request->all();

        $this->validate($request, $this->rules());

        if($request->hasFile('product_image'))
        {
            $image = $request->file('product_image');

            $upload = $this->fileUpload($image, '/product_image/', null);

            if (!$upload)
            {
                $status = ['type' => 'danger', 'message' => 'Image Must be JPG, JPEG, PNG, GIF'];
                return back()->with('status', $status);
            }
            $inputs['product_image'] = $upload;
        }

        Item::create($inputs);

        $status = ['type' => 'success', 'message' => 'Successfully Added.'];

        return back()->with('status', $status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['modal'] = Item::findorFail($id);
        $data['categories'] = Category::all()->pluck('display_name', 'id');
        $data['units'] = Unit::all()->pluck('name', 'name');

        return view('member.items.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $modal = Item::find($id);
        $inputs = $request->all();

        $this->validate($request, $this->rules($id));

        if($request->hasFile('product_image'))
        {
            $image = $request->file('product_image');

            $upload = $this->fileUpload($image, '/product_image/', null);

            if (!$upload)
            {
                $status = ['type' => 'danger', 'message' => 'Image Must be JPG, JPEG, PNG, GIF'];
                return back()->with('status', $status);
            }
            $inputs['product_image'] = $upload;
        }

        /**
         *  TODO: Before update image last image will be deleted.
         */
        $modal->update($inputs);

        $status = ['type' => 'success', 'message' => 'Successfully Updated.'];

        return back()->with('status', $status);
    }


    public function print_barcode_form(Request $request)
    {
        $data['products'] = Item::get()->pluck('item_name', 'id');

        return view('member.items.barcode_print_form', $data);
    }

    public function print_barcode(Request $request)
    {
        $item = Item::find($request->item_id);

        $data['item'] = $item;
        $data['barcode'] = DNS1D::getBarcodeHTML( $item->productCode, "C128", 1,20);
        $data['print_qty'] = $request->print_qty;
        $data = $this->company($data);
//        $data['full_url'] =  $request->fullUrl().($request->fullUrl() == $request->url() ? "?" : "&");

//        if($request->type=="print" || $request->type=="download") {
//            if ($request->type == "print") {
//                return View('member.items.print_barcode', $data);
//            } else if ($request->type == "download") {
//                $pdf = PDF::loadView('member.items.print_barcode', $data);
//                $file_name = file_name_generator("Print_Product_Barcode");
//                return $pdf->download($file_name);
//            }
//        }else{
//            return view('member.items.show_product_barcode', $data);
        return View('member.items.print_barcode', $data);
//        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $modal = Item::findOrFail($id);
        $modal->delete();

        return response()->json([
            'data' => [
                'message' => 'Successfully deleted'
            ]
        ], 200);
    }

    private function rules($id='')
    {
        if($id==''){
            $rules = [
                'item_name' => 'required|unique:items,item_name',
                'productCode' => 'required|unique:items,productCode',
                'skuCode' => 'unique:items,skuCode',
                'unit' => 'required',
            ];
        }else{
            $rules = [
                'item_name' => 'required|unique:items,item_name,'.$id,
                'productCode' => 'required|unique:items,productCode,'.$id,
                'skuCode' => 'unique:items,skuCode,'.$id,
                'unit' => 'required',
            ];
        }

        return $rules;
    }
}
