<?php

namespace App\Http\Controllers\Member;

use App\DataTables\MemberUsersDataTable;
use App\Http\Traits\VerifyUserTrait;
use App\Models\Branch;
use App\Models\Company;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    use VerifyUserTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(MemberUsersDataTable $dataTable)
    {
        return $dataTable->render('member.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['roles'] = Role::filter()->get()->pluck('display_name', 'id');
        return view('member.users.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->getValidationRules());

        $inputs = $request->all();
        $email = $request->email;
        $roles = $request->roles;
        $inputs['member_id'] = Auth::user()->member_id;
        $inputs['membership_id'] = Auth::user()->membership_id;
        $inputs['company_id'] = Auth::user()->company_id;
        $inputs['verify_token'] = $verify_token = verify_token_generate();
        $inputs['status'] = "inactive";
//        DB::beginTransaction();
//
//        try{
            $saveUser = User::create($inputs);
            $assignRole = $saveUser->attachRoles($roles);
            $this->sendVerifyToken($verify_token, $email, 'User');
            $status = ['type' => 'success', 'message' => 'User Added Successfully'];

//        }catch (\Exception $e){
//
//            $status = ['type' => 'danger', 'message' => 'Unable to User Add'];
//            DB::rollBack();
//        }
//
//        DB::commit();

        return back()->with('status', $status);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['model'] = $user = User::where('id', $id)->membersUser()->first();
        if(!$user)
        {
            $status = ['type' => 'danger', 'message' => 'User Not find'];
            return back()->with('status', $status);
        }

        $data['roles'] = Role::filter()->get()->pluck('display_name', 'id');
        $data['assignRole'] = $user->roles->toArray();
        return view('member.users.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user =  User::where('id', $id)->membersUser()->first();

        if(!$user)
        {
            $status = ['type' => 'danger', 'message' => 'User Not find'];
            return back()->with('status', $status);
        }

        $this->validate($request, $this->getValidationRules($id));

        $user->email = $email = $request->email;
        $user->full_name = $request->full_name;
        $user->phone = $request->phone;
        $user->status = $request->status;
        $roles = $request->roles;

        DB::beginTransaction();

        try{

            $user->save();
            $assignRole = $user->roles()->sync($roles);
            $status = ['type' => 'success', 'message' => 'User Updated Successfully'];

        }catch (\Exception $e){

            $status = ['type' => 'danger', 'message' => 'Unable to User update'];
            DB::rollBack();
        }

        DB::commit();

        return back()->with('status', $status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $modal = User::findOrFail($id);
        $modal->delete();

        return response()->json([
            'data' => [
                'message' => 'Successfully deleted'
            ]
        ], 200);
    }

    private function getValidationRules($id='')
    {
        $rules = [
            'full_name' => 'required'
        ];

        if (is_null($id)) {
            $rules['email'] = 'required|email|unique:users';
            $rules['phone'] = 'nullable|unique:users,phone';
        } else {
            $rules['email'] = 'required|email|unique:users,email,' . $id;
            $rules['phone'] = 'nullable|unique:users,phone,' . $id;
        }

        return $rules;
    }


    public function set_users_company()
    {
        $data = [];
        $data['companies'] = Company::active()->authMember()->get()->pluck('company_name', 'id');
        $data['users'] = User::active()->membersUser()->get()->pluck('user_details', 'id');
        $data['users_company'] = User::active()->membersUser()->systemUser()->paginate(10);
        return view('member.users.company_assign', $data);
    }

    public function save_users_company(Request $request)
    {
        $rules = [
            'company_id' => 'required',
            'user_id' => 'required'
        ];

        $this->validate($request, $rules);

        $user  = User::membersUser()->where('id', $request->user_id)->first();
        $user->company_id = $request->company_id;
        $user->save();
        $status = ['type' => 'success', 'message' => 'Successfully User Add Company '.$user->full_name." ".$user->email];
        return back()->with('status', $status);
    }



    public function set_users_branch()
    {
        $data = [];
        $data['branches'] = Branch::active()->authMember()->get()->pluck('display_name', 'id');
        $data['users'] = User::active()->membersUser()->get()->pluck('user_details', 'id');
        $data['users_branch'] = User::active()->membersUser()->systemUser()->paginate(10);
        return view('member.users.branch_assign', $data);
    }


    public function save_users_branch(Request $request)
    {
        $rules = [
            'branch_id' => 'required',
            'user_id' => 'required'
        ];

        $this->validate($request, $rules);

        $user  = User::membersUser()->where('id', $request->user_id)->first();
        $user->branch_id = $request->branch_id;
        $user->save();
        $status = ['type' => 'success', 'message' => 'Successfully User Add Branch '.$user->full_name." ".$user->email];
        return back()->with('status', $status);
    }
}
