<?php

namespace App\Http\Controllers\Member;

use App\Http\Traits\TransactionDetailsTrait;
use App\Models\AccountType;
use App\Models\CashOrBankAccount;
use App\Models\TransactionDetail;
use App\Models\Transactions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GeneralLedgerController extends Controller
{
    use TransactionDetailsTrait;
    /**
     * Show list of General Ledger by Auth Company
     */
    public function search(Request $request)
    {
        $inputs = $request->all();

        $multiple_condition = [];

        if( !empty($inputs['date']))
            $multiple_condition['transactions.date'] = db_date_format($request->date);

        if( !empty($inputs['transaction_code']))
            $multiple_condition['transaction_code'] = $request->transaction_code;

        if( !empty($inputs['from_account_type_id']))
            $multiple_condition['transactions.cash_or_bank_id'] = $request->from_account_type_id;

        if( !empty($inputs['to_account_type_id']))
            $multiple_condition['transactions.cash_or_bank_id'] = $request->to_account_type_id;


        $data['modal'] = $this->transaction_full_details(
            $member = true, $company=true, $page = 20, $tr_payment=false, $updated_user=false,
            $tr_category=false, $select_column="tr-data", $group_tr_code=true, $group_tr_type=true,
            $order = 'DESC', '','','', $multiple_condition, $get=true
        );

        $data['accounts'] = CashOrBankAccount::authMember()->active()->get()->pluck('title','id');


        return view('member.general-ledger.index', $data);
    }

    /**
     * Show list of All Ledgers for Only Members
     */
    public function list_ledger(Request $request)
    {
        $inputs = $request->all();

        $multiple_condition = [];


        if( !empty($inputs['date']))
            $multiple_condition['transactions.date'] = db_date_format($request->date);

        if( !empty($inputs['transaction_code']))
            $multiple_condition['transaction_code'] = $request->transaction_code;

        if( !empty($inputs['from_account_type_id']))
            $multiple_condition['transactions.cash_or_bank_id'] = $request->from_account_type_id;

        if( !empty($inputs['to_account_type_id']))
            $multiple_condition['transactions.cash_or_bank_id'] = $request->to_account_type_id;


        $data['modal'] =  $this->transaction_full_details(
            $member = true, $company=false, $page = 20, $tr_payment=false, $updated_user=false,
            $tr_category=false, $select_column="tr-data", $group_tr_code=true, $group_tr_type=true,
            $order = 'DESC', '','','', $multiple_condition, $get=true
        );

        $data['accounts'] = CashOrBankAccount::authMember()->active()->get()->pluck('title','id');

        return view('member.general-ledger.index', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($code)
    {
        $data['transaction'] = $transaction  = Transactions::where('transaction_code', $code)->get();

        if(count($transaction)<1)
        {
            $status = ['type' => 'danger', 'message' => 'Transaction Code: '.$code." not found. "];
            return back()->with('status', $status);
        }

//         $general_ledger = Transactions::where('transaction_code', $code)
//                                        ->first();

        if($transaction[0]->transaction_method == 'Journal Entry')
            return redirect()->route('member.journal_entry.show', $code);

        $condition =  '=';
        $condition_col = "transaction_code";
        $value = $code;

        $general_ledger = $this->transaction_full_details(
            true, true, '', false, false,false, $select_column="",
            false, false, 'ASC', $condition, $condition_col, $value );



        $account_type_bank = AccountType::where('id', $general_ledger[0]->cash_account_type_id)->first();

        $data['general_ledger']['transaction'] = $general_ledger;
        $data['general_ledger']['transaction_code'] = $general_ledger[0]->transaction_code;
        $data['general_ledger']['method'] = $general_ledger[0]->transaction_method;
        $data['general_ledger']['date'] = db_date_month_year_format($general_ledger[0]->date);
        $data['general_ledger']['transaction_form'] = isset($account_type_bank->display_name) ? $account_type_bank->display_name :  '';
        $data['general_ledger']['transaction_form_code'] = isset($account_type_bank->id) ? format_number_digit($account_type_bank->id) : '';
        $data['general_ledger']['entry_by'] = $general_ledger[0]->created_user;


        return view('member.general-ledger.show', $data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



}
