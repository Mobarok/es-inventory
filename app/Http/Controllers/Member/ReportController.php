<?php

namespace App\Http\Controllers\Member;

use App\Http\Traits\AccountTypeTrait;
use App\Http\Traits\CompanyInfoTrait;
use App\Http\Traits\TransactionDetailsTrait;
use App\Models\AccountType;
use App\Models\Branch;
use App\Models\CashOrBankAccount;
use App\Models\DueCollectionHistory;
use App\Models\GLAccountClass;
use App\Models\Item;
use App\Models\Purchase;
use App\Models\PurchaseDetail;
use App\Models\Sale;
use App\Models\Setting;
use App\Models\Stock;
use App\Models\StockReport;
use App\Models\SupplierOrCustomer;
use App\Models\Transactions;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\DataTables;
use App\Acme\pdfGenerator\GeneratePdf;
use Barryvdh\DomPDF\Facade as PDF;

class ReportController extends Controller
{
    use TransactionDetailsTrait, AccountTypeTrait, CompanyInfoTrait;

    private $print_system;

    public function __construct(){
        $this->check_print_system();
    }

    public function search(Request $request)
    {
        $query = $this->select_heads($page='get');
        $data['accounts'] = $query->get()->pluck('display_name', 'id');

        return view('member.reports.search_data', $data);
    }

    public function report_list()
    {
        return view('member.reports.report_list');
    }

    public function all_report_list($type, Request $request){

        $data['modal']  = $this->search_data($request, true);

        $data['title'] = $data['report_title'] = 'All '.$type.' report ';
        $data['filename'] = filename('Income report').'pdf';
        $data['filepage'] = 'member.reports.pdf_print';

        $data = $this->company($data);

        // Generate Consignments Report PDF
//        $all_income_report = new \App\Acme\pdfGenerator\Transactions($data);
        $pdf = PDF::loadHTML('<h1>Test</h1>');
//        $pdf = PDF::loadHTML('member.reports.pdf_print', $data);
//        return $pdf->download($data['filename']);
        return $pdf->stream();

    }

    public function daily_stocks(Request $request){

        $data['products'] = Item::get()->pluck('item_name', 'id');

        $inputs = $request->all();

        $query = new StockReport();

        if(isset($request->item_id))
        {
            $query = $query->where('item_id', $request->item_id);
        }

        $data['branches'] = Branch::get()->pluck('display_name', 'id');
        $query = $this->branch_check($query, $request);

        $fromDate = $toDate = '';
        if( !empty($inputs['from_date']) )
            $inputs['from_date'] = $fromDate = db_date_format($inputs['from_date']);

        if( !empty($inputs['to_date']) )
            $inputs['to_date'] = $toDate = db_date_format($inputs['to_date']);


        if( empty($fromDate) && (!empty($toDate)) ) {

            $query = $query->where('date','<=', $toDate);
        }elseif( (!empty($fromDate)) && empty($toDate) ) {

            $query = $query->where('date','>=', $fromDate);
        }elseif ( $fromDate !='' && $toDate != '' ) {

            $query = $query->whereBetween('date',[$fromDate, $toDate]);
        }
        $data['full_url'] =  $request->fullUrl().($request->fullUrl() == $request->url() ? "?" : "&");
        $data['title'] = $data['report_title']= $title = "Daily Stock Report";
        $data = $this->company($data);

        if($request->type=="print" || $request->type=="download") {

            $data['stocks'] = $query->get();
            if ($request->type == "print") {
                return view('member.reports.print_daily_stock_report', $data);
            } else if ($request->type == "download") {
                $pdf = PDF::loadView('member.reports.print_daily_stock_report', $data);
                $file_name = file_name_generator($title);
                return $pdf->download($file_name);
            }
        } else {
            $data['stocks']  = $query->paginate(20);
            return view('member.reports.stock_report', $data);
        }

    }

    public function cost_report($cost_type, Request $request){

        $inputs = $request->all();

        $query = new Purchase();
        if(isset($request->memo_no))
        {
            $query = $query->where('memo_no', $request->memo_no);
        }

        if(isset($request->chalan))
        {
            $query = $query->where('chalan', $request->chalan);
        }

        $data['branches'] = Branch::get()->pluck('display_name', 'id');
        $query = $this->branch_check($query, $request);

        $fromDate = $toDate = '';
        if( !empty($inputs['from_date']) )
            $inputs['from_date'] = $fromDate = db_date_format($inputs['from_date']);

        if( !empty($inputs['to_date']) )
            $inputs['to_date'] = $toDate = db_date_format($inputs['to_date']);


        if( empty($fromDate) && (!empty($toDate)) ) {

            $query = $query->where('date','<=', $toDate);
        }elseif( (!empty($fromDate)) && empty($toDate) ) {

            $query = $query->where('date','>=', $fromDate);
        }elseif ( $fromDate !='' && $toDate != '' ) {

            $query = $query->whereBetween('date',[$fromDate, $toDate]);
        }
        $data['full_url'] =  $request->fullUrl().($request->fullUrl() == $request->url() ? "?" : "&");
//        $data['full_url'] =  $request->url();

        $data['branches'] = Branch::get()->pluck('display_name', 'id');
        $data = $this->company($data);
        $data['cost_type'] = $cost_type;

        if($cost_type=="transport"){
            $query = $query->where("transport_cost",'>', 0);
        }else{
            $query = $query->where("unload_cost",'>', 0);
        }

        $data['report_title'] = ucfirst($cost_type).' Cost Report';

        if($request->type=="print"|| $request->type=="download") {

            $data['modal']  = $query->get();
            if ($request->type == "print") {
                return view('member.reports.print_cost_report', $data);
            } else if ($request->type == "download") {
                $pdf = PDF::loadView('member.reports.print_cost_report', $data);
                $file_name = file_name_generator("Cost_Report_");
                return $pdf->download($file_name);
            }

        } else {
            $data['modal']  = $query->paginate(20)->appends(request()->query());
            return view('member.reports.cost_report', $data);
        }

    }

    public function stocks(Request $request){

        $data['products'] = Item::get()->pluck('item_name', 'id');

        $inputs = $request->all();

        $query = new Stock();

        if(isset($request->item_id))
        {
            $query = $query->where('id', $request->item_id);
        }
        $data['branches'] = Branch::get()->pluck('display_name', 'id');
        $query = $this->branch_check($query, $request);

        $data['full_url'] =  $request->fullUrl();

        $data = $this->company($data);
        $data['report_title'] = $title = "Stock Report";
        if($request->type=="print"|| $request->type=="download") {

            $data['stocks'] = $query->get();
            if ($request->type == "print") {
                return view('member.reports.print_stocks', $data);
            } else if ($request->type == "download") {
                $pdf = PDF::loadView('member.reports.print_stocks', $data);
                $file_name = file_name_generator($title);
                return $pdf->download($file_name);
            }

        } else {
            $data['stocks']  = $query->paginate(20);
            return view('member.reports.stocks', $data);
        }

    }

    public function sharer_due_report($type, Request $request)
    {
        $inputs = $request->all();
        $query = SupplierOrCustomer::authMember();

        if($type=="customer")
        {
            $data['type'] = $type = "customer";
            $query = $query->onlyCustomers()->where('customer_current_balance','<',0);
            $sharers = $query->onlyCustomers();
        }else{
            $data['type'] = $type = "supplier";
            $query = $query->onlySuppliers()->where('supplier_current_balance','>',0);
            $sharers = $query->onlySuppliers();
        }

        if(!empty($inputs['sharer_id']))
        {
            $query = $query->where('id', $inputs['sharer_id']);
        }

        $data['sharers'] = $sharers->get()->pluck('name','id');
        $data['full_url'] =  $request->fullUrl().($request->fullUrl() == $request->url() ? "?" : "&");
        $data['report_title'] = ucfirst($type)." Due Report";

        if($request->type=="print"|| $request->type=="download") {
            $data['modal'] = $query->get();
            $data = $this->company($data);

            if ($request->type == "print") {
                return view('member.reports.print_sharer_due_report', $data);
            } else if ($request->type == "download") {
                $pdf = PDF::loadView('member.reports.print_sharer_due_report', $data);
                $file_name = file_name_generator(ucfirst($type)."_Due_Report");
                return $pdf->download($file_name);
            }
        } else {
            $data['modal'] = $query->paginate(10)->appends(request()->query());
            return view('member.reports.sharer_due_report', $data);
        }

    }

    public function inventory_due_report($type, Request $request)
    {

        $inputs = $request->all();

        if($type=="sale")
        {
            $data['type'] = $type = "Sale";
            $sharer_type = "customer";
            $query = Sale::authMember()->authCompany()->where('due', '>', 0);
            $sharers = SupplierOrCustomer::onlyCustomers();
            if(!empty($inputs['sharer_id']))
            {
                $query = $query->where('customer_id', '=',  $inputs['sharer_id']);
            }

        }else{
            $data['type'] = $type = "Purchase";
            $sharer_type = "supplier";
            $query = Purchase::authMember()->authCompany()->where('due_amount', '>', 0);
            $sharers = SupplierOrCustomer::onlySuppliers();
            if(!empty($inputs['sharer_id']))
            {
                $query = $query->where('supplier_id', '=',  $inputs['sharer_id']);
            }
        }

        $fromDate = $toDate = '';
        if( !empty($inputs['from_date']) )
            $inputs['from_date'] = $fromDate = db_date_format($inputs['from_date']);

        if( !empty($inputs['to_date']) )
            $inputs['to_date'] = $toDate = db_date_format($inputs['to_date']);


        if( empty($fromDate) && (!empty($toDate)) ) {

            $query = $query->where('date','<=', $toDate);
        }elseif( (!empty($fromDate)) && empty($toDate) ) {

            $query = $query->where('date','>=', $fromDate);
        }elseif ( $fromDate !='' && $toDate != '' ) {

            $query = $query->whereBetween('date',[$fromDate, $toDate]);
        }


        $data['branches'] = Branch::get()->pluck('display_name', 'id');
//        $data['sharers'] = SupplierOrCustomer::authMember()->where('customer_type', '=', $sharer_type)->active()->get()->pluck('name','id');
        $data['sharers'] = $sharers->get()->pluck('name','id');
        $data['full_url'] =  $request->fullUrl().($request->fullUrl() == $request->url() ? "?" : "&");
        $data['report_title'] = $type." Due Report".$fromDate.($toDate?" - ".$toDate:"");

        if($request->type=="print"|| $request->type=="download") {
            $data['modal'] = $query->get();
            $data = $this->company($data);

            if ($request->type == "print") {
                return view('member.reports.print_inventory_due_report', $data);
            } else if ($request->type == "download") {
                $pdf = PDF::loadView('member.reports.print_inventory_due_report', $data);
                $file_name = file_name_generator($type."_Due_Report");
                return $pdf->download($file_name);
            }

        } else {

            $data['modal'] = $query->paginate(10)->appends(request()->query());
            return view('member.reports.inventory_due_report', $data);
        }
    }

    public function sharer_due_collection_report($type, Request $request)
    {
        if($type=="customer")
        {
            $condition = "Sale due payment";
            $sharers = SupplierOrCustomer::authMember()->onlyCustomers();
        }else{
            $condition = "Purchase due payment";
            $sharers = SupplierOrCustomer::authMember()->onlySuppliers();
        }
        $data['type'] = $type;

        $inputs = $request->all();
        $query = DB::table('transactions')
            ->leftJoin('transaction_details', 'transaction_details.transaction_id', '=','transactions.id')
            ->leftJoin('suppliers_or_customers','transactions.supplier_id','=','suppliers_or_customers.id')
            ->select('transactions.*','transaction_details.*','suppliers_or_customers.name as sharer_name')
            ->where('transaction_details.description','like',"%".$condition."%")
            ->orderBy('transactions.id','ASC');

        if(!empty($inputs['sharer_id']))
        {
            $query = $query->where('supplier_id', '=',  $inputs['sharer_id']);
        }

        $query = $this->transactionDateCheck($query, $inputs);

        $fromDate = $toDate = '';
        if( !empty($inputs['from_date']) )
            $inputs['from_date'] = $fromDate = db_date_format($inputs['from_date']);

        if( !empty($inputs['to_date']) )
            $inputs['to_date'] = $toDate = db_date_format($inputs['to_date']);

        $data['sharers'] = $sharers->get()->pluck('name','id');
        $data['full_url'] =  $request->fullUrl().($request->fullUrl() == $request->url() ? "?" : "&");
//        dd($query->get());

        $data['report_title'] = $title = ucfirst($type)." Due Collection Report".$fromDate.($toDate?" - ".$toDate:"");

        if($request->type=="print"|| $request->type=="download") {
            $data['modal'] = $query->get();
            $data = $this->company($data);

            if ($request->type == "print") {
                return view('member.reports.print_sharer_due_collection_report', $data);
            } else if ($request->type == "download") {
                $pdf = PDF::loadView('member.reports.print_sharer_due_collection_report', $data);
                $file_name = file_name_generator(ucfirst($type)."_Due_Collection_Report_");
                return $pdf->download($file_name);
            }

        } else {

            $data['modal'] = $query->paginate(10)->appends(request()->query());
            return view('member.reports.sharer_due_collection_report', $data);
        }

    }

    public function balance_sheet(Request $request)
    {
        $purchase = new Purchase();
        $sale = new Sale();
        $due_collection = new DueCollectionHistory();


        $inputs = $request->all();
        $fromDate = $toDate = '';
        if( !empty($inputs['from_date']) )
            $inputs['from_date'] = $fromDate = db_date_format($inputs['from_date']);

        if( !empty($inputs['to_date']) )
            $inputs['to_date'] = $toDate = db_date_format($inputs['to_date']);


        if( empty($fromDate) && (!empty($toDate)) ) {
            $purchase = $purchase->where('date','<=', $toDate);
            $sale = $sale->where('date','<=', $toDate);
            $due_collection = $due_collection->where('collection_date','<=', $toDate);


        }elseif( (!empty($fromDate)) && empty($toDate) ) {
            $purchase = $purchase->where('date','>=', $fromDate);
            $sale = $sale->where('date','>=', $fromDate);
            $due_collection = $due_collection->where('collection_date','>=', $fromDate);

        }elseif ( $fromDate !='' && $toDate != '' ) {
            $purchase = $purchase->whereBetween('date',[$fromDate, $toDate]);
            $sale = $sale->whereBetween('date',[$fromDate, $toDate]);
            $due_collection = $due_collection->whereBetween('collection_date',[$fromDate, $toDate]);
        }else{
            $purchase = $purchase->where('date', '=', Carbon::today() );
            $sale = $sale->where('date', '=', Carbon::today() );
            $due_collection = $due_collection->where('collection_date', '=', Carbon::today() );
        }
        // Group by So 2nd row select that's why it's reverse order

        $data['total_purchase'] = $purchase->sum('paid_amount');
        $data['total_due_purchase'] = $purchase->sum('due_amount');
        $data['total_advance_purchase'] = $purchase->sum('advance_amount');
        $data['total_sale'] = $sale->sum('total_price');
        $data['total_due_sale'] = $sale->sum('due');
        $data['total_due_collection_amount'] = $due_collection->sum('amount');


        $data['full_url'] =  $request->fullUrl().($request->fullUrl() == $request->url() ? "?" : "&");

        $data['report_title'] = "Balance Sheet Report: ".db_date_month_year_format($fromDate).($toDate?" - ".db_date_month_year_format($toDate):"");
        $data = $this->company($data);

        if($request->type=="print"|| $request->type=="download") {

            if ($request->type == "print") {
                return view('member.reports.print_balance_sheet_report', $data);
            } else if ($request->type == "download") {
                $pdf = PDF::loadView('member.reports.print_balance_sheet_report', $data);
                $file_name = file_name_generator("Balance_Sheet_Report_");
                return $pdf->download($file_name);
            }

        } else {
            return view('member.reports.balance_sheet_report', $data);
        }
    }

    public function total_stocks(Request $request){

        $data['products'] = Item::get()->pluck('item_name', 'id');

        $inputs = $request->all();

        $query = new Stock();

        if(isset($request->item_id))
        {
            $query = $query->where('item_id', $request->item_id);
        }

        $data['full_url'] =  $request->fullUrl();

        $query = $query->selectRaw('*, sum(stock) as stock')->groupBy('item_id');

        $data['report_title'] = $title = "All Stock Report";
        $data = $this->company($data);

        if($request->type=="print"|| $request->type=="download") {

            $data['stocks'] = $query->get();
            if ($request->type == "print") {
                return view('member.reports.print_total_stocks', $data);
            } else if ($request->type == "download") {
                $pdf = PDF::loadView('member.reports.print_total_stocks', $data);
                $file_name = file_name_generator($title);
                return $pdf->download($file_name);
            }

        } else {
            $data['stocks']  = $query->paginate(20);
            return view('member.reports.total_stock', $data);
        }

    }

    public function product_purchase_report(Request $request){

        $inputs = $request->all();
        $data['products'] = Item::get()->pluck('item_name', 'id');
        $data['branches'] = Branch::get()->pluck('display_name', 'id');
        $query = new PurchaseDetail();
        $data['item'] = null;
        if(!empty($inputs['item_id']))
        {
            $query = $query->where('item_id', $inputs['item_id']);
            $data['item'] = $inputs['item_id'];
        }

        $data['branches'] = Branch::get()->pluck('display_name', 'id');
        $query = $this->branch_check($query, $request);

        $fromDate = $toDate = '';
        if( !empty($inputs['from_date']) )
            $inputs['from_date'] = $fromDate = db_date_format($inputs['from_date']);

        if( !empty($inputs['to_date']) )
            $inputs['to_date'] = $toDate = db_date_format($inputs['to_date']);

        if( empty($fromDate) && (!empty($toDate)) ) {

            $query = $query->whereHas('purchases', function ($query) use($toDate){
                $query->whereDate('date','<=', $toDate);
            });
        }elseif( (!empty($fromDate)) && empty($toDate) ) {

            $query = $query->whereHas('purchases', function ($query) use($fromDate){
                $query->whereDate('date','>=', $fromDate);
            });
        }elseif ( $fromDate !='' && $toDate != '' ) {

            $query = $query->whereHas('purchases', function ($query) use($fromDate, $toDate){
                $query->whereBetween('date', [$fromDate, $toDate]);
            });
        }else{
//            $query  = $query->whereDate('created_at', Carbon::today());
        }

        $query = $query->orderBy('item_id', 'ASC');

        $query = $query->whereHas('purchases', function ($query){
            return $query->orderBy('date', 'DESC');
        });

        $data['full_url'] =  $request->fullUrl().($request->fullUrl() == $request->url() ? "?" : "&");
        $data['report_title'] = "Product Purchase Report";
//        $query = $query->groupBy(function ($val) {
//            return Carbon::parse($val->created_at)->format('Y-m-d');
//        });
//        $query  = $query->groupBy('created_date');

        if($request->type=="print" || $request->type=="download") {

            $data['purchases']  = $query->get();
            $data = $this->company($data);
            if ($request->type == "print") {
                return View('member.reports.print_product_purchase_report', $data);
            } else if ($request->type == "download") {
                $pdf = PDF::loadView('member.reports.print_product_purchase_report', $data);
                $file_name = file_name_generator("Product_Purchase_Report_");
                return $pdf->download($file_name);
            }
        } else {
            $data['purchases']  = $query->paginate(20)->appends(request()->query());
            return view('member.reports.product_purchase_report', $data);
        }
    }

    public function supplier_purchase_report(Request $request){

        $inputs = $request->all();
        $data['products'] = Item::get()->pluck('item_name', 'id');
        $data['suppliers'] = SupplierOrCustomer::get()->pluck('name', 'id');
        $data['item'] = $data['sharer'] = null;
        $query = new PurchaseDetail();

        if(!empty($inputs['item_id']))
        {
            $data['item'] = $inputs['item_id'];
            $query = $query->where('item_id', $inputs['item_id']);
        }

        if(!empty($inputs['supplier_id'])){
            $data['sharer'] = $inputs['supplier_id'];
            $query = $query->whereHas('purchases', function ($query) use($inputs){
                $query->where('supplier_id', $inputs['supplier_id']);
            });
        }

        $data['branches'] = Branch::get()->pluck('display_name', 'id');
        $query = $this->branch_check($query, $request);

        $fromDate = $toDate = '';
        if( !empty($inputs['from_date']) )
            $inputs['from_date'] = $fromDate = db_date_format($inputs['from_date']);

        if( !empty($inputs['to_date']) )
            $inputs['to_date'] = $toDate = db_date_format($inputs['to_date']);

        if( empty($fromDate) && (!empty($toDate)) ) {

            $query = $query->whereHas('purchases', function ($query) use($toDate){
                $query->whereDate('date','<=', $toDate);
            });
        }elseif( (!empty($fromDate)) && empty($toDate) ) {

            $query = $query->whereHas('purchases', function ($query) use($fromDate){
                $query->whereDate('date','>=', $fromDate);
            });
        }elseif ( $fromDate !='' && $toDate != '' ) {

            $query = $query->whereHas('purchases', function ($query) use($fromDate, $toDate){
                $query->whereBetween('date', [$fromDate, $toDate]);
            });
        }else{
//            $query  = $query->whereDate('created_at', Carbon::today());
        }

        $query = $query->orderBy('item_id', 'ASC');

        $query = $query->whereHas('purchases', function ($query){
            return $query->orderBy('date', 'DESC');
        });

        $data['full_url'] =  $request->fullUrl().($request->fullUrl() == $request->url() ? "?" : "&");
        $data['report_title'] = " Purchase Report By Supplier";

        if($request->type=="print" || $request->type=="download") {
            $data['purchases'] = $query->get();
            $data = $this->company($data);

            if ($request->type == "print") {
                return View('member.reports.print_supplier_purchase_report', $data);
            } else if ($request->type == "download") {
                $pdf = PDF::loadView('member.reports.print_supplier_purchase_report', $data);
                $file_name = file_name_generator("Supplier_Purchase_Report_");
                return $pdf->download($file_name);
            }
        }
        else {
            $data['purchases']  = $query->paginate(20)->appends(request()->query());
            return view('member.reports.purchase-from-supplier', $data);
        }
    }

    public function sale_report(Request $request)
    {
        $inputs = $request->all();
        $query = new Sale();

        $fromDate = $toDate = '';
        if( !empty($inputs['from_date']) )
            $inputs['from_date'] = $fromDate = db_date_format($inputs['from_date']);

        if( !empty($inputs['to_date']) )
            $inputs['to_date'] = $toDate = db_date_format($inputs['to_date']);

        if( empty($fromDate) && (!empty($toDate)) ) {
            $query->whereDate('date','<=', $toDate);
        }elseif( (!empty($fromDate)) && empty($toDate) ) {
            $query->whereDate('date','>=', $fromDate);
        }elseif ( $fromDate !='' && $toDate != '' ) {
            $query->whereBetween('date', [$fromDate, $toDate]);
        }else{
//            $query  = $query->whereDate('created_at', Carbon::today());
        }
        $data['branches'] = Branch::get()->pluck('display_name', 'id');
        $query = $this->branch_check($query, $request);


        $data['report_title'] = " Sale Report";
        $data['full_url'] =  $request->fullUrl().($request->fullUrl() == $request->url() ? "?" : "&");

        if($request->type=="print"|| $request->type=="download") {

            $data['sales']  = $query->get();
            $data = $this->company($data);

            if ($request->type == "print") {
                return view('member.reports.print_sale_report', $data);
            } else if ($request->type == "download") {
                $pdf = PDF::loadView('member.reports.print_sale_report', $data);
                $file_name = file_name_generator("Sale_Report_");
                return $pdf->download($file_name);
            }
        } else {
            $data['sales']  = $query->paginate(20)->appends(request()->query());
            return view('member.reports.sale_report', $data);
        }
    }

    public function purchase_report(Request $request)
    {
        $inputs = $request->all();
        $query = new Purchase();

        $fromDate = $toDate = '';
        if( !empty($inputs['from_date']) )
            $inputs['from_date'] = $fromDate = db_date_format($inputs['from_date']);

        if( !empty($inputs['to_date']) )
            $inputs['to_date'] = $toDate = db_date_format($inputs['to_date']);

        if( empty($fromDate) && (!empty($toDate)) ) {
            $query->whereDate('date','<=', $toDate);
        }elseif( (!empty($fromDate)) && empty($toDate) ) {
            $query->whereDate('date','>=', $fromDate);
        }elseif ( $fromDate !='' && $toDate != '' ) {
            $query->whereBetween('date', [$fromDate, $toDate]);
        }else{
//            $query  = $query->whereDate('created_at', Carbon::today());
        }

        $data['branches'] = Branch::get()->pluck('display_name', 'id');
        $query = $this->branch_check($query, $request);

        $data['full_url'] =  $request->fullUrl().($request->fullUrl() == $request->url() ? "?" : "&");

        $data['report_title'] = "Purchase Report";

        if($request->type=="print"|| $request->type=="download") {

            $data['purchases'] = $query->get();
            $data = $this->company($data);

            if ($request->type == "print") {
                return view('member.reports.print_purchase_report', $data);
            } else if ($request->type == "download") {
                $pdf = PDF::loadView('member.reports.print_purchase_report', $data);
                $file_name = file_name_generator("Purchase_Report_");
                return $pdf->download($file_name);
            }
        } else {
            $data['purchases']  = $query->paginate(20)->appends(request()->query());
            return view('member.reports.purchase_report', $data);
        }
    }

    private function search_data(Request $request, $get=false)
    {
        $inputs = $request->all();

        $multiple_condition = [];

        if( !empty($inputs['transaction_code']))
            $multiple_condition['transaction_code'] = $request->transaction_code;

        $query =  $this->transaction_full_details(
            $member = true, $company=false, $page = 20, $tr_payment=false, $updated_user=false,
            $tr_category=false, $select_column="", $group_tr_code=false, $group_tr_type=false,
            $order = 'DESC', '','','', $multiple_condition, $get
        );

        $query = $this->transactionDateCheck($query, $inputs);


        if( !empty($inputs['head_account_type_id']))
        {
            $query = $query->where('transaction_details.account_type_id', $request->head_account_type_id);
        }

        if( !empty($inputs['d_head_account_type_id']))
        {
            $query = $query->where('transaction_details.account_type_id', $request->d_head_account_type_id);
        }

        if( !empty($inputs['group_account_type_id']))
        {
            $query = $query->where('transaction_details.account_type_id', $request->group_account_type_id);
        }

        if( !empty($inputs['sub_head_account_type_id']))
        {
            $query = $query->where('transaction_details.account_type_id', $request->sub_head_account_type_id);
        }

        return $query;
    }

    private function transactionTypeCheck($query, $type)
    {
        if($type=='Income'){
            $query = $query->where('transactions.transaction_method', 'Income');
        }elseif($type=='Expense'){
            $query = $query->where('transactions.transaction_method', 'Expense');
        }elseif($type=='Journal Entry') {
            $query = $query->where('transactions.transaction_method', 'Journal Entry');
        }elseif($type=='Transfer'){
            $query = $query->where('transactions.transaction_method', 'Transfer');
        }

        return $query;
    }

    private function transactionDateCheck($query, $inputs)
    {
        $fromDate = $toDate = '';
        if( !empty($inputs['from_date']) )
            $inputs['from_date'] = $fromDate = db_date_format($inputs['from_date']);

        if( !empty($inputs['to_date']) )
            $inputs['to_date'] = $toDate = db_date_format($inputs['to_date']);


        if( empty($fromDate) && (!empty($toDate)) ) {

            $query = $query->where('transactions.date','<=', $toDate);
        }elseif( (!empty($fromDate)) && empty($toDate) ) {

            $query = $query->where('transactions.date','>=', $fromDate);
        }elseif ( $fromDate !='' && $toDate != '' ) {

            $query = $query->whereBetween('transactions.date',[$fromDate, $toDate]);
        }

        return $query;
    }

    private function saleDateCheck($query, $inputs)
    {
        $fromDate = $toDate = '';
        if( !empty($inputs['from_date']) )
            $inputs['from_date'] = $fromDate = db_date_format($inputs['from_date']);

        if( !empty($inputs['to_date']) )
            $inputs['to_date'] = $toDate = db_date_format($inputs['to_date']);


        if( empty($fromDate) && (!empty($toDate)) ) {

            $query = $query->whereDate('created_at','<=', $toDate);
        }elseif( (!empty($fromDate)) && empty($toDate) ) {

            $query = $query->whereDate('created_at','>=', $fromDate);
        }elseif ( $fromDate !='' && $toDate != '' ) {

            $query = $query->whereDate('created_at','>=', $fromDate);
            $query = $query->whereDate('created_at','<=', $toDate);
        }

        return $query;
    }

    private function purchaseDateCheck($query, $inputs)
    {
        $fromDate = $toDate = '';
        if( !empty($inputs['from_date']) )
            $inputs['from_date'] = $fromDate = db_date_format($inputs['from_date']);

        if( !empty($inputs['to_date']) )
            $inputs['to_date'] = $toDate = db_date_format($inputs['to_date']);


        if( empty($fromDate) && (!empty($toDate)) ) {

            $query = $query->whereDate('created_at','<=', $toDate);
        }elseif( (!empty($fromDate)) && empty($toDate) ) {

            $query = $query->whereDate('created_at','>=', $fromDate);
        }elseif ( $fromDate !='' && $toDate != '' ) {

            $query = $query->whereDate('created_at','>=', $fromDate);
            $query = $query->whereDate('created_at','<=', $toDate);
        }

        return $query;
    }

    private function check_print_system()
    {
        $settings = Setting::where('key', '=', 'print_page_setup')->first();

        if($settings)
        {
            $this->print_system = $settings->value;
        }else{
            $this->print_system = '';
        }

        $this->print_system = $this->print_system == "default" ? '' : $this->print_system;
    }

    private function branch_check($query, $request)
    {
        if(Auth::user()->hasRole(['super-admin', 'admin', 'developer'])){

            if(isset($request->branch_id))
                $query = $query->where('branch_id', $request->branch_id);
        }else if(Auth::user()->hasRole(['user'])){

            $query = $query->where('branch_id', Auth::user()->branch_id);
        }

        return $query;
    }
}
