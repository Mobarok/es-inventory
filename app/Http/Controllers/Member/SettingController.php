<?php

namespace App\Http\Controllers\Member;

use App\Models\Company;
use App\Models\FiscalYear;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{

    public function company_fiscal_year()
    {
        $data = [];
        $data['companies'] = Company::active()->authMember()->get()->pluck('company_name', 'id');
        $data['company_fiscal_years'] = Company::active()->authMember()->paginate(10);
        $data['fiscal_year'] = FiscalYear::active()->authMember()->get()->pluck('fiscal_year_details', 'id');
        return view('member.settings.company_fiscal_year', $data);
    }

    public function set_company_fiscal_year(Request $request)
    {
        $rules = [
            'company_id' => 'required',
            'fiscal_year_id' => 'required'
        ];

        $this->validate($request, $rules);

        $company  = Company::authMember()->where('id', $request->company_id)->first();
        $company->fiscal_year_id = $request->fiscal_year_id;
        $company->save();
        $status = ['type' => 'success', 'message' => 'Successfully Fiscal Year Add Company '.$company->company_name];
        return back()->with('status', $status);
    }

    public function general_settings()
    {
        $settings = Setting::where('key', '=', 'print_page_setup')->first();

        $data = [];
        $data['print_page_option'] = [
            'pos' => "POS Print",
            'default' => "Default A4 Print"
        ];
        $data['page_setup'] = $settings;

        return view('member.settings.general_settings', $data);
    }

    public function set_print_page_setup(Request $request)
    {
        $settings = Setting::where('key', '=', 'print_page_setup')->first();

        $input = [];
        if( isset($request->print_page_setup) && !$settings)
        {
            $input['key'] = 'print_page_setup';
            $input['value'] = $request->print_page_setup;
            Setting::create($input);
        }else{
            $input['value'] = $request->print_page_setup;
            $settings->update($input);
        }

        $status = ['type' => 'success', 'message' => 'Successfully Print Page setup Done'];
        return back()->with('status', $status);
    }


}
