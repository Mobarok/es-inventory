<?php

namespace App\Http\Controllers\Member;

use App\Http\Traits\FileUploadTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Country;
use App\Models\FiscalYear;
use Illuminate\Support\Facades\Storage;
use App\DataTables\CompanyDataTable;

class CompanyController extends Controller
{
    use FileUploadTrait;

        /*
         * TODO: Company Controller fully functional check in later.
         */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CompanyDataTable $dataTable)
    {
        return $dataTable->render('member.companies.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['countries'] = Country::all()->pluck('countryName', 'id');

        $data['fiscal_year'] = FiscalYear::active()->authMember()->get()->pluck('fiscal_year_details','id');
        // "<pre>";print_r($data['fiscal_year']);die();
        return view('member.companies.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, $this->validationRules());

        $inputs = $request->all();        

        if ($request->hasFile('logo')) {
             $id_photo = $request->file('logo');
    //         $destinationPath='storage/company_logo';
    //         $ext=$id_photo->getClientOriginalExtension();
    //         $file_name=file_name_generator().'.'.$ext;
    //         $upload =$id_photo->move($destinationPath,$file_name);

                $upload = $this->fileUpload($id_photo, '/company_logo/', null);

                if (!$upload)
                {
                    $status = ['type' => 'danger', 'message' => 'Image Must be JPG, JPEG, PNG, GIF'];
                    return back()->with('status', $status)->withInput();
                }
                $inputs['logo'] = $upload;

         }

     Company::create($inputs);


     $status = ['type' => 'success', 'message' => 'Successfully Added.'];

     return back()->with('status', $status);
 }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['model'] = Company::findOrFail($id);
        $data['countries'] = Country::all()->pluck('countryName', 'id');

        $data['fiscal_year'] = FiscalYear::active()->authMember()->get()->pluck('fiscal_year_details','id');

        return view('member.companies.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::findOrFail($id);
        $this->validate($request, $this->validationRules());

        $inputs = $request->all();

        if ($request->hasFile('logo')) {
            $id_photo = $request->file('logo');

            $upload = $this->fileUpload($id_photo, '/company_logo/', null);

            if (!$upload)
            {
                $status = ['type' => 'danger', 'message' => 'Image Must be JPG, JPEG, PNG, GIF'];
                return back()->with('status', $status);
            }
                $inputs['logo'] = $upload;

            }

            $company->update($inputs);

            $status = ['type' => 'success', 'message' => 'Successfully Updated'];

        return back()->with('status', $status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    private function validationRules(){

        $rules = [
            'company_name' => 'required',
            'phone' => 'required',
            'email' => 'required'
        ];

        return $rules;
    }
}
