<?php

namespace App\Http\Controllers\Member;

use App\DataTables\TransactionDataTable;
use App\DataTables\TransactionTransferDataTable;
use App\Http\Traits\FileUploadTrait;
use App\Http\Traits\PaymentDetailTrait;
use App\Http\Traits\TransactionHistoryTrait;
use App\Http\Traits\TransactionTrait;
use App\Models\AccountType;
use App\Models\CashOrBankAccount;
use App\Models\MediaStore;
use App\Models\PaymentMethod;
use App\Models\SupplierOrCustomer;
use App\Models\TransactionCategory;
use App\Models\Transactions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;
use Barryvdh\Snappy;

class TransactionController extends Controller
{
    use TransactionTrait, TransactionHistoryTrait, FileUploadTrait, PaymentDetailTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( TransactionDataTable $dataTable)
    {
        return $dataTable->render('member.transaction.index');
    }

    public function manage_daily_sheet()
    {
        $check = $this->assignedCheck();
        if( $check !='Success')
            return $check;

        $data['type'] = "Manage Daily Sheet";
        $data['transaction_categories_id'] = AccountType::get()->pluck('account_code', 'id')->toArray();
        $data['transaction_categories'] =  AccountType::get()->pluck('display_name', 'id');
        $data['accounts'] = CashOrBankAccount::authMember()->active()->get();
        $data['sharers'] = SupplierOrCustomer::authMember()->active()->get()->pluck('name','id');
        $data['payment_methods'] = PaymentMethod::active()->get()->pluck('name','id');
        return view('member.transaction.manage_daily_sheet', $data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type)
    {
        $check = $this->assignedCheck();
        if( $check !='Success')
            return $check;

        $data = [];
        if( $type == 'Income')
        {
            $data['type'] = 'Income';
//            $data['transaction_categories'] = TransactionCategory::income()->get();
        }else{
            $data['type'] = 'Expense';
//            $data['transaction_categories'] = TransactionCategory::Expense()->get();
        }
        $data['transaction_categories_id'] = AccountType::get()->pluck('account_code', 'id')->toArray();
        $data['transaction_categories'] =  AccountType::get()->pluck('display_name', 'id');
        $data['accounts'] = CashOrBankAccount::authMember()->active()->get();
        $data['sharers'] = SupplierOrCustomer::authMember()->active()->get()->pluck('name','id');
        $data['payment_methods'] = PaymentMethod::active()->get()->pluck('name','id');
        return view('member.transaction.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */

    public function store(Request $request)
    {

        $this->validate($request, $this->validationRules());

        $inputs = $request->all();

        $total_entry = count($request->account_type_id);

        $account = CashOrBankAccount::find($request->cash_or_bank_id);

        if($account->account_type_id==null)
        {
            $status = ['type' => 'danger', 'message' => 'Unable to save. There is no AccountType assign in Cash and Bank Account'];
            return back()->with('status', $status);
        }

        $inputs['account_name'] = $account->title;
        $inputs['current_balance'] = empty($account->current_balance) ? 0.00 : $account->current_balance;

        if(isset($request->supplier_id)) {
            $sharer = SupplierOrCustomer::find($request->supplier_id);
            $inputs['sharer_name'] = $sharer->name;

        }else{
            $inputs['sharer_name'] = '';
        }

        $status['message'] = '';
        DB::beginTransaction();
        try{

            $inputs['transaction_code'] = transaction_code_generate();

            $save_transaction = new Transactions();
            $save_transaction->transaction_code = $inputs['transaction_code'];
            $save_transaction->cash_or_bank_id = $inputs['cash_or_bank_id'];
            $save_transaction->supplier_id = $inputs['supplier_id'];
            $save_transaction->date = db_date_format($inputs['date']);
            $save_transaction->amount = array_sum($inputs['amount']);
            $save_transaction->notation = $inputs['notation'];
            $save_transaction->transaction_method = $inputs['transaction_method'];
            $save_transaction->save();

            $inputs['ip_address'] = $request->ip();
            $inputs['browser_history'] = $request->header('User-Agent');
            $inputs['flag'] = "add";

            $account_type_id = $request->account_type_id;
            $payment_method_id = $request->payment_method;


            $amount = $request->amount;
            $description = $request->description;

            $total_amount = 0;
            $credit_account_name = '';

            $inputs['transaction_id'] = $save_transaction->id;

            for($i=0; $i<$total_entry; $i++)
            {
                $account_type = AccountType::find($account_type_id[$i]);
                if(!$account_type)
                {
                    $status['message'] = 'Account Name not found';
                }

                $credit_account_name = !empty($credit_account_name) ? $credit_account_name.' & ' : '';
                $credit_account_name = $credit_account_name.$account_type->display_name;


                $inputs['to_account_name'] = $account_type->display_name;
                $inputs['account_group_id'] = $account_type->parent_id;
                $inputs['amount'] = $amount[$i];
                $inputs['description'] = $description[$i];
                $inputs['account_type_id'] = $account_type_id[$i];
                $inputs['payment_method_id'] = $payment_method_id[$i];

                // Create Transaction Debit Amount
                $transactionDr = $this->createDebitAmount($inputs);
                $inputs['transaction_details_id'] = $transactionDr->id;

                // Add Payment Details using payment Method based
                /*
                 *  TODO: This will be use in Later for Re-Conciliation
                 */
//                $this->checkingPaymentMethod($inputs, $i);

                $total_amount = $total_amount+$amount[$i];
                $type = ($inputs['transaction_method'] == 'Expense') ? 'credit' : 'debit';

                // Create Transaction History Per transaction
                $last_balance = $this->historyCreate($inputs, $type);
                $inputs['current_balance'] = $last_balance;

            }

            // Update Transaction Total Amount Balance
            $save_transaction->amount = $total_amount;
            $save_transaction->save();

            // Update Cash and Bank Account Balance
            $this->bankAccountBalanceUpdate($inputs['transaction_method'], $account, $total_amount);

            if( isset($request->supplier_id)&& $sharer->customer_type=='supplier' )
                $inputs['current_balance'] = empty($sharer->supplier_current_balance) ? 0.00 : $sharer->supplier_current_balance;
            else
                $inputs['current_balance'] = empty($sharer->customer_current_balance) ? 0.00 : $sharer->customer_current_balance;


            $inputs['amount'] = $total_amount;
            $inputs['account_type_id'] = $account->account_type_id;
            $inputs['account_group_id'] = $account->account_type->account_type_id;

            $inputs['to_account_name'] = $credit_account_name;

            // Create Transaction Credit Amount
            $transactionCr = $this->createCreditAmount($inputs);
            $inputs['transaction_details_id'] = $transactionCr->id;
            $type = ($inputs['transaction_method'] == 'Expense') ? 'debit' : 'credit';

            // Create Transaction History Per transaction
            $this->historyCreate($inputs, $type);

            if(isset($request->supplier_id)) {
                $this->sharerBalanceUpdate($inputs['transaction_method'], $sharer, $total_amount);
            }

            $status = ['type' => 'success', 'message' => $status['message']."<br/>".ucfirst($request->transaction_method).' save Successfully'];

            $status['transaction_code'] = $inputs['transaction_code'];


            if($request->hasFile('attach'))
            {
                $file = $request->file('attach');

                $mediaData = $this->fileUploadWithDetails($file, $inputs['transaction_code'], null);
                $mediaData['model_id'] = $save_transaction->id;
                $mediaData['use_model'] = "Transactions";
                if($mediaData['file_name'] == null)
                {
                    $status = ['type' => 'success', 'message' => $status['message'].' But unable to Save File'];

                }else{

                    MediaStore::create($mediaData);

                }
            }

        }catch (\Exception $e){

            $status = ['type' => 'danger', 'message' => 'Unable to save '.ucfirst($request->transaction_method)];
            DB::rollBack();
        }

        DB::commit();


        if(!isset($status['transaction_code']))
        {
            return back()->with('status', $status);
        }else{
            return view('member.transaction.store-transaction')->with('status', $status);
        }

    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listTransfer( TransactionTransferDataTable $dataTable)
    {
        return $dataTable->render('member.transaction.transfer-list');
    }


    /**
     * Show the form for creating a transfer.
     *
     * @return \Illuminate\Http\Response
     */
    public function transfer()
    {
        $check = $this->assignedCheck();
        if( $check !='Success')
            return $check;

        $data = [];
        $data['accounts'] = CashOrBankAccount::authMember()->active()->get()->pluck('title','id');
        $data['payment_methods'] = PaymentMethod::active()->get()->pluck('name','id');

        return view('member.transaction.transfer', $data);
    }

    public function saveTransfer(Request $request)
    {
        $rules = [
            'amount' => 'required',
            'from_cash_or_bank_id' => 'required',
            'to_cash_or_bank_id' => 'required',
            'date' => 'required'
        ];

        $this->validate($request, $rules);


        $from_account = CashOrBankAccount::find($request->from_cash_or_bank_id);
        $to_account = CashOrBankAccount::find($request->to_cash_or_bank_id);


        if( empty($from_account->account_type_id) || empty($to_account->account_type_id)){
            $status = ['type' => 'danger', 'message' => 'There is no Account Type for Transaction from or To. Please add Those'];
            return back()->with('status', $status);
        }

        DB::beginTransaction();
        try{

            $inputs = $request->all();
            $inputs['transaction_code'] = transaction_code_generate();
            $inputs['amount']  = $request->amount;

            $inputs['date'] = db_date_format($request->date);
            $inputs['account_name'] = $from_account->title;
            $inputs['current_balance'] = empty($from_account->current_balance) ? 0.00 : $from_account->current_balance;
            $inputs['to_account_name'] = $to_account->title;
            $inputs['transaction_method'] = "Transfer";
            $inputs['notation'] = $inputs['description'];
            $inputs['cash_or_bank_id'] = $request->from_cash_or_bank_id;
            $transaction =  Transactions::create($inputs);

            $inputs['transaction_id'] = $transaction->id;
            $inputs['account_type_id'] = $from_account->account_type_id;
            $inputs['account_group_id'] = $from_account->account_type->account_type_id;
            $inputs['payment_method_id'] = $request->payment_method_id;


            $inputs['ip_address'] = $request->ip();
            $inputs['browser_history'] = $request->header('User-Agent');
            $inputs['flag'] = 'Add';

            $transactionCr = $this->createCreditAmount($inputs);
            $inputs['transaction_details_id'] = $transactionCr->id;
            $this->historyCreate($inputs, 'Credit');

            $inputs['cash_or_bank_id'] = $request->to_cash_or_bank_id;
            $inputs['account_type_id'] = $to_account->account_type_id;
            $inputs['account_group_id'] = $to_account->account_type->account_type_id;

            $transactionDr = $this->createDebitAmount($inputs);
            $inputs['transaction_details_id'] = $transactionDr->id;
            $this->historyCreate($inputs, 'Debit');

            /*
                 *  TODO: This will be use in Later for Re-Conciliation
                 */
//                $this->checkingPaymentMethod($inputs, $i);


            $status = ['type' => 'success', 'message' => 'Transfer save Successfully'];

        }catch (\Exception $e){

            $status = ['type' => 'danger', 'message' => 'Unable to save Transfer'];
        DB::rollBack();
        }

        DB::commit();

        return back()->with('status', $status);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('member.transaction.store-transaction');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    protected function validationRules()
    {
        $rules = [
            'amount' => 'required|array|min:1',
            'amount.*' => 'required|string',
            'cash_or_bank_id' => 'required',
            'date' => 'required'
        ];

        return $rules;
    }

    protected function assignedCheck(){

        if(empty(Auth::user()->company_id))
        {
            $status = ['type'=>'danger','message'=>'Company Is not assign for You. Please confirm your company Name'];
            return redirect('member/set-users-company')->with('status', $status);
        }

        if(empty(Auth::user()->company->fiscal_year_id))
        {
            $status = ['type'=>'danger','message'=>'Fiscal Year is not set for your Company. Please confirm your Fiscal Year'];
            return redirect('member/company-fiscal-year')->with('status', $status);
        }

        return "Success";

    }



}
