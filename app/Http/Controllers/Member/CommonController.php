<?php

namespace App\Http\Controllers\Member;

use App\Models\AccountType;
use App\Models\SupplierOrCustomer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommonController extends Controller
{
    public function saveAccountType(Request $request)
    {
        $rules = [
            'name' => 'required|unique:account_types,name'
        ];

        $customMessages = [
            'name.required' => 'Display Name field is required and Unique.'
        ];

        $inputs = $request->all();
        $inputs['name'] = $request['name'] = snake_case($request->display_name);

        $this->validate($request, $rules, $customMessages);

        $save = AccountType::create($inputs);

        $data = array(
            "okResponse" => true,
            "status" => 1,
            "value" => null,
            "values" => $save
        );

        header('Content-Type: application/json');

        echo json_encode($data);
    }

    public function payerSearch(Request $request)
    {
        $customer_type = $request->customer_type;
        $sharers = new SupplierOrCustomer();
        if($customer_type=="customer")
        {
            $sharers = $sharers->onlyCustomers();
        }elseif($customer_type=="both") {
            $sharers = $sharers->where('customer_type','both');
        }elseif($customer_type=="supplier") {
            $sharers = $sharers->onlySuppliers();
        }

        $sharers  = $sharers->get()->pluck('name', 'id');

        $data = array(
            "okResponse" => true,
            "status" => 1,
            "value" => null,
            "values" => $sharers
        );

        header('Content-Type: application/json');

        echo json_encode($data);

    }

    public function saveCustomer(Request $request)
    {
        $rules = [
            'name' => 'required|unique:suppliers_or_customers,name',
            'phone' => 'required|unique:suppliers_or_customers,phone',
            'address' => 'required',
        ];

        $customMessages = [
            'name.required' => 'Name field is required and Unique.',
            'phone.required' => 'Phone field is required and Unique.'
        ];

        $inputs = $request->all();

        $this->validate($request, $rules, $customMessages);

        $save = SupplierOrCustomer::create($inputs);

        $data = array(
            "okResponse" => true,
            "status" => 1,
            "value" => null,
            "values" => $save
        );

        header('Content-Type: application/json');

        echo json_encode($data);
    }
}
