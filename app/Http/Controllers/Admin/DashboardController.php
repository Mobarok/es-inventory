<?php

namespace App\Http\Controllers\Admin;

use App\Models\Item;
use App\Models\Purchase;
use App\Models\Sale;
use App\Models\Transactions;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(){

        $data['total_products'] = Item::authMember()->authCompany()->count();
        $data['total_purchases'] = Purchase::authMember()->authCompany()->count();
        $data['total_sales'] = Sale::authMember()->authCompany()->count();
        $data['total_users'] = User::membersUser()->active()->count();

        $data['total_transport_cost'] = Purchase::authMember()->authCompany()->sum('transport_cost');
        $data['total_unload_cost'] = Purchase::authMember()->authCompany()->sum('unload_cost');

        $data['total_purchase_amount'] = Purchase::authMember()->authCompany()->sum('total_amount');
        $data['total_sales_amount'] = Sale::authMember()->authCompany()->sum('grand_total');
        $data['today_sales_amount'] = Sale::authMember()->authCompany()->whereDate('created_at', Carbon::today())->sum('grand_total');
        $data['today_purchase_amount'] = Purchase::authMember()->authCompany()->whereDate('created_at', Carbon::today())->sum('total_amount');

        $data['today_due'] = Sale::authMember()->authCompany()->whereDate('created_at', Carbon::today())->sum('due');
        $data['total_due'] = Sale::authMember()->authCompany()->sum('due');
        $data['today_out_standing'] = Purchase::authMember()->authCompany()->whereDate('created_at', Carbon::today())->sum('due_amount');
        $data['total_out_standing'] = Purchase::authMember()->authCompany()->sum('due_amount');

//        dd($data);
        return view('admin.dashboard.index', $data);
    }
}
