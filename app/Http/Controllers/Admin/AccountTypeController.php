<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\AccountTypesDataTable;
use App\Http\Traits\AccountTypeTrait;
use App\Models\AccountType;
use App\Models\GLAccountClass;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AccountTypeController extends Controller
{
    use AccountTypeTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AccountTypesDataTable $dataTable)
    {
        return $dataTable->render('admin.account_types.index');
    }

    public function group()
    {
        $data['account_types'] = AccountType::group()->paginate(20);
        return view('admin.account_types.group', $data);
    }

    public function heads()
    {
        $data['account_types'] = $this->select_heads();

        $data['title'] = 'Heads';
        return view('admin.account_types.heads', $data);
    }

    public function sub_heads()
    {
        $data['account_types'] = $this->select_sub_heads();

        $data['title'] = 'Sub-Heads';
        return view('admin.account_types.heads', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['accounts'] = AccountType::active()->get()->pluck('display_name','id');
        $data['account_groups'] = GLAccountClass::active()->get()->pluck('name','id');

        return view( 'admin.account_types.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['name'] = snake_case($request->display_name);

        $this->validate($request, $this->rules());

        AccountType::create($request->all());

        $status = ['type' => 'success', 'message' => 'Successfully Added.'];

        return back()->with('status', $status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['modal'] = $modal = AccountType::find($id);

        if(!$modal)
        {
            $status = ['type' => 'danger', 'message' => 'Unable to Find data.'];
            return back()->with('status', $status);
        }

        $data['accounts'] = AccountType::active()->get()->pluck('display_name','id');
        $data['account_groups'] = GLAccountClass::active()->get()->pluck('name','id');

        return view( 'admin.account_types.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $modal = AccountType::find($id);
        $request['name'] = snake_case($request->display_name);
        if(!$modal)
        {
            $status = ['type' => 'danger', 'message' => 'Unable to Find data.'];
            return back()->with('status', $status);
        }

        $this->validate($request, $this->rules($id));

        $modal->update($request->all());

        $status = ['type' => 'success', 'message' => 'Successfully Updated.'];

        return back()->with('status', $status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $modal = AccountType::findOrFail($id);
        $modal->delete();

        return response()->json([
            'data' => [
                'message' => 'Successfully deleted'
            ]
        ], 200);
    }

    private function rules($id='')
    {
        if(is_null($id))
        {
            $data =  [
                'name' => 'required|unique:account_types,name',
                'display_name' => 'required|unique:account_types,display_name',
            ];
        }else{
            $data =  [
                'name' => 'required|unique:account_types,name,'.$id,
                'display_name' => 'required|unique:account_types,display_name,'.$id,
            ];
        }

        return $data;
    }
}
