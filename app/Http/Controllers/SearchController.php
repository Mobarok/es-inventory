<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Purchase;
use App\Models\PurchaseDetail;
use App\Models\Sale;
use App\Models\SaleDetails;
use App\Models\SupplierOrCustomer;
use App\Models\SupplierPurchases;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    public function item_details(Request $request){

        $id = $request->item_id;
        $supplierId = $request->supplier_id;
        $item = Item::find($id);

//        $item = $item->with(['stock_details' => function ($query) {
//            $query->where('branch_id', '=', Auth::user()->branch_id);
//        }])->first();

        $data = [];
        $data = $item;
        $data['stock'] = !empty($item->stock_details) ? $item->stock_details->stock : 0;
        $data['supplier_purchases'] = $this->supplier_last_purchase_by_item($supplierId, $id);

        if(isset($request->purchase_id))
        {
            $data['last_qty'] = $this->purchase_items($request->purchase_id, $id);
        }

        if($item)
            $data['status'] = 'success';
        else
            $data['status'] = 'failure';

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function sale_item_details(Request $request){

        $id = $request->item_id;
        $product_code = $request->product_code;
        $item = Item::where('id', $id);

        if(isset($product_code))
        {
            $item = $item->orWhere('productCode', $product_code);
        }
        $item->first();

        $item = $item->with(['stock_details' => function ($query) {
            $query->where('branch_id', '=', Auth::user()->branch_id);
        }])->first();

        $data = [];
        $data = $item;
        $data['stock'] = !empty($item->stock_details) ? $item->stock_details->stock : 0;
//        $data['price'] = PurchaseDetail::where('item_id', $id)->orderBy('created_at', 'desc')->select('price')->first();
        $data['last_sale_qty'] = $this->sale_items($id);

//        if(isset($request->sale_id))
//        {
//            $data['last_qty'] = $this->sale_items($request->sale_id, $id);
//        }

        if($item)
            $data['status'] = 'success';
        else
            $data['status'] = 'failure';

        header('Content-Type: application/json');
        echo json_encode($data);
    }


    public function sale_bags(Request $request){

        $id = $request->item_id;
        $item = Item::where('id', $id)->first();

        $item = $item->with(['stock_details' => function ($query) {
            $query->where('branch_id', '=', Auth::user()->branch_id);
        }])->first();

        $data = [];
        $data = $item;
        $data['stock'] = !empty($item->stock_details) ? $item->stock_details->stock : 0;

        if($item)
            $data['status'] = 'success';
        else
            $data['status'] = 'failure';

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function supplier_last_purchase_by_item($supplierId, $itemId)
    {
       $supplierPurchases =  SupplierPurchases::authMember()->authCompany()
                            ->where('supplier_id', $supplierId)
                            ->where('item_id', $itemId)
                            ->where('branch_id', '=', Auth::user()->branch_id)
                            ->orderBy('created_at', 'DESC')
                            ->first();

       return $supplierPurchases;
    }

    public function supplier_info(Request $request){

        $data['supplier'] = $supplier = SupplierOrCustomer::find($request->supplier_id);
        $data['last_purchase_amount'] = Purchase::where('supplier_id', $request->supplier_id)->latest()->first();


        if($supplier)
            $data['status'] = 'success';
        else
            $data['status'] = 'failure';

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function purchase_items( $purchase_id, $item_id)
    {
        $purchase = PurchaseDetail::where('purchase_id',$purchase_id)
            ->where('item_id',$item_id)->where('branch_id', '=', Auth::user()->branch_id)->sum('qty');

        return $purchase;
    }

    public function customer_info(Request $request){

        $data['customer'] = $customer = SupplierOrCustomer::find($request->customer_id);
        $data['last_sale_amount'] = Sale::where('customer_id', $request->customer_id)->latest()->first();


        if($customer)
            $data['status'] = 'success';
        else
            $data['status'] = 'failure';

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function sale_items($item_id)
    {
        $sales = SaleDetails::where('item_id',$item_id)->where('branch_id', '=', Auth::user()->branch_id)->orderBy('id', 'DESC')->groupBy('sale_id')->sum('qty');

        return $sales;
    }
}
