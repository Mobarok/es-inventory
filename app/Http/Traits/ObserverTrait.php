<?php
/**
 * Created by PhpStorm.
 * User: R-Creation
 * Date: 3/10/2019
 * Time: 2:50 PM
 */

namespace App\Http\Traits;


use App\Models\AccountType;
use App\Models\Branch;
use App\Models\CashOrBankAccount;
use App\Models\Category;
use App\Models\Company;
use App\Models\DueCollectionHistory;
use App\Models\FiscalYear;
use App\Models\Item;
use App\Models\JournalEntryDetail;
use App\Models\Member;
use App\Models\Membership;
use App\Models\Purchase;
use App\Models\PurchaseDetail;
use App\Models\ReturnPurchase;
use App\Models\Sale;
use App\Models\SaleDetails;
use App\Models\SaleReturn;
use App\Models\Stock;
use App\Models\StockHistory;
use App\Models\StockReport;
use App\Models\SupplierOrCustomer;
use App\Models\SupplierPurchases;
use App\Models\TrackShoppingBags;
use App\Models\TransactionHistory;
use App\Models\Transactions;
use App\Models\Unit;
use App\Models\User;
use App\Observers\AccountTypesObserver;
use App\Observers\BranchObserver;
use App\Observers\CashAndBankAccountObserver;
use App\Observers\CategoryObserver;
use App\Observers\CompanyObserver;
use App\Observers\DueCollectionHistoryObserver;
use App\Observers\FiscalYearObserver;
use App\Observers\ItemObserver;
use App\Observers\JournalEntryDetailsObserver;
use App\Observers\MemberObserver;
use App\Observers\MembershipObserver;
use App\Observers\PurchaseDetailsObserver;
use App\Observers\PurchaseObserver;
use App\Observers\ReturnPurchaseObserver;
use App\Observers\SaleDetailsObserver;
use App\Observers\SaleObserver;
use App\Observers\SaleReturnObserver;
use App\Observers\StockHistoryObserver;
use App\Observers\StockObserver;
use App\Observers\StockReportObserver;
use App\Observers\SupplierOrCustomerObserver;
use App\Observers\SupplierPurchaseObserver;
use App\Observers\TrackShoppingBagsObserver;
use App\Observers\TransactionHistoryObserver;
use App\Observers\TransactionObserver;
use App\Observers\UnitObserver;
use App\Observers\UserObserver;

Trait ObserverTrait
{
    /**
     * Get model observers
     */
    public function getObservers()
    {
        // Model Observers
        User::observe(UserObserver::class);
        Member::observe(MemberObserver::class);
        Membership::observe(MembershipObserver::class);
        Company::observe(CompanyObserver::class);
        SupplierOrCustomer::observe(SupplierOrCustomerObserver::class);
        FiscalYear::observe(FiscalYearObserver::class);
        Transactions::observe(TransactionObserver::class);
        TransactionHistory::observe(TransactionHistoryObserver::class);
        CashOrBankAccount::observe(CashAndBankAccountObserver::class);
        AccountType::observe(AccountTypesObserver::class);
        JournalEntryDetail::observe(JournalEntryDetailsObserver::class);
        Unit::observe(UnitObserver::class);
        Stock::observe(StockObserver::class);
        Category::observe(CategoryObserver::class);
        Purchase::observe(PurchaseObserver::class);
        Item::observe(ItemObserver::class);
        ReturnPurchase::observe(ReturnPurchaseObserver::class);
        SupplierPurchases::observe(SupplierPurchaseObserver::class);
        Sale::observe(SaleObserver::class);
        TrackShoppingBags::observe(TrackShoppingBagsObserver::class);
        SaleReturn::observe(SaleReturnObserver::class);
        StockHistory::observe(StockHistoryObserver::class);
        DueCollectionHistory::observe(DueCollectionHistoryObserver::class);
        Branch::observe(BranchObserver::class);
        StockReport::observe(StockReportObserver::class);
        SaleDetails::observe(SaleDetailsObserver::class);
        PurchaseDetail::observe(PurchaseDetailsObserver::class);
    }
}
