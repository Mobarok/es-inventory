<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 3/24/2019
 * Time: 10:50 AM
 */

namespace App\Http\Traits;


use App\Models\TransactionDetail;
use App\Models\Transactions;

trait TransactionTrait
{

    /**
     * @param array $inputs
     * @return TransactionDetail
     */

    public function createDebitAmount($inputs = array())
    {

        $save_transaction_details = new TransactionDetail();

        $type = ($inputs['transaction_method'] == 'Income' || $inputs['transaction_method'] == 'Transfer') ? 'Debited to' : '';
        $type = (empty($type) && $inputs['transaction_method'] == 'Expense') ? 'Credited from' : $type;

        $save_transaction_details->date = db_date_format($inputs['date']);
        $save_transaction_details->amount = $inputs['amount'];
        $save_transaction_details->transaction_id = $inputs['transaction_id'];
        $save_transaction_details->account_type_id = $inputs['account_type_id'];
//        $save_transaction_details->against_account_type_id = isset($inputs['against_account_type_id']) ? $inputs['against_account_type_id'] : '';
//        $save_transaction_details->against_account_name = isset($inputs['against_account_name']) ? $inputs['against_account_name'] : '';
        $save_transaction_details->description = $inputs['description'];
        $save_transaction_details->short_description = $inputs['account_name'].' '.$type." ".$inputs['to_account_name'];
        $save_transaction_details->short_description = !empty($inputs['sharer_name']) ? $save_transaction_details->short_description." by ".$inputs['sharer_name']:'';
        !empty($inputs['payment_method_name']) ? $save_transaction_details->short_description." using ".$inputs['payment_method_name']:'';
        $inputs['transaction_method'] != 'Transfer' ? $save_transaction_details->account_type_id = $inputs['account_type_id'] : '';

       if( isset($inputs['transaction_type']) ){
        $save_transaction_details->transaction_type = $inputs['transaction_type'];

       }else{
           // For Expense, it will type CR Else DR
           $save_transaction_details->transaction_type = ( $save_transaction_details->transaction_method == 'Income' || $inputs['transaction_method'] == 'Transfer')  ? 'dr' : 'cr';
       }

        $save_transaction_details->payment_method_id = $inputs['payment_method_id'];
        $save_transaction_details->save();

        return $save_transaction_details;
    }

    /**
     * @param array $inputs
     * @return TransactionDetail
     */

    public function createCreditAmount($inputs = array())
    {
        $save_transaction_details = new TransactionDetail();

        $type = $inputs['transaction_method'] == 'Expense' ? 'Debited to' : '';
        $type = (empty($type) && ($inputs['transaction_method'] == 'Income' || $inputs['transaction_method'] == 'Transfer')) ? 'Credited from' : $type;

        $save_transaction_details->date = db_date_format($inputs['date']);
        $save_transaction_details->amount = $inputs['amount'];
        $save_transaction_details->transaction_id = $inputs['transaction_id'];
        $save_transaction_details->account_type_id = $inputs['account_type_id'];
//        $save_transaction_details->against_account_type_id = isset($inputs['against_account_type_id']) ? $inputs['against_account_type_id'] : '';
//        $save_transaction_details->against_account_name = isset($inputs['against_account_name']) ? $inputs['against_account_name'] : '';
        $save_transaction_details->short_description = $inputs['to_account_name']." ".$type." ".$inputs['account_name'];
        !empty($inputs['sharer_name']) ? $save_transaction_details->short_description." by ".$inputs['sharer_name']:'';
        !empty($inputs['payment_method_name']) ? $save_transaction_details->short_description." using ".$inputs['payment_method_name']:'';
        $save_transaction_details->description = $inputs['description'];
        $inputs['transaction_method'] != 'Transfer' ? $save_transaction_details->account_type_id = $inputs['account_type_id'] : '';

        if(!isset($inputs['transaction_type'])) {
            // For Expense, it will type DR Else CR
            $save_transaction_details->transaction_type = ($save_transaction_details->transaction_method == 'Income' || $inputs['transaction_method'] == 'Transfer') ? 'cr' : 'dr';
        }else{
            $save_transaction_details->transaction_type = $inputs['transaction_type'];
        }

        $save_transaction_details->payment_method_id = $inputs['payment_method_id'];
        $save_transaction_details->save();

        return $save_transaction_details;
    }


    /**
     * @param $transaction_method
     * @param $sharer
     * @param $amount
     * @return true/False
     */
    public function sharerBalanceUpdate($transaction_method, $sharer, $amount)
    {
        if($transaction_method == 'Income')
        {
            if($sharer->customer_type=='supplier')
                $sharer->supplier_current_balance = $sharer->supplier_current_balance-$amount;
            else
                $sharer->customer_current_balance = $sharer->customer_current_balance-$amount;
        }

        if($transaction_method == 'Expense')
        {
            if($sharer->customer_type=='supplier')
                $sharer->supplier_current_balance = $sharer->supplier_current_balance+$amount;
            else
                $sharer->customer_current_balance = $sharer->customer_current_balance+$amount;
        }

        if($transaction_method == 'Purchases')
        {
            $sharer->supplier_current_balance = $sharer->supplier_current_balance-$amount;
        }

        if($transaction_method == 'Purchase Update')
        {
            $sharer->supplier_current_balance = $sharer->supplier_current_balance+$amount;
        }

        if($transaction_method == 'Sales')
        {
            $sharer->customer_current_balance = $sharer->customer_current_balance-$amount;
        }

        if($transaction_method == 'Sale Update')
        {
            $sharer->customer_current_balance = $sharer->customer_current_balance+$amount;
        }


       return $sharer->save();
    }


    /**
     * @param $transaction_method
     * @param $mainAccount
     * @param $total_amount
     * @return true/False
     */
    public function bankAccountBalanceUpdate($transaction_method, $mainAccount, $total_amount)
    {
        if($transaction_method == 'Income')
        {
            $mainAccount->current_balance  = $mainAccount->current_balance+$total_amount;
        }

        if($transaction_method == 'Expense')
        {
            $mainAccount->current_balance  = $mainAccount->current_balance-$total_amount;
        }

        if($transaction_method == 'Purchases')
        {
            $mainAccount->current_balance  = $mainAccount->current_balance-$total_amount;
        }

        if($transaction_method == 'Sales')
        {
            $mainAccount->current_balance  = $mainAccount->current_balance+$total_amount;
        }


        if($transaction_method == 'Purchase Update')
        {
            $mainAccount->current_balance  = $mainAccount->current_balance-$total_amount;
        }

        if($transaction_method == 'Sale Update')
        {
            $mainAccount->current_balance  = $mainAccount->current_balance+$total_amount;
        }

        return $mainAccount->save();

    }
}
