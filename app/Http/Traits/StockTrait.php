<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 5/13/2019
 * Time: 11:47 AM
 */

namespace App\Http\Traits;


use App\Models\Item;
use App\Models\Stock;
use App\Models\StockHistory;
use App\Models\StockReport;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

trait StockTrait
{
    public function createStockHistory($item_id, $qty, $flag="stock in")
    {
        $data = [];
        $data['item_id'] = $item_id;
        $data['stock'] = $qty;
        $data['flag'] = $flag;
        StockHistory::create($data);
    }

    public function stockIn($item_id, $qty)
    {
        $data = [];
        $data['item_id'] = $item_id;

        $stockCheck = Stock::where('item_id', $item_id)->where('branch_id', Auth::user()->branch_id)->first();
        if(!$stockCheck)
        {
            $data['stock'] = $qty;
            Stock::create($data);
        }else{
            $data['stock'] = $qty+$stockCheck->stock;
            $stockCheck->update($data);
        }
    }

    public function stockOut($item_id, $qty)
    {
        $stockCheck = Stock::where('item_id', $item_id)
            ->where('branch_id', Auth::user()->branch_id)
            ->first();

        if(!$stockCheck) {
            $data = [];
            $data['stock'] = $stockCheck->stock - $qty;
            $stockCheck->update($data);
        }
    }


    public function stock_report($item_id, $qty, $type)
    {
        $stockReport = StockReport::where('date', Carbon::today())->where('item_id', $item_id)->where('branch_id', Auth::user()->branch_id)->first();

        $currentStock = Stock::where('item_id', $item_id)->where('branch_id', Auth::user()->branch_id)->first();
        $item = Item::find($item_id);

        $stock = [];
//        dd(count($stockReport));
        if($stockReport == null) {
            $stock['item_id'] = $item_id;
            $stock['product_code'] = $item->productCode;
            $stock['product_name'] = $item->item_name;
            $stock['opening_stock'] = isset($currentStock->stock) ? $currentStock->stock : 0;
            $stock['date'] = Carbon::today();
        }

        switch ($type)
        {
            case "purchase":
                $stock['purchase_qty'] = $stockReport ? $stockReport->purchase_qty+$qty : $qty;
                break;
            case "purchase return":
                $stock['purchase_return_qty'] = $stockReport ? $stockReport->purchase_return_qty+$qty : $qty;
                break;
            case "sale":
                $stock['sale_qty'] = $stockReport ? $stockReport->sale_qty+$qty : $qty;
                break;
            case "sale return":
                $stock['sale_return_qty'] = $stockReport ? $stockReport->sale_return_qty+$qty : $qty;
                break;
//            default:
//                $stock['sale_qty'] = 0;
//                $stock['purchase_qty'] = 0;
        }


        if(!$stockReport)
        {
            StockReport::create($stock);
        }else{
            $stockReport->update($stock);
        }
    }
}
