<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 9/3/2019
 * Time: 11:44 AM
 */

namespace App\Http\Traits;


use Illuminate\Support\Facades\Auth;

trait BranchTrait
{
    public function branch_assign_check(){

        if( Auth::check() && Auth::user()->branch_id)
        {
            return 1;
        }

        return 0;
    }
}
