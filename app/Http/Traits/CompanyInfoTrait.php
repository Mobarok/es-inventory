<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 7/8/2019
 * Time: 3:15 PM
 */

namespace App\Http\Traits;


use App\Models\Company;
use Illuminate\Support\Facades\Auth;

trait CompanyInfoTrait
{

    private function company($data=[]){

        $data['company_logo']= !empty(Auth::user()->company->logo) ? Auth::user()->company->company_logo_path : "";
        $data['company_name']= Auth::user()->company->company_name;
        $data['company_address']= Auth::user()->company->address;
        $data['company_city']= Auth::user()->company->city;
        $data['company_country']= Auth::user()->company->country->countryName;
        $data['company_phone']= Auth::user()->company->phone;
        $data['company_email']= Auth::user()->company->email;

        return $data;
    }


    private function envCompany($data=[]){

        $company = Company::where('status', 'active')->first();
        $data['company_name']= $company->company_name;
        $data['company_address']= $company->address;
        $data['company_city']= $company->city;
        $data['company_country']= $company->country->countryName;
        $data['company_phone']= $company->phone;
        $data['company_email']= $company->email;
        $data['company_logo']= asset('public/storage/company_logo/'. $company->logo);

        return $data;
    }
}
