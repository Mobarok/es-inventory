<?php

namespace App\DataTables;

use App\Http\Traits\TransactionDetailsTrait;
use App\Models\Transactions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Services\DataTable;

class TransactionDataTable extends DataTable
{
    use TransactionDetailsTrait;
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function ajax()
    {
        return datatables()
            ->eloquent($this->query())
            ->editColumn('cash_or_bank_id', function($model){
                return isset($model->cash_or_bank_id) ? $model->cash_or_bank_account->title : '';
            })
            ->editColumn('account_name', function($model){
                $data = isset($model->transaction_details->transaction_category_id) ? $model->transaction_details->transaction_category->display_name : '';
                $data = isset($model->transaction_details->account_type_id) ? $model->transaction_details->account_type->display_name : $data;

                return $data;
            })
            ->editColumn('debit', function($model){
                return $model->transaction_details[0]->transaction_type == 'dr' ? $model->transaction_details[0]->amount : create_money_format(0);
            })
            ->editColumn('credit', function($model){
                return $model->transaction_details[0]->transaction_type == 'cr' ? $model->transaction_details[0]->amount : create_money_format(0);
            })
//            ->addColumn('action', function($model) {
//                return view('common._action-button', ['model' => $model, 'route' => 'member.transaction']);
//            })
//            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = Transactions::authMember()->authCompany()
        ->withoutTransfer()
        ->orderBy('id','desc');

//        $query = $this->transaction_full_details(
//            $member = true, $company=true, $page = '', $tr_payment=false, $updated_user=false, $tr_category=false,
//            $select_column="tr-data", $group_tr_code=true, $group_tr_type=false, $order = 'DESC', $condition='!=',
//            $condition_col='transaction_method', $value='Transfer'
//        );

//        $query = DB::table('transactions')
//                    ->where('member_id', Auth::user()->member_id)
//                    ->groupBy("transaction_code")
//                    ->latest();

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'name' => 'date',
                'data' => 'date_format',
                'title' => 'Date',
                'className' => 'transaction-date',
                'orderable' => true,
                'searchable' => true
            ],
            'transaction_code',
//            [
//                'name' => 'transaction_method',
//                'data' => 'transaction_method',
//                'title' => 'Type'
//            ],
//            [
//                'name' => 'transaction_type',
//                'data' =>'uc_transaction_type',
//                'title' => 'Transaction Type'
//            ],
            'cash_or_bank_id' => [
                'name' => 'transaction_from',
                'title' => 'Transaction From',
                'orderable' => false,
                'searchable' => false
            ],
            'account_type_id'=>[
                'name' => 'account_name',
                'data' => 'account_name',
                'title' => 'Account Name',
                'orderable' => false,
                'searchable' => false
            ],
            [
                'name' => 'debit',
                'data' => 'debit',
                'title' => 'Debit',
                'className' => 'text-right',
                'orderable' => false,
                'searchable' => false
            ],
            [
                'name' => 'credit',
                'data' => 'credit',
                'title' => 'Credit',
                'className' => 'text-right',
                'orderable' => false,
                'searchable' => false
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Transaction_' . date('YmdHis');
    }
}
