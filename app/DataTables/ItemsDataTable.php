<?php

namespace App\DataTables;

use App\Models\Item;

use Milon\Barcode\DNS1D;
use Yajra\DataTables\Services\DataTable;

class ItemsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function ajax()
    {
        return datatables()
            ->eloquent($this->query())
            ->editColumn('product_code', function($row){
                $barcode = !empty($row->productCode) ? DNS1D::getBarcodeHTML( $row->productCode, "C128", 1,33) : '';
                return $barcode;
            })
            ->editColumn('product_image', function($row){
                $logo = !empty($row->product_image) ? '<img src="'.$row->product_image_path.'" width="50px" />' : '';
                return $logo;
            })
            ->addColumn('action', function($modal) {
                return view('common._action-button', ['model' => $modal, 'route' => 'member.items']);
            })
            ->rawColumns(['product_code', 'product_image','status', 'action'])
            ->make(true);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Item $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {

        $query = Item::latest()->with('category');

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['name' => 'id', 'data' => 'id',  'title' => "ID" ],
            'product_code'=>['searchable'=>false,'orderable'=>false],
            'product_image',
            ['name' => 'item_name', 'data' => 'item_name',  'title' => "Product Name" ],
            ['name' => 'category.name', 'data' => 'category.display_name',  'title' => "Product Category" ],
            'unit'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Items_' . date('YmdHis');
    }
}
