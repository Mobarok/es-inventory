@extends('layouts.app')

@section('content')
           <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <img src="{{ asset('public/login_assets/img/icon/icon-plane.png') }}" alt="">
                    </div>
                    <div class="col-lg-6 p-t-100">
                        <div class="text-white">
                            <h1>Welcome to {{  human_words(config('app.name')) }} Inventory</h1>
                            <p class="s-18 p-t-b-20 font-weight-lighter">Hey Soldier welcome back signin now we are waiting for you</p>
                        </div>
                        <form method="POST" action="{{ route('login') }}">

                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group has-icon"><i class="icon-envelope-o"></i>
                                        <input id="email" type="email" class="form-control form-control-lg no-b {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                               placeholder="Email Address/Username"  name="email" value="{{ old('email') }}" required autofocus>
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group has-icon"><i class="icon-user-secret"></i>
                                        <input id="password" type="password" class="form-control form-control-lg no-b {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                               placeholder="Password"  name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-success btn-lg btn-block">
                                        {{ __('Login') }}
                                    </button>
                                    @if (Route::has('password.request'))
                                        <a class="forget-pass text-white" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
@endsection
