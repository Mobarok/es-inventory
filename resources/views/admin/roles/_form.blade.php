<?php
/**
 * Created by PhpStorm.
 * User: R-Creation
 * Date: 2/27/2019
 * Time: 4:25 PM
 */

?>
 
    <div class="col-md-7">
        <div class="form-group">
            <label for="name">Name <span class="text-red"> * </span> </label>
            {!! Form::text('name',null,['id'=>'name','class'=>'form-control','placeholder'=>'Enter Name', 'required']); !!}
        </div>
        <div class="form-group">
            <label for="display_name">Display Name <span class="text-red"> * </span> </label>
            {!! Form::text('display_name',null,['id'=>'display_name','class'=>'form-control','placeholder'=>'Enter Display Name', 'required']); !!}
        </div>

        <div class="form-group">
            <label for="description">Description  </label>
            {!! Form::textarea('description',null,['id'=>'description','class'=>'form-control','placeholder'=>'Enter description']); !!}
        </div>
    </div>
    <div class="col-md-12">
        <hr>
        <h4>Permissions</h4>
        <hr>

        @foreach($permissions as $key => $permission)
            @if($permission->name != 'super-admin' || Auth::user()->hasRole(['super-admin']))
            <div class="col-md-3 ">
                <label class="permission-label">
                        @if(isset($rolePermissions))
                            <input type="checkbox" class="" value="{{ $permission->id }}" {{ in_array($permission->id, $rolePermissions) ? 'checked' : '' }} name="permissions[]"> {{ $permission->display_name }}
                        @else
                            <input type="checkbox" class="" value="{{ $permission->id }}" name="permissions[]"> {{ $permission->display_name }}
                        @endif
                </label>
            </div>
            @endif
        @endforeach
    </div>


