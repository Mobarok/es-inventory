<?php
/**
 * Created by PhpStorm.
 * User: R-Creation
 * Date: 2/27/2019
 * Time: 4:25 PM
 */

?>


@push('styles')

    <link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/select2/dist/css/select2.min.css')}}">
@endpush

    <div class="col-md-7">
        <div class="form-group">
            <label for="name">Name <span class="text-red"> * </span> </label>
            {!! Form::text('display_name', null, ['id'=>'display_name','class'=>'form-control','placeholder'=>'Enter Name', 'required']); !!}
        </div>
        <div class="form-group">
            <label for="parent_id">GL Account Parent Group  </label>
            {!! Form::select('parent_id', $accounts, null,['id'=>'parent_id','class'=>'form-control select2','placeholder'=>'Enter GL Account Group Name']); !!}
        </div>
        <div class="form-group">
            <label for="class_id">GL Account Class  <span class="text-red"> * </span>  </label>
            {!! Form::select('class_id', $account_groups, null,['id'=>'class_id','class'=>'form-control select2','placeholder'=>'Enter GL Account Class']); !!}
        </div>
        <div class="form-group">
            <label for="status">Status <span class="text-red"> * </span>  </label>
            {!! Form::select('status',['active'=>'Active', 'inactive'=>'Inactive'], null,['id'=>'status','class'=>'form-control', 'required']); !!}
        </div>
    </div>

@push('scripts')

    <script src="{{ asset('public/adminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <script type="text/javascript">

        // var date = new Date();
        $(function () {

            $('.select2').select2();
        });
        </script>
    @endpush
