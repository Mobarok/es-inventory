<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 2/26/2019
 * Time: 11:39 AM
 */
?>
<header class="main-header">
@php
    $route = Auth::user()->hasRole(['admin','super-admin','developer']) ? 'admin' : 'member';
    $route .= '.dashboard';
@endphp
    <!-- Logo -->
    <a href="{{ route($route) }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><?= header_shortname(config('app.name', '') ) ?></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">{{ human_words(config('app.name')) }}</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div style="float: left" class="rmm style">
            <ul>
                <li>
                    @php
                        $route = Auth::user()->hasRole(['admin','super-admin','developer']) ? 'admin' : 'member';
                        $route .= '.dashboard';
                    @endphp
                    <a href="{{ route($route) }}">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                @if(Auth::user()->can(['super-admin', 'create-sale','list-sales', 'sales-due-list','sales-return-list']))

                <li>
                    <a href="#">
                        <i class="fa fa-shopping-cart"></i>
                        <span> Sale </span>
                    </a>
                    <ul>
                        @if(Auth::user()->can(['super-admin', 'create-sale']))
                            <li><a href="{{ route('member.sales.create') }}">New Sale</a></li>
                        @endif
                        @if(Auth::user()->can(['super-admin', 'list-sales']))
                            <li><a href="{{ route('member.sales.index') }}">List Sales</a></li>
                        @endif
                        @if(Auth::user()->can(['super-admin', 'sales-due-list']))
                            <li><a href="{{ route('member.sales.due_list') }}">Due Sales</a></li>
                        @endif
                        @if(Auth::user()->can(['super-admin', 'sales-return-list']))
                            <li><a href="{{ route('member.sales.sales_return_list') }}">Sales Return </a></li>
                        @endif
                    </ul>
                </li>
                @endif

                @if(Auth::user()->can(['super-admin', 'create-purchase','list-purchase', 'purchase-due-list','purchase-return-list']))
                <li >
                    <a href="#">
                        <i class="fa fa-shopping-bag"></i>
                        <span> Purchase </span>
                    </a>
                    <ul >
                        @if(Auth::user()->can(['super-admin', 'create-purchase']))
                            <li><a href="{{ route('member.purchase.create') }}">New Purchase</a></li>
                        @endif
                        @if(Auth::user()->can(['super-admin', 'list-purchase']))
                            <li><a href="{{ route('member.purchase.index') }}">List Purchases</a></li>
                        @endif
                        @if(Auth::user()->can(['super-admin', 'purchase-due-list']))
                            <li><a href="{{ route('member.purchase.due_list') }}">Due Purchases</a></li>
                        @endif
                        @if(Auth::user()->can(['super-admin', 'purchase-return-list']))
                            <li><a href="{{ route('member.purchase_return.index') }}">Purchases Return </a></li>
                        @endif
                    </ul>
                </li>

                @endif
            </ul>
        </div>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
{{--                <li class="dropdown messages-menu">--}}
{{--                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
{{--                        <i class="fa fa-envelope-o"></i>--}}
{{--                        <span class="label label-success">4</span>--}}
{{--                    </a>--}}
{{--                    <ul class="dropdown-menu">--}}
{{--                        <li class="header">You have 4 messages</li>--}}
{{--                        <li>--}}
{{--                            <!-- inner menu: contains the actual data -->--}}
{{--                            <ul class="menu">--}}
{{--                                <li><!-- start message -->--}}
{{--                                    <a href="#">--}}
{{--                                        <div class="pull-left">--}}
{{--                                            <img src="{{ asset('public/adminLTE/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">--}}
{{--                                        </div>--}}
{{--                                        <h4>--}}
{{--                                            Support Team--}}
{{--                                            <small><i class="fa fa-clock-o"></i> 5 mins</small>--}}
{{--                                        </h4>--}}
{{--                                        <p>Why not buy a new awesome theme?</p>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <!-- end message -->--}}
{{--                                <li>--}}
{{--                                    <a href="#">--}}
{{--                                        <div class="pull-left">--}}
{{--                                            <img src="{{ asset('public/adminLTE/dist/img/user3-128x128.jpg') }}" class="img-circle" alt="User Image">--}}
{{--                                        </div>--}}
{{--                                        <h4>--}}
{{--                                            adminLTE Design Team--}}
{{--                                            <small><i class="fa fa-clock-o"></i> 2 hours</small>--}}
{{--                                        </h4>--}}
{{--                                        <p>Why not buy a new awesome theme?</p>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="#">--}}
{{--                                        <div class="pull-left">--}}
{{--                                            <img src="{{ asset('public/adminLTE/dist/img/user4-128x128.jpg') }}" class="img-circle" alt="User Image">--}}
{{--                                        </div>--}}
{{--                                        <h4>--}}
{{--                                            Developers--}}
{{--                                            <small><i class="fa fa-clock-o"></i> Today</small>--}}
{{--                                        </h4>--}}
{{--                                        <p>Why not buy a new awesome theme?</p>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="#">--}}
{{--                                        <div class="pull-left">--}}
{{--                                            <img src="{{ asset('public/adminLTE/dist/img/user3-128x128.jpg') }}" class="img-circle" alt="User Image">--}}
{{--                                        </div>--}}
{{--                                        <h4>--}}
{{--                                            Sales Department--}}
{{--                                            <small><i class="fa fa-clock-o"></i> Yesterday</small>--}}
{{--                                        </h4>--}}
{{--                                        <p>Why not buy a new awesome theme?</p>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="#">--}}
{{--                                        <div class="pull-left">--}}
{{--                                            <img src="{{ asset('public/adminLTE/dist/img/user4-128x128.jpg') }}" class="img-circle" alt="User Image">--}}
{{--                                        </div>--}}
{{--                                        <h4>--}}
{{--                                            Reviewers--}}
{{--                                            <small><i class="fa fa-clock-o"></i> 2 days</small>--}}
{{--                                        </h4>--}}
{{--                                        <p>Why not buy a new awesome theme?</p>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        <li class="footer"><a href="#">See All Messages</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
                <!-- Notifications: style can be found in dropdown.less -->
{{--                <li class="dropdown notifications-menu">--}}
{{--                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
{{--                        <i class="fa fa-bell-o"></i>--}}
{{--                        <span class="label label-warning">10</span>--}}
{{--                    </a>--}}
{{--                    <ul class="dropdown-menu">--}}
{{--                        <li class="header">You have 10 notifications</li>--}}
{{--                        <li>--}}
{{--                            <!-- inner menu: contains the actual data -->--}}
{{--                            <ul class="menu">--}}
{{--                                <li>--}}
{{--                                    <a href="#">--}}
{{--                                        <i class="fa fa-users text-aqua"></i> 5 new members joined today--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="#">--}}
{{--                                        <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the--}}
{{--                                        page and may cause design problems--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="#">--}}
{{--                                        <i class="fa fa-users text-red"></i> 5 new members joined--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="#">--}}
{{--                                        <i class="fa fa-shopping-cart text-green"></i> 25 sales made--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="#">--}}
{{--                                        <i class="fa fa-user text-red"></i> You changed your username--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        <li class="footer"><a href="#">View all</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
                <!-- Tasks: style can be found in dropdown.less -->
{{--                <li class="dropdown tasks-menu">--}}
{{--                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
{{--                        <i class="fa fa-flag-o"></i>--}}
{{--                        <span class="label label-danger">9</span>--}}
{{--                    </a>--}}
{{--                    <ul class="dropdown-menu">--}}
{{--                        <li class="header">You have 9 tasks</li>--}}
{{--                        <li>--}}
{{--                            <!-- inner menu: contains the actual data -->--}}
{{--                            <ul class="menu">--}}
{{--                                <li><!-- Task item -->--}}
{{--                                    <a href="#">--}}
{{--                                        <h3>--}}
{{--                                            Design some buttons--}}
{{--                                            <small class="pull-right">20%</small>--}}
{{--                                        </h3>--}}
{{--                                        <div class="progress xs">--}}
{{--                                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"--}}
{{--                                                 aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">--}}
{{--                                                <span class="sr-only">20% Complete</span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <!-- end task item -->--}}
{{--                                <li><!-- Task item -->--}}
{{--                                    <a href="#">--}}
{{--                                        <h3>--}}
{{--                                            Create a nice theme--}}
{{--                                            <small class="pull-right">40%</small>--}}
{{--                                        </h3>--}}
{{--                                        <div class="progress xs">--}}
{{--                                            <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar"--}}
{{--                                                 aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">--}}
{{--                                                <span class="sr-only">40% Complete</span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <!-- end task item -->--}}
{{--                                <li><!-- Task item -->--}}
{{--                                    <a href="#">--}}
{{--                                        <h3>--}}
{{--                                            Some task I need to do--}}
{{--                                            <small class="pull-right">60%</small>--}}
{{--                                        </h3>--}}
{{--                                        <div class="progress xs">--}}
{{--                                            <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar"--}}
{{--                                                 aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">--}}
{{--                                                <span class="sr-only">60% Complete</span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <!-- end task item -->--}}
{{--                                <li><!-- Task item -->--}}
{{--                                    <a href="#">--}}
{{--                                        <h3>--}}
{{--                                            Make beautiful transitions--}}
{{--                                            <small class="pull-right">80%</small>--}}
{{--                                        </h3>--}}
{{--                                        <div class="progress xs">--}}
{{--                                            <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar"--}}
{{--                                                 aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">--}}
{{--                                                <span class="sr-only">80% Complete</span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <!-- end task item -->--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        <li class="footer">--}}
{{--                            <a href="#">View all tasks</a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
                <!-- User Account: style can be found in dropdown.less -->


                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ asset('public/adminLTE/dist/img/user2-160x160.jpg') }}" class="user-image" alt="User Image">
                        <span class="hidden-xs username">{{ Auth::user()->full_name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{ asset('public/adminLTE/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">

                            <p>
                                {{ Auth::user()->full_name }}
{{--                                <small>Member since Nov. 2012</small>--}}
                            </p>
                        </li>
                        <!-- Menu Body -->
{{--                        <li class="user-body">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-xs-4 text-center">--}}
{{--                                    <a href="#">Followers</a>--}}
{{--                                </div>--}}
{{--                                <div class="col-xs-4 text-center">--}}
{{--                                    <a href="#">Sales</a>--}}
{{--                                </div>--}}
{{--                                <div class="col-xs-4 text-center">--}}
{{--                                    <a href="#">Friends</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- /.row -->--}}
{{--                        </li>--}}
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-default btn-flat" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Sign Out') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
{{--                <li>--}}
{{--                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>--}}
{{--                </li>--}}
            </ul>
        </div>
    </nav>
</header>
