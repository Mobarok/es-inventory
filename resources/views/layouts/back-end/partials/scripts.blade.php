<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 2/26/2019
 * Time: 11:40 AM
 */
?>

<!-- jQuery 3 -->
<script src="{{ asset('public/adminLTE/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('public/adminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('public/adminLTE/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<!-- DataTables -->
<script src="{{ asset('public/adminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/adminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>


<!-- daterangepicker -->
<script src="{{ asset('public/adminLTE/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/adminLTE/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('public/adminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('public/adminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>

<!-- SlimScroll -->
<script src="{{ asset('public/adminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('public/adminLTE/bower_components/fastclick/lib/fastclick.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

<!-- public/adminLTE App -->
<script src="{{ asset('public/adminLTE/dist/js/adminlte.min.js') }}"></script>
<!-- public/adminLTE for demo purposes -->
<script src="{{ asset('public/adminLTE/dist/js/demo.js') }}"></script>
<script src="{{ asset('public/adminLTE/jquery.printPage.js') }}"></script>

@stack('scripts')



<script>

    $('input[type=number]').attr('min', 0);
    $('.input-number').keypress(function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && ( charCode != 46 && (charCode < 48 || charCode > 57)))
            return false;
        return true;
    });

    function company(){
        var html = '';
            @if(isset( Auth::user()->company))

        var logo = "{{  emptyCheck(Auth::user()->company->company_logo_path)}}";
        var company = "{{ emptyCheck(Auth::user()->company->company_name) }}";
        var address = "{{ emptyCheck(str_replace( array( "\n", "\r" ), array( "\\n", "\\r" ), Auth::user()->company->address)) }}";
        var city = "{{ emptyCheck(Auth::user()->company->city) }}";
        var country = "{{ emptyCheck(Auth::user()->company->country->countryName) }}";
        var phone = "{{ emptyCheck(Auth::user()->company->country->phone) }}";
        var email = "{{ emptyCheck(Auth::user()->company->country->email) }}";

        html = "<div> <div style='text-align: right !important; float: left; width:25%' >" +
            "<img width='130px !important' src='"+logo+"' alt='"+company+"'/> </div> <div  style='text-align: center !important; float: left; padding-left: 13%; width:50%' >" +
            "<h3>"+company+"</h3>" +
            "<p>"+address+" <br/> "+city+", "+country+"</p>" +
            "<p>"+phone+"</p>" +
            "<p>"+email+"</p>" +
            "</div></div>";
        @endif

            return html;
    }

    $('#print').click( function () {
        var header = company();
        var printContents = $('#custom-print').html();
        var originalContents = $('body').html();

        $('body').html(header+printContents);
        window.print();
        $('body').html(originalContents);
    });


    var $body = $('body');

    $body.on('click', '.delete-confirm', function () {
        var $this = $(this);
        bootbox.confirm("Are you sure? " ,  function(result) {

            if (result){
                $.ajax({
                    url: $this.data('target'),
                    type: "DELETE",
                    dataType: "json",
                    success: function (data) {
                        bootbox.alert("Delete successfully completed",
                            function () {
                                location.reload();
                            });
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        var response = xhr.responseJSON;
                        bootbox.alert("OOP! Sorry Response Denied");
                    }
                });
            }else{
                bootbox.alert("OOP! Sorry Response Denied");
            }
        });
    });


    $('#btn-print, .btn-print').printPage();

</script>


