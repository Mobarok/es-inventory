<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 2/26/2019
 * Time: 11:40 AM
 */
?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('public/adminLTE/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p class="username">{{ Auth::user()->full_name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
    {{--<form action="#" method="get" class="sidebar-form">--}}
    {{--<div class="input-group">--}}
    {{--<input type="text" name="q" class="form-control" placeholder="Search...">--}}
    {{--<span class="input-group-btn">--}}
    {{--<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>--}}
    {{--</button>--}}
    {{--</span>--}}
    {{--</div>--}}
    {{--</form>--}}
    <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                @php
                    $route = Auth::user()->hasRole(['admin','super-admin','developer']) ? 'admin' : 'member';
                    $route .= '.dashboard';
                @endphp
                <a href="{{ route($route) }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>

            @if(Auth::user()->can(['super-admin', 'create-purchase','list-purchase', 'purchase-due-list','purchase-return-list']))
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-shopping-cart"></i>
                    <span> Purchase </span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @if(Auth::user()->can(['super-admin', 'create-purchase']))
                    <li><a href="{{ route('member.purchase.create') }}">New Purchase</a></li>
                    @endif
                    @if(Auth::user()->can(['super-admin', 'list-purchase']))
                    <li><a href="{{ route('member.purchase.index') }}">List Purchases</a></li>
                    @endif
                    @if(Auth::user()->can(['super-admin', 'purchase-due-list']))
                    <li><a href="{{ route('member.purchase.due_list') }}">Due Purchases</a></li>
                    @endif
                    @if(Auth::user()->can(['super-admin', 'purchase-return-list']))
                    <li><a href="{{ route('member.purchase_return.index') }}">Purchases Return </a></li>
                    @endif
                </ul>
            </li>
            @endif

            @if(Auth::user()->can(['super-admin', 'create-sale','list-sales', 'sales-due-list','sales-return-list']))
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-shopping-bag"></i>
                    <span> Sale </span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @if(Auth::user()->can(['super-admin', 'create-sale']))
                    <li><a href="{{ route('member.sales.create') }}">New Sale</a></li>
                    @endif
                    @if(Auth::user()->can(['super-admin', 'list-sales']))
                    <li><a href="{{ route('member.sales.index') }}">List Sales</a></li>
                    @endif
                    @if(Auth::user()->can(['super-admin', 'sales-due-list']))
                    <li><a href="{{ route('member.sales.due_list') }}">Due Sales</a></li>
                    @endif
                    @if(Auth::user()->can(['super-admin', 'sales-return-list']))
                    <li><a href="{{ route('member.sales.sales_return_list') }}">Sales Return </a></li>
                    @endif
                </ul>
            </li>
            @endif

            @if(Auth::user()->can(['super-admin', 'add-supplier','list-suppliers']))
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Manage Supplier</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">

                    @if(Auth::user()->can(['super-admin', 'add-supplier' ]))
                        <li><a href="{{ route('member.sharer.supplier_list') }}"><i class="fa fa-users"></i> Suppliers</a></li>
                    @endif
                    @if(Auth::user()->can(['super-admin', 'list-suppliers']))
                        <li><a href="{{ route('member.sharer.create', 'supplier') }}"><i class="fa fa-plus"></i> Add Supplier</a></li>
                    @endif
                </ul>
            </li>
            @endif

            @if(Auth::user()->can(['super-admin', 'add-customer','list-customer']))
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Manage Customers</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @if(Auth::user()->can(['super-admin', 'list-customer']))
                        <li><a href="{{ route('member.sharer.customer_list') }}"><i class="fa fa-users"></i> Customers</a></li>
                    @endif
                    @if(Auth::user()->can(['super-admin', 'add-customer' ]))
                        <li><a href="{{ route('member.sharer.create', 'customer') }}"><i class="fa fa-plus"></i> Add Customer</a></li>
                    @endif
                </ul>
            </li>
            @endif

            @if(Auth::user()->can(['super-admin', 'create-cash-bank-account','list-accounts']))
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-university"></i>
                    <span> Cash & Bank </span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @if(Auth::user()->can(['super-admin', 'create-cash-bank-account']))
                        <li><a href="{{ route('member.cash_or_bank_accounts.create') }}">New Account</a></li>
                    @endif
                    @if(Auth::user()->can(['super-admin','list-accounts']))
                         <li><a href="{{ route('member.cash_or_bank_accounts.index') }}">List Accounts</a></li>
                    @endif
                </ul>
            </li>
            @endif

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart"></i>
                    <span>Reports</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">

                    @if(Auth::user()->can(['super-admin', 'report-list']))
                    <li><a href="{{ route('member.report.list') }}">Report List</a></li>
                    @endif

                    @if(Auth::user()->can(['super-admin', 'purchase-due-report', 'supplier-due-report','supplier-due-collection-report',
                    'purchase-report','transport-cost-report', 'unload-cost-report', 'product-purchase-report', 'purchase-report-by-supplier']))
                    <li class="treeview">
                        <a href="#">
                            <span> Purchase Reports</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul  class="treeview-menu">
                            @if(Auth::user()->can(['super-admin', 'purchase-report']))
                            <li><a href="{{ route('member.report.purchase') }}">Purchase Report</a></li>
                            @endif
                            @if(Auth::user()->can(['super-admin', 'supplier-due-report']))
                                <li><a href="{{ route('member.report.sharer_due', 'supplier') }}">Supplier Due</a></li>
                            @endif
                            @if(Auth::user()->can(['super-admin', 'supplier-due-collection-report']))
                                <li><a href="{{ route('member.report.sharer_due_collection', 'supplier') }}">Supplier Due Collection</a></li>
                            @endif
                            @if(Auth::user()->can(['super-admin', 'purchase-due-report']))
                                <li><a href="{{ route('member.report.inventory_due', 'purchase') }}">Purchase's Due </a></li>
                            @endif
                            @if(Auth::user()->can(['super-admin', 'transport-cost-report']))
                                <li><a href="{{ route('member.report.cost', 'transport') }}">Transport Cost </a></li>
                            @endif
                            @if(Auth::user()->can(['super-admin', 'unload-cost-report']))
                                <li><a href="{{ route('member.report.cost', 'unload')}}">Unload Cost </a></li>
                            @endif
                            @if(Auth::user()->can(['super-admin', 'product-purchase-report']))
                                <li><a href="{{ route('member.report.product_purchase_report')}}">Product Purchase </a></li>
                            @endif
                            @if(Auth::user()->can(['super-admin', 'purchase-report-by-supplier']))
                                <li><a href="{{ route('member.report.supplier_purchase')}}">Report by Supplier </a></li>
                            @endif
                        </ul>
                    </li>
                    @endif

                    @if(Auth::user()->can(['super-admin', 'sale-report','customer-due-report','customer-due-collection-report','sale-due-report']))
                    <li class="treeview">
                        <a href="#">
                            <span> Sale Reports</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul  class="treeview-menu">
                            @if(Auth::user()->can(['super-admin', 'sale-report']))
                            <li><a href="{{ route('member.report.sale') }}">Sale Report</a></li>
                            @endif
                            @if(Auth::user()->can(['super-admin', 'customer-due-report']))
                            <li><a href="{{ route('member.report.sharer_due', 'customer') }}">Customer Due</a></li>
                            @endif
                            @if(Auth::user()->can(['super-admin', 'customer-due-collection-report']))
                            <li><a href="{{ route('member.report.sharer_due_collection', 'customer') }}">Customer Due Collection</a></li>
                            @endif
                            @if(Auth::user()->can(['super-admin', 'sale-due-report']))
                            <li><a href="{{ route('member.report.inventory_due', 'sale') }}"> Sale's Due </a></li>
                            @endif
                        </ul>
                    </li>
                    @endif

                    @if(Auth::user()->can(['super-admin', 'daily-stocks-report', 'stocks-report', 'total-stock-report', 'balance-sheet']))
                    <li class="treeview">
                        <a href="#">
                            <span> Summary Reports</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul  class="treeview-menu">
                            @if(Auth::user()->can(['super-admin', 'daily-stocks-report']))
                            <li><a href="{{ route('member.report.daily_stocks') }}"> Daily Stocks</a></li>
                            @endif
                            @if(Auth::user()->can(['super-admin', 'stocks-report']))
                            <li><a href="{{ route('member.report.stocks') }}"> Stocks</a></li>
                            @endif
                            @if(Auth::user()->hasRole(['super-admin', 'admin', 'developer']) || Auth::user()->can(['super-admin', 'total-stock-report']))
                            <li><a href="{{ route('member.report.total_stocks') }}"> Total Stocks</a></li>
                            @endif
                            @if(Auth::user()->can(['super-admin', 'balance-sheet']))
                            <li><a href="{{ route('member.report.balance_sheet')}}"> Balance Sheet </a></li>
                            @endif
                        </ul>
                    </li>
                    @endif

                </ul>
            </li>

            <li class="header">Admin Settings</li>


            @if(Auth::user()->can(['super-admin', 'add-category', 'category-list']))
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list"></i> <span>Categories</span>
                </a>
                <ul class="treeview-menu">

                    @if(Auth::user()->can(['super-admin', 'category-list']))
                    <li><a href="{{ route('member.categories.index') }}"><i class="fa fa-unlock-alt"></i> Categories</a></li>
                    @endif
                    @if(Auth::user()->can(['super-admin', 'add-category']))
                    <li><a href="{{ route('member.categories.create') }}"><i class="fa fa-plus"></i> Add Category</a></li>
                    @endif
                </ul>
            </li>
            @endif

            @if(Auth::user()->can(['super-admin', 'add-product', 'list-product', 'barcode-print']))
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-product-hunt"></i> <span>Items/Products</span>
                </a>
                <ul class="treeview-menu">
                    @if(Auth::user()->can(['super-admin', 'list-product']))
                    <li><a href="{{ route('member.items.index') }}"><i class="fa fa-unlock-alt"></i> Items/Products</a></li>
                    @endif
                    @if(Auth::user()->can(['super-admin', 'add-product']))
                    <li><a href="{{ route('member.items.create') }}"><i class="fa fa-plus"></i> Add Item/Product</a></li>
                    @endif
                    @if(Auth::user()->can(['super-admin', 'barcode-print']))
                    <li><a href="{{ route('member.items.print_barcode_form') }}"><i class="fa fa-plus"></i> Barcode Print</a></li>
                    @endif
                </ul>
            </li>
            @endif


            @if(Auth::user()->hasRole(['super-admin', 'admin', 'developer']))

                @if(Auth::user()->can(['super-admin', 'payment-methods', 'general-settings']))
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-gears"></i> <span>Settings</span>
                        </a>
                        <ul class="treeview-menu">

                            @if(Auth::user()->can(['super-admin', 'payment-methods']))
                                <li><a href="{{ route('admin.payment_methods.index') }}"><i class="fa fa-money"></i> Payment Methods</a></li>
                            @endif

                            @if(Auth::user()->can(['super-admin', 'general-settings']))
                                <li><a href="{{ route('member.settings.general_settings') }}"><i class="fa fa-cog"></i> General Settings</a></li>
                            @endif
                        </ul>
                    </li>
                @endif

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-building"></i> <span>Branch</span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('member.branch.index') }}"><i class="fa fa-unlock-alt"></i> Branches</a></li>
                        <li><a href="{{ route('member.branch.create') }}"><i class="fa fa-plus"></i> Add Branch</a></li>
                    </ul>
                </li>

                <li>
                    <a href="{{ route('admin.units.index') }}">
                        <i class="fa fa-tags"></i> <span>Units</span>
                    </a>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i> <span>Users</span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('member.users.index') }}"><i class="fa fa-user"></i> Users</a></li>
                        <li><a href="{{ route('member.users.create') }}"><i class="fa fa-user-plus"></i> Add User</a></li>
                        <li><a href="{{ route('member.users.set_users_branch') }}"><i class="fa fa-user-plus"></i> Assign User Branch</a></li>
                    </ul>
                </li>
            @endif

            @if( Auth::user()->hasRole(['super-admin', 'developer']) || Auth::user()->can(['super-admin', 'add-role', 'roles']))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-unlock-alt"></i> <span>Roles</span>
                    </a>
                    <ul class="treeview-menu">
                        @if(Auth::user()->hasRole(['super-admin']) ||  Auth::user()->can(['super-admin', 'roles']))
                            <li><a href="{{ route('admin.roles.index') }}"><i class="fa fa-unlock-alt"></i> Roles</a></li>
                        @endif
                        @if(Auth::user()->hasRole(['super-admin']) ||  Auth::user()->can(['super-admin', 'add-role']))
                            <li><a href="{{ route('admin.roles.create') }}"><i class="fa fa-plus"></i> Add Role</a></li>
                        @endif
                    </ul>
                </li>
            @endif

            @if(Auth::user()->hasRole(['super-admin']))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-unlock-alt"></i> <span>Permissions</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.permissions.index') }}"><i class="fa fa-unlock-alt"></i> Permissions</a></li>
                    <li><a href="{{ route('admin.permissions.create') }}"><i class="fa fa-plus"></i> Add Permission</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Members</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.members.index') }}"><i class="fa fa-user"></i> Members</a></li>
                    <li><a href="{{ route('admin.members.create') }}"><i class="fa fa-user-plus"></i> Add Member</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Membership Feature</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.memberships.index') }}"><i class="fa fa-user"></i> Memberships</a></li>
                    <li><a href="{{ route('admin.memberships.create') }}"><i class="fa fa-user-plus"></i> Add Membership</a></li>
                </ul>
            </li>
            @endif
            </ul>
    </section>
    <!-- /.sidebar -->
</aside>

