<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 4/2/2019
 * Time: 12:28 PM
 */

$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('member.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => 'General Ledger',
        'href' => route('member.general_ledger.index'),
    ],
    [
        'name' => "Completed Transaction",
    ],
];

$data['data'] = [
    'name' => 'Completed Transaction',
    'title'=>'Completed Transaction',
    'heading' => 'Completed Transaction',
];
?>


@extends('layouts.back-end.master', $data)

@section('contents')


    <div class="row">
        <div class="col-xs-12">

            @include('common._alert')

            <div class="box">

                <div class="box-body">

                    <div class="text-center store-trans">
                        <a href="{{ route('member.general_ledger.show', $status['transaction_code']) }}" class="btn btn-primary"> View the GL Postings for this Payment </a>
                        <br/>
                        <a href="{{ route('member.transaction.create', 'Income') }}" class="btn btn-success"><i class="fa fa-long-arrow-right"></i> New Deposit/Income</a>
                        <br/>
                        <a href="{{ route('member.transaction.create', 'Expense') }}" class="btn btn-info"><i class="fa fa-long-arrow-left"></i> New Payment/Expense</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
