<?php
/**
 * Created by PhpStorm.
 * User: R-Creation
 * Date: 2/27/2019
 * Time: 4:25 PM
 */

?>
@push('styles')
    <link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/select2/dist/css/select2.min.css')}}">
@endpush

@include('common._error')

    <div class="col-md-6">
        <div class="form-group">
            <label for="title">Account Title <span class="text-red"> * </span> </label>
            {!! Form::text('title',null,['id'=>'title','class'=>'form-control','placeholder'=>'Enter Title', 'required']); !!}
        </div>
{{--        <div class="form-group">--}}
{{--            <label for="account_type">Bank Account GL Code <span class="text-red"> * </span>  </label>--}}
{{--            {!! Form::select('account_type_id', $account_types, null,['id'=>'account_type','class'=>'form-control select2','placeholder'=>'Please GL Account', 'required' ]); !!}--}}
{{--        </div>--}}
{{--        <div class="form-group">--}}
{{--            <label for="bank_charge_account_id">Bank Charge Account <span class="text-red"> * </span>  </label>--}}
{{--            {!! Form::select('bank_charge_account_id', $account_types, null,['id'=>'bank_charge_account_id','class'=>'form-control select2','placeholder'=>'Please GL Account', 'required' ]); !!}--}}
{{--        </div>--}}
        <div class="form-group">
            <label for="description">Description  </label>
            {!! Form::textarea('description',null,['id'=>'description','class'=>'form-control','rows'=>'5', 'placeholder'=>'Enter description']); !!}
        </div>

        <div class="form-group">
            <label for="initial_balance">Initial Balance  </label>
            {!! Form::number('initial_balance',null,['id'=>'initial_balance','class'=>'form-control input-number','placeholder'=>'Enter Initial Balance ', isset($model) ? "readonly" : '']); !!}
        </div>
        <div class="form-group">
            <label for="status">Status <span class="text-red"> * </span>  </label>
            {!! Form::select('status',['active'=>'Active', 'inactive'=>'Inactive'], null,['id'=>'status','class'=>'form-control',  'required' ]); !!}
        </div>
    </div>
<div class="col-md-6">
        <div class="form-group">
            <label for="email">Contact Person </label>
            {!! Form::text('contact_person',null,['id'=>'contact_person','class'=>'form-control','placeholder'=>'Enter Contact Person', 'required']); !!}
        </div>
        <div class="form-group">
            <label for="phone">Phone  <span class="text-red"> * </span> </label>
            {!! Form::text('phone',null,['id'=>'phone','class'=>'form-control input-number','placeholder'=>'Enter phone', 'required']); !!}
        </div>
        <div class="form-group">
            <label for="account_number">Account Number  </label>
            {!! Form::text('account_number',null,['id'=>'account_number','class'=>'form-control input-number','placeholder'=>'Enter Account Number']); !!}
        </div>
        <div class="form-group">
            <label for="internet_banking_url">Internet Banking URL </label>
            {!! Form::url('internet_banking_url',null,['id'=>'internet_banking_url','class'=>'form-control','placeholder'=>'Enter Internet Banking URL  ']); !!}
        </div>

    </div>


