<?php
/**
 * Created by PhpStorm.
 * Cash or Bank Account: R-Creation
 * Date: 2/27/2019
 * Time: 12:52 PM
 */



$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('member.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => 'Cash & Bank Accounts',
        'href' => route('member.cash_or_bank_accounts.index'),
    ],
    [
        'name' => 'Edit',
    ],
];

$data['data'] = [
    'name' => 'Cash & Bank  Accounts',
    'title'=>'Edit Cash & Bank  Account',
    'heading' => 'Edit Cash & Bank  Account',
];

?>
@extends('layouts.back-end.master', $data)

@section('contents')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">

            @include('common._alert')

            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Update </h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                {!! Form::model($model, ['route' => ['member.cash_or_bank_accounts.update', $model],  'method' => 'put']) !!}

                <div class="box-body">

                    @include('member.cash_or_bank_account._form')

                    <div class="box-footer">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->

            {!! Form::close() !!}
            <!-- /.box -->
            </div>
        </div>
    </div>
@endsection



@push('scripts')

    <script src="{{ asset('public/adminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <script type="text/javascript">

        $(document).ready( function(){
            $('.select2').select2();
        });
    </script>
@endpush
