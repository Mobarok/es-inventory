<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 3/21/2019
 * Time: 3:55 PM
 */
?>


@push('styles')
    <link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/select2/dist/css/select2.min.css')}}">
@endpush


    <div class="col-md-7">

        <div class="form-group  {{ $errors->has('display_name') ? 'has-error' : '' }} ">
            <label for="name">Name <span class="text-red"> * </span> </label>
            {!! Form::text('display_name', null, ['id'=>'display_name','class'=>'form-control','placeholder'=>'Enter Name', 'required']); !!}
        </div>
        
        <div class="form-group {{ $errors->has('parent_id') ? 'has-error' : '' }}">
            <label for="parent_id">Select Parent Category  </label>
            {!! Form::select('parent_id', $categories , null,['id'=>'parent_id','class'=>'form-control select2','placeholder'=>'Please select Parent Category']); !!}
        </div>
        <div class="form-group">
            <label for=file> Category Image </label>
            <input class="form-control" name="category_image" type="file" accept='image/*' />
        </div>
        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
            <label for="status">Select Parent Category  </label>
            {!! Form::select('status', ['active'=>'Active', 'inactive'=>'Inactive'] , null,['id'=>'status','class'=>'form-control']); !!}
        </div>
    </div>



@push('scripts')

   <script src="{{ asset('public/adminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- Date range picker -->
    <script type="text/javascript">

        // var date = new Date();
        $(function () {

            $('.select2').select2();

        });
    </script>

@endpush


