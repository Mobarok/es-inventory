<?php
/**
 * Created by PhpStorm.
 * User: R-Creation
 * Date: 2/27/2019
 * Time: 4:25 PM
 */

?>

@push('styles')

<link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
<link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/select2/dist/css/select2.min.css')}}">
<link rel="stylesheet" href="{{ asset('public/adminLTE/dist/css/jasny-bootstrap.css')}}">

@endpush



<div class="col-md-6">
    <div class="form-group">
        <label for="name">Company Name<span class="text-red"> * </span> </label>
        {!! Form::text('company_name',null,['id'=>'company_name','class'=>'form-control','placeholder'=>'Enter Company Name', 'required']); !!}
    </div>
    <div class="form-group">
        <label for="phone">Phone No<span class="text-red"> * </span> </label>
        {!! Form::text('phone',null,['id'=>'phone','class'=>'form-control','placeholder'=>'Enter Phone No', 'required']); !!}
    </div>
    <div class="form-group">
        <label for="phone">Email<span class="text-red"> * </span> </label>
        {!! Form::text('email',null,['id'=>'email','class'=>'form-control ','placeholder'=>'Enter Email', 'required']); !!}
    </div>
    <div class="form-group">
        <label for="display_name">Fiscal Year </label>
        {!! Form::select('fiscal_year_id',$fiscal_year,null,['id'=>'fiscal_year_id','class'=>'form-control select2','placeholder'=>'Select Fiscal Year']); !!}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label for="display_name">Address <span class="text-red"> * </span> </label>
        {!! Form::textarea('address',null,['id'=>'address','class'=>'form-control','placeholder'=>'Enter Address',  'rows'=>"2"]); !!}
    </div>
    <div class="form-group">
        <label for="city">City  </label>
        {!! Form::text('city',null,['id'=>'city','class'=>'form-control','placeholder'=>'Enter City']); !!}
    </div>
    <div class="form-group">
        <label for="display_name">Country <span class="text-red"> * </span> </label>
        {!! Form::select('country_id',$countries,null,['id'=>'country','class'=>'form-control select2','placeholder'=>'Select Country','required']); !!}
    </div>
    <div class="form-group">
        <label for="logo">Logo  </label>
        {!! Form::file('logo',null,['id'=>'logo','class'=>'form-control','placeholder'=>'Enter Title']); !!}
    </div>
</div>

    @push('scripts')

    <!-- Date range picker -->

    <script src="{{ asset('public/adminLTE/bower_components/moment/min/moment.min.js') }}"></script>

    <script src="{{ asset('public/adminLTE/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <!-- Select2 -->
    <script src="{{ asset('public/adminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <!-- CK Editor -->
    <script src="{{ asset('public/adminLTE/bower_components/ckeditor/ckeditor.js') }}"></script>

    <script src="{{ asset('public/adminLTE/dist/js/jasny-bootstrap.min.js') }}"></script>


    

    <script type="text/javascript">

      $(function () {

        $('.select2').select2()
    });



</script>


@endpush

