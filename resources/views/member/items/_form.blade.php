<?php
/**
 * Created by PhpStorm.
 * User: R-Creation
 * Date: 2/27/2019
 * Time: 4:25 PM
 */

?>

@push('styles')

    <link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/select2/dist/css/select2.min.css')}}">
@endpush


    <div class="col-md-6">
        <div class="form-group">
            <label for="item_name">Item Name <span class="text-red"> * </span> </label>
            {!! Form::text('item_name',null,['id'=>'item_name','class'=>'form-control','placeholder'=>'Enter Item Name', 'required']); !!}
        </div>
        <div class="form-group">
            <label for="item_name">SkuCode <span class="text-red"> * </span> </label>
            {!! Form::text('skuCode',null,['id'=>'sku_code','class'=>'form-control','placeholder'=>'Enter Sku Code']); !!}
        </div>
        <div class="form-group">
            <label for="item_name"> Product Code <span class="text-red"> * </span> </label>
            {!! Form::text('productCode',null,['id'=>'product_code','class'=>'form-control','placeholder'=>'Enter  Product Code', 'required']); !!}
        </div>
        <div class="form-group">
            <label for="status">Product Category <span class="text-red"> * </span>  </label>
            {!! Form::select('category_id',$categories, null,['id'=>'category_id','class'=>'form-control select2','placeholder'=>'Please select status', 'required']); !!}
        </div>
        <div class="form-group">
            <label for="product_image">Product Image  </label>
            {!! Form::file('product_image', null,['id'=>'product_image', 'accept'=>'image/*' , 'class'=>'form-control','placeholder'=>'Enter Title' , ]); !!}
        </div>
        <div class="form-group">
            <label for="status">Unit <span class="text-red"> * </span>  </label>
            {!! Form::select('unit', $units, null,['id'=>'unit','class'=>'form-control','placeholder'=>'Please select Unit', 'required']); !!}
        </div>
        <div class="form-group">
            <label for="warranty"> Price  <span class="text-red"> * </span> </label>
            {!! Form::number('price',null,['id'=>'price','class'=>'form-control','placeholder'=>'Enter Price', 'required']); !!}
        </div>
        <div class="form-group">
            <label for="status">Status <span class="text-red"> * </span>  </label>
            {!! Form::select('status',['active'=>'Active', 'inactive'=>'Inactive'], null,['id'=>'status','class'=>'form-control', 'required']); !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="description">Description </label>
            {!! Form::textarea('description',null,['id'=>'description','class'=>'form-control','placeholder'=>'Enter description']); !!}
        </div>
        <div class="form-group">
            <label for="warranty"> Item Warranty </label>
            {!! Form::number('warranty',null,['id'=>'warranty','class'=>'form-control','placeholder'=>'Enter warranty']); !!}
        </div>
        <div class="form-group">
            <label for="warranty"> Item Guarantee </label>
            {!! Form::number('guarantee',null,['id'=>'guarantee','class'=>'form-control','placeholder'=>'Enter Guarantee']); !!}
        </div>
    </div>



@push('scripts')

    <!-- Select2 -->
    <script src="{{ asset('public/adminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- CK Editor -->
    <script src="{{ asset('public/adminLTE/bower_components/ckeditor/ckeditor.js') }}"></script>


    <script type="text/javascript">
        $(function () {

            $('.select2').select2()
        });

    </script>


@endpush
