<?php
/**
 * Print Barcoded by PhpStorm.
 * User: Mobarok Hossen
 * Date: 8/29/2019
 * Time: 1:15 PM
 */


$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('member.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => 'Products',
        'href' => route('member.items.index'),
    ],
    [
        'name' => 'Print Barcode',
    ],
];

$data['data'] = [
    'name' => 'Products',
    'title'=>'Print Barcode Product',
    'heading' => 'Print Barcode Product',
];

?>
@extends('layouts.back-end.master', $data)
@push('styles')
    <link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/select2/dist/css/select2.min.css')}}">
@endpush
@section('contents')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">

        @include('common._alert')

        @include('common._error')
        <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Print Barcode Product</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="item_name">Product Name <span class="text-red"> * </span> </label>
                            {!! Form::select('item_id', $products, null,['id'=>'item_id', 'class'=>'form-control select2','required', 'placeholder'=>'Select Item Name']); !!}
                        </div>
                        <div class="form-group">
                            <label for="warranty"> Print Qty  <span class="text-red"> * </span> </label>
                            {!! Form::text('print_qty',null,['id'=>'print_qty','class'=>'form-control input-number','placeholder'=>'Enter Print Quantity', 'required']); !!}
                        </div>
                    </div>

                    <div class="box-footer">
                        <div class="col-md-12">
                            <a href="#" class=" btn btn-sm btn-primary hidden" id="btn-print"> <i class="fa fa-print"></i> Submit </a>
                            <a href="#" class=" btn btn-sm btn-primary" id="print-barcode"> <i class="fa fa-print"></i> Submit </a>
                        </div>
                    </div>

                </div>
            <!-- /.box -->
            </div>
        </div>
    </div>
@endsection


@push('scripts')
   <script src="{{ asset('public/adminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <!-- Date range picker -->
    <script type="text/javascript">

        // var date = new Date();
        $(function () {
            $('.select2').select2();

            $('#print-barcode').click( function (e) {
                var item_id = $("#item_id").val();
                var print_qty = $("#print_qty").val();
                if( item_id == '' || print_qty == '') {
                    if(item_id == "")
                    {
                        bootbox.alert("Please select product Name");
                    }else{
                        bootbox.alert("Please select print Qty");
                    }
                    return false;
                }
               var link = "{{ route('member.items.print_barcode') }}"+"?item_id="+item_id+"&print_qty="+print_qty;
               // alert(link);
                $("#btn-print").attr("href", link);
                $("#btn-print").trigger('click');
               //  $("#print-barcode").attr("href", link);
               //  $("#print-barcode").trigger('click');
            });
        });
        </script>
@endpush
