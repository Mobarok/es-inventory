<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 4/24/2019
 * Time: 12:28 PM
 */




$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('member.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => 'Journal Entry',
        'href' => route('member.journal_entry.index'),
    ],
    [
        'name' => 'Index',
    ],
];

$data['data'] = [
    'name' => 'Journal Entry',
    'title'=>'List Of Journal Entry',
    'heading' => 'List Of Journal Entry',
];

?>
@push('styles')
    <link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/select2/dist/css/select2.min.css')}}">
@endpush

@extends('layouts.back-end.master', $data)

@section('contents')


    <div class="row">
        <div class="col-xs-12">

            @include('common._alert')

            <div class="box">

                <div class="box-body">

                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 search-transaction">
                           {!! Form::open(['route' => 'member.journal_entry.index','method' => 'GET', 'role'=>'form' ]) !!}

                        <div class="col-md-3">
                            <label> Transaction Code</label>
                            <input class="form-control" name="transaction_code" value="{{ isset($transaction_code) ? $transaction_code : ''  }}" type="text"/>

                        </div>
                        <div class="col-md-3">
                            <label> Transaction Date</label>
                            <input class="form-control" name="date" id="date" value="{{ isset($date) ? $date : ''  }}" autocomplete="off"/>
                        </div>
                        <div class="col-md-3">
                            <label> Transaction From</label>
                            {!! Form::select('from_account_type_id', $accounts, isset($from_account_type_id) ? $from_account_type_id : '',['id'=>'from_account_type_id','class'=>'form-control select2','placeholder'=>'Select Account From']); !!}
                        </div>
                        <div class="col-md-3">
                            <label> Transaction To</label>
                            {!! Form::select('to_account_type_id', $accounts, isset($to_account_type_id) ? $to_account_type_id : '',['id'=>'to_account_type_id','class'=>'form-control select2','placeholder'=>'Select Account To']); !!}
                        </div>
                        <div class="col-md-3">
                            <input class="btn btn-info btn-search" value="Search" type="submit"/>
                            <a class="btn btn-primary btn-search" href="{{ route(Route::current()->getName()) }}"> <i class="fa fa-refresh"></i> </a>
                        </div>

                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <!-- BEGIN SAMPLE FORM PORTLET-->

                        <div class="table-responsive">

                            <table class="table table-responsive table-striped">
                                <tbody>
                                <tr>
                                    <th>Transaction Date</th>
                                    <th>Transaction Code</th>
                                    <th>Transaction From</th>
                                    <th>Debit </th>
                                    <th>Credit </th>
                                    <th>Entry By</th>
                                    <th>Manage</th>
                                </tr>
                                @php
                                    $lastCode = '';
                                @endphp
                                @foreach($modal as $value)
                                    <tr>
                                        <td>{{ db_date_month_year_format($value->date) }}</td>
                                        <td>{{ $value->transaction_code }}</td>
                                        <td>{{ human_words($value->title) }}</td>
                                        <td class="text-right">{{ $value->transaction_type=='dr' ? create_money_format($value->total_amount) : create_money_format(0) }}</td>
                                        <td class="text-right">{{ $value->transaction_type=='cr' ? create_money_format($value->total_amount) : create_money_format(0)  }}</td>
                                        <td>{{ human_words($value->created_user) }}</td>
                                        @if($value->transaction_code != $lastCode)
                                            <td rowspan="{{ $value->transaction_code != $lastCode ? 2 : ''}}">
                                                <a class="btn btn-xs btn-info btn-trans-show" href="{{ route('member.journal_entry.show', $value->transaction_code) }}"><i class="fa fa-eye"></i></a>
                                            </td>
                                        @endif
                                    </tr>
                                    @php
                                        $lastCode = $value->transaction_code;
                                    @endphp
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6" class="text-right"> {{ $modal->links() }}</td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script src="{{ asset('public/adminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <script type="text/javascript">

        $(document).ready( function(){
            $('.select2').select2();
            $('#date').datepicker({
                "setDate": new Date(),
                "format": 'mm/dd/yyyy',
                "endDate": "+0d",
                "todayHighlight": true,
                "autoclose": true
            });
        });
    </script>
@endpush
