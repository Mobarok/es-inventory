<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 5/29/2019
 * Time: 2:57 PM
 */
?>

@include('member.reports.print_head')

<body>
<div id="page-wrap">

    @include('member.reports.company')

    <div style=" overflow: hidden; clear: both; float: inherit; text-align: center!important;">
        <table cellspacing="0" style="width:100%;" class="table" >
            <tbody>
            <tr>
                <th>ID</th>
                <th>Product Code</th>
                <th>Product Name</th>
                <th>Opening Stock</th>
                <th>Purchase Qty</th>
                <th>Purchase Return Qty</th>
                <th>Sale Qty</th>
                <th>Sale Return Qty</th>
                <th>Closing Stock</th>
                <th>Unit</th>
                <th> Date </th>
            </tr>
            @foreach($stocks as $key=>$value)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $value->product_code }}</td>
                    <td>{{ $value->product_name }}</td>
                    <td>{{ $value->opening_stock }}</td>
                    <td>{{ $value->purchase_qty }}</td>
                    <td>{{ $value->purchase_return_qty }}</td>
                    <td>{{ $value->sale_qty }}</td>
                    <td>{{ $value->sale_return_qty }}</td>
                    <td>{{ $value->opening_stock+$value->purchase_qty-$value->purchase_return_qty-$value->sale_qty+$value->sale_return_qty}}</td>
                    <td>{{ $value->item->unit }}</td>
                    <td>{{ db_date_month_year_format($value->date) }}</td>
                </tr>
            @endforeach

            <?php
            if(count($stocks)==0){
            ?>

            <tr>
                <td class="text-left" colspan="8">No Report available for Stock</td>
            </tr>

            <?php } ?>

            </tbody>
        </table>
    </div>
</div>
</body>
</html>

