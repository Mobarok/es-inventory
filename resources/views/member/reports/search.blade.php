<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 4/24/2019
 * Time: 2:53 PM
 */

?>

<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Transaction Search</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-6">
                <label> Transaction Code</label>
                <input class="form-control" name="transaction_code" value="{{ isset($transaction_code) ? $transaction_code : ''  }}" type="text"/>
            </div>
            <div class="col-md-6">
                <label>  Accounts Head </label>
                {!! Form::select('head_account_type_id', $accounts, isset($head_account_type_id) ? $head_account_type_id : '',['id'=>'head_account_type_id','class'=>'form-control select2','placeholder'=>'Select All']); !!}
            </div>
        </div>
        <!-- /.row -->
    </div>

</div>

<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Date-Wise Search</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">

            <div class="col-md-4">
                <label> From Date (Transaction)</label>
                <input class="form-control date" name="from_date" value="" autocomplete="off"/>
            </div>
            <div class="col-md-4">
                <label> To Date (Transaction)</label>
                <input class="form-control date" name="to_date" value="" autocomplete="off"/>
            </div>
            <div class="col-md-4">
                <label>  Accounts Head </label>
                {!! Form::select('d_head_account_type_id', $accounts, isset($d_head_account_type_id) ? $d_head_account_type_id : '',['id'=>'d_head_account_type_id','class'=>'form-control select2','placeholder'=>'Select All']); !!}
            </div>

        </div>
        <!-- /.row -->
    </div>

</div>


<div class="col-md-4">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Group wise Search</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">

                <div class="col-md-12">
                    <label>  Accounts Group </label>
                    {!! Form::select('group_account_type_id', $account_groups, isset($group_account_type_id) ? $group_account_type_id : '',['id'=>'group_account_type_id','class'=>'form-control select2','placeholder'=>'Select All']); !!}
                </div>

            </div>
            <!-- /.row -->
        </div>

    </div>
</div>
<div class="col-md-4">

<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Head Wise Search</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">

            <div class="col-md-12">
                <label>  Accounts Head </label>
                {!! Form::select('head_account_type_id', $accounts, isset($head_account_type_id) ? $head_account_type_id : '',['id'=>'head_account_type_id','class'=>'form-control select2','placeholder'=>'Select All']); !!}
            </div>

        </div>
        <!-- /.row -->
    </div>

</div>
</div>

<div class="col-md-4">
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Sub-Head Wise Search</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">

            <div class="col-md-12">
                <label>  Accounts Sub-Head </label>
                {!! Form::select('sub_head_account_type_id', $account_sub_heads, isset($sub_head_account_type_id) ? $sub_head_account_type_id : '',['id'=>'sub_head_account_type_id','class'=>'form-control select2','placeholder'=>'Select All']); !!}
            </div>

        </div>
        <!-- /.row -->
    </div>

</div>
</div>

<div class="box-body">

    <div class="col-md-3">
        <input class="btn btn-info" value="Search" type="submit"/>
    </div>
</div>
