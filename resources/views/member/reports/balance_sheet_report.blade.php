<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 7/11/2019
 * Time: 2:01 PM
 */

$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('member.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => 'List',
        'href' => route('member.report.balance_sheet'),
    ],
    [
        'name' => 'Balance Sheet Report',
    ],
];

$data['data'] = [
    'name' => "Balance Sheet",
    'title'=> 'Balance Sheet Report',
    'heading' => 'Balance Sheet Report',
];

?>
@extends('layouts.back-end.master', $data)

@push('styles')
<link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/select2/dist/css/select2.min.css')}}">
@endpush

@section('contents')

<div class="row">
    <div class="col-xs-12">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Search</h3>
            </div>

            {!! Form::open(['route' => ['member.report.balance_sheet'],'method' => 'GET', 'role'=>'form' ]) !!}

            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                        <label> From Date </label>
                        <input class="form-control date" name="from_date" value="" autocomplete="off"/>
                    </div>
                    <div class="col-md-2">
                        <label> To Date</label>
                        <input class="form-control date" name="to_date" value="" autocomplete="off"/>
                    </div>
                    <div class="col-md-2 margin-top-23">
                        <label></label>
                        <input class="btn btn-sm btn-info" value="Search" type="submit"/>
                        <a href="{{ route(Route::current()->getName()) }}" class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i> Reload</a>

                    </div>
                </div>
                <!-- /.row -->
            </div>

            {!! Form::close() !!}
        </div>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ ucfirst($report_title) }}</h3>
                <a href="{{ $full_url}}type=print" class="btn btn-sm btn-primary pull-right" id="btn-print"> <i class="fa fa-print"></i> Print </a>
                <a href="{{ $full_url}}type=download" class="btn btn-sm btn-success pull-right"> <i class="fa fa-download"></i> Download </a>
            </div>

            <div class="box-body">

                <div class="col-lg-12">
                    <table class="table table-striped" id="dataTable">
                        <tbody>

                            <tr>
                                <td> Total Purchase</td>
                                <td class="text-right"> {{ create_money_format($total_purchase) }}</td>
                            </tr>
                            <tr>
                                <td> Total Due Purchase (+)</td>
                                <td class="text-right"> {{ create_money_format($total_due_purchase) }}</td>
                            </tr>
                            <tr>
                                <td> Total Advance Purchase (-)</td>
                                <td class="text-right"> {{ create_money_format($total_advance_purchase) }}</td>
                            </tr>
                            <tr >
                                <th> Total</th>
                                <th class="text-right" style="border-top: 1px solid #000;"> @php $total = $total_purchase+$total_due_purchase-$total_advance_purchase @endphp {{ create_money_format($total) }}</th>
                            </tr>
                            <tr>
                                <td> Total Sale (+)</td>
                                <td class="text-right"> {{ create_money_format($total_sale) }}</td>
                            </tr>
                            <tr>
                                <td> Total Due Sale (-)</td>
                                <td class="text-right"> {{ create_money_format($total_due_sale) }}</td>
                            </tr>
                            <tr>
                                <th> Total</th>
                                <th class="text-right" style="border-top: 1px solid #000;"> @php $sub_total = $total+$total_sale-$total_due_sale @endphp {{ create_money_format($sub_total) }}</th>
                            </tr>
                            <tr>
                                <td> Total Due Collection (+)</td>
                                <td class="text-right"> {{ create_money_format($total_due_collection_amount) }}</td>
                            </tr>
                            <tr>
                                <th> Total</th>
                                <th class="text-right" style="border-top: 1px solid #000;"> {{ create_money_format($sub_total+$total_due_collection_amount) }}</th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection



@push('scripts')

<script src="{{ asset('public/adminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

<script type="text/javascript">

    $(document).ready( function(){
        $('.select2').select2();
        $('.date').datepicker({
            "setDate": new Date(),
            "format": 'mm/dd/yyyy',
            "endDate": "+0d",
            "todayHighlight": true,
            "autoclose": true
        });
    });
</script>
@endpush

