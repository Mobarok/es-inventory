<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 7/22/2019
 * Time: 4:58 PM
 */
?>


@include('member.reports.print_head')

<body>
<div id="page-wrap">

    @include('member.reports.company')

    <div style="width: 100%; display: flex; flex-wrap: nowrap; overflow: hidden; clear: both;">
        <table class="table table-striped" id="dataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Product Code</th>
                    <th>Product Name</th>
                    <th>Unit</th>
                    <th class="text-center">Quantity</th>
                    <th class="text-right">Price</th>
                    <th class="text-right">Total Price</th>
                </tr>
            </thead>
            <tbody>
            @php
                $last_date = 0;
                $purchase_total_price = 0;
            @endphp
            @foreach($purchases as $key => $value)
                @if($last_date!=0 && $last_date!=db_date_month_year_format($value->purchases->date))
                    <tr class=" margin-bottom-20">
                        <th colspan="6" class="text-right">Total</th>
                        <th colspan="2" class="text-right">{{ create_money_format($purchase_total_price) }}</th>
                    </tr>
                    @php
                        $purchase_total_price = 0;
                    @endphp
                @endif
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ db_date_month_year_format($value->purchases->date) }}</td>
                    <td>{{ $value->item->productCode }}</td>
                    <td>{{ $value->item->item_name }}</td>
                    <td>{{ $value->unit }}</td>
                    <td class="text-center">{{ $value->qty }}</td>
                    <td class="text-right">{{ create_money_format($value->price) }}</td>
                    <td class="text-right">{{ create_money_format($value->qty*$value->price) }}</td>
                </tr>
                @php
                    $last_date = db_date_month_year_format($value->purchases->date);
                    $purchase_total_price += $value->qty*$value->price;
                @endphp

                @if( $loop->last)
                    <tr class=" margin-bottom-20">
                        <th colspan="6" class="text-right">Total</th>
                        <th colspan="2" class="text-right">{{ create_money_format($purchase_total_price) }}</th>
                    </tr>
                    @php
                        $purchase_total_price = 0;
                    @endphp
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>
</html>

