<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 6/16/2019
 * Time: 11:41 AM
 */


$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('member.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => 'Purchases',
        'href' => route('member.purchase.index'),
    ],
    [
        'name' => ($cost_type == "unload" ? 'Unload' : 'Transport').' Cost Report',
    ],
];

$data['data'] = [
    'name' => ($cost_type == "unload" ? 'Unload' : 'Transport').' Cost Report',
    'title'=> ($cost_type == "unload" ? 'Unload' : 'Transport').' Cost Report',
    'heading' => ($cost_type == "unload" ? 'Unload' : 'Transport').' Cost Report',
];

?>

@extends('layouts.back-end.master', $data)


@push('styles')
    <link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/select2/dist/css/select2.min.css')}}">
@endpush

@section('contents')

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Search</h3>
                </div>

            {!! Form::open(['route' => ['member.report.cost', $cost_type],'method' => 'GET', 'role'=>'form' ]) !!}
            <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">

                        @if(Auth::user()->hasRole(['super-admin', 'admin', 'developer']))
                            <div class="col-md-4">
                                <label>  Branch Name </label>
                                {!! Form::select('branch_id', $branches, null,['id'=>'branch_id','class'=>'form-control select2','placeholder'=>'Select All']); !!}
                            </div>
                        @endif
                        <div class="col-md-4">
                            <label> Memo No </label>
                            <input class="form-control" name="memo_no" value="" autocomplete="off"/>
                        </div>
                        <div class="col-md-4">
                            <label> Chalan No </label>
                            <input class="form-control" name="chalan" value="" autocomplete="off"/>
                        </div>
                        <div class="col-md-4">
                            <label> From Date </label>
                            <input class="form-control date" name="from_date" value="" autocomplete="off"/>
                        </div>
                        <div class="col-md-4">
                            <label> To Date</label>
                            <input class="form-control date" name="to_date" value="" autocomplete="off"/>
                        </div>
                        <div class="col-md-4 margin-top-23">
                            <label></label>
                            <input class="btn btn-info" value="Search" type="submit"/>
{{--                            <a href="{{ route(Route::current()->getName()) }}" class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i> Reload</a>--}}

                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                {{--<div class="box-body">--}}

                {{----}}
                {{--</div>--}}

                {!! Form::close() !!}
            </div>

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $cost_type == "unload" ? 'Unload' : 'Transport' }} Cost Report</h3>
                    <a href="{{ $full_url}}type=print" class="btn btn-sm btn-primary  pull-right" id="btn-print"> <i class="fa fa-print"></i> Print </a>
                    <a href="{{ $full_url}}type=download" class="btn btn-sm btn-success  pull-right"> <i class="fa fa-download"></i> Download </a>
                </div>

                <div class="box-body">

                    <div class="col-lg-12">
                        <table class="table table-striped" id="dataTable">

                            <tbody>
                            <tr>
                                <th> ID</th>
                                <th> Memo No</th>
                                <th> Chalan No</th>
                                <th> {{ $cost_type == "unload" ? 'Unload' : 'Transport' }} Cost</th>
                                <th> Date </th>
                            </tr>
                            @foreach($modal as $key=>$value)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $value->memo_no }}</td>
                                    <td>{{ $value->chalan }}</td>
                                    <td class="text-center">
                                        {{ $cost_type == "unload" ? create_money_format($value->unload_cost) : create_money_format($value->transport_cost) }}
                                    </td>
                                    <td>{{ db_date_month_year_format($value->date) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-12 col-sm-12 col-md-12 text-right">
                        {{ $modal->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('scripts')

    <script src="{{ asset('public/adminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <script type="text/javascript">

        $(document).ready( function(){
            $('.select2').select2();
            $('.date').datepicker({
                "setDate": new Date(),
                "format": 'mm/dd/yyyy',
                "endDate": "+0d",
                "todayHighlight": true,
                "autoclose": true
            });
        });
    </script>
@endpush
