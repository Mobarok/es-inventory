<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 7/6/2019
 * Time: 4:16 PM
 */

$cus_type = ucfirst($type);
$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('member.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => 'List',
        'href' => route('member.report.inventory_due', $type),
    ],
    [
        'name' => $cus_type.' Due Report',
    ],
];

$data['data'] = [
    'name' => $cus_type.' Due Report',
    'title'=> $cus_type.' Due Report',
    'heading' => $cus_type.' Due Report',
];

?>
@extends('layouts.back-end.master', $data)

@push('styles')
    <link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/select2/dist/css/select2.min.css')}}">
@endpush

@section('contents')

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Search</h3>
                </div>

            {!! Form::open(['route' => ['member.report.inventory_due', $type],'method' => 'GET', 'role'=>'form' ]) !!}

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label> {{ $cus_type == "Sale" ? "Customers" : "Suppliers"}} </label>
                            {!! Form::select('sharer_id', $sharers, null,['id'=>'sharer_id','class'=>'form-control select2','placeholder'=>'Select All']); !!}
                        </div>
                        @if($type=="Sale" || $type=="Purchase")
                        <div class="col-md-2">
                            <label> From Date </label>
                            <input class="form-control date" name="from_date" value="" autocomplete="off"/>
                        </div>
                        <div class="col-md-2">
                            <label> To Date</label>
                            <input class="form-control date" name="to_date" value="" autocomplete="off"/>
                        </div>
                        @endif
                        <div class="col-md-2 margin-top-23">
                            <label></label>
                            <input class="btn btn-sm btn-info" value="Search" type="submit"/>
  <a href="{{ route(Route::current()->getName(), $type) }}" class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i> Reload</a>

                        </div>
                    </div>
                    <!-- /.row -->
                </div>

                {!! Form::close() !!}
            </div>

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ ucfirst($type) }} Due Report</h3>
                    <a href="{{ $full_url}}type=print" class="btn btn-sm btn-primary pull-right" id="btn-print"> <i class="fa fa-print"></i> Print </a>
                    <a href="{{ $full_url}}type=download" class="btn btn-sm btn-success pull-right"> <i class="fa fa-download"></i> Download </a>
                </div>

                <div class="box-body">

                    <div class="col-lg-12">
                        <table class="table table-striped" id="dataTable">

                            <tbody>
                            <tr>
                                <th> ID</th>
                                <th> {{ $type=="customer" || $type=="Sale" ? "Customer" : "Supplier" }} Name</th>
                                <th> Paid Amount</th>
                                <th> Due Amount</th>
                                <th> Discount Amount</th>
                                <th> Total Amount</th>
                                <th> Date</th>
                            </tr>
                            @foreach($modal as $key=>$value)
                                <tr>
                                    <td> {{ $key+1 }} </td>
                                    @if($type=="customer" || $type=="Sale")
                                        <td> {{ $value->customer ? $value->customer->name : '' }} </td>
                                        <td> {{ create_money_format($value->paid_amount) }} </td>
                                        <td> {{ create_money_format($value->due) }} </td>
                                        <td> {{ create_money_format($value->total_discount) }} </td>
                                        <td> {{ create_money_format($value->total_price) }} </td>
                                    @else
                                        <td> {{ $value->supplier ? $value->supplier->name : '' }} </td>
                                        <td> {{ create_money_format($value->paid_amount) }} </td>
                                        <td> {{ create_money_format($value->due_amount) }} </td>
                                        <td> {{ create_money_format($value->total_discount) }} </td>
                                        <td> {{ create_money_format($value->total_amount) }} </td>
                                    @endif
                                    <td> {{ $value->date_format }} </td>
                                </tr>
                            @endforeach

                            @if(count($modal)<1)
                                <tr>
                                    <td colspan="7"> No Data Available </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-12 col-sm-12 col-md-12 text-right">
                        {{ $modal->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('scripts')

    <script src="{{ asset('public/adminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <script type="text/javascript">

        $(document).ready( function(){
            $('.select2').select2();
            $('.date').datepicker({
                "setDate": new Date(),
                "format": 'mm/dd/yyyy',
                "endDate": "+0d",
                "todayHighlight": true,
                "autoclose": true
            });
        });
    </script>
@endpush
