<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 7/18/2019
 * Time: 12:48 PM
 */
$title = "Report";

$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('member.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => $title,
        'href' => route(Route::current()->getName()),
    ],
    [
        'name' => 'Index',
    ],
];

$data['data'] = [
    'name' => $title,
    'title'=>'List Of '.$title,
    'heading' => 'List Of '.$title,
];

?>
@extends('layouts.back-end.master', $data)

@section('contents')


    <div class="row small-text">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-body report_list">
                    <div class="col-md-6">
                        <h3> Purchase Reports</h3>
                        <ul>
                            <li><a href="{{ route('member.report.purchase') }}"><i class="fa fa-line-chart"></i> Purchase Report</a></li>
                            <li><a href="{{ route('member.report.sharer_due', 'supplier') }}"><i class="fa fa-line-chart"></i> Supplier Due</a></li>
                            <li><a href="{{ route('member.report.sharer_due_collection', 'supplier') }}"><i class="fa fa-line-chart"></i> Supplier Due Collection</a></li>
                            <li><a href="{{ route('member.report.inventory_due', 'purchase') }}"><i class="fa fa-line-chart"></i> Purchase's Due </a></li>
                            <li><a href="{{ route('member.report.cost', 'transport') }}"><i class="fa fa-line-chart"></i> Transport Cost </a></li>
                            <li><a href="{{ route('member.report.cost', 'unload')}}"><i class="fa fa-line-chart"></i> Unload Cost </a></li>
                            <li><a href="{{ route('member.report.product_purchase_report')}}"><i class="fa fa-line-chart"></i> Product Purchase </a></li>
                            <li><a href="{{ route('member.report.supplier_purchase')}}"><i class="fa fa-line-chart"></i> Purchase Report by Supplier </a></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <h3>Sales Reports</h3>
                        <ul>
                            <li><a href="{{ route('member.report.sale') }}"><i class="fa fa-line-chart"></i> Sale Report</a></li>
                            <li><a href="{{ route('member.report.sharer_due', 'customer') }}"><i class="fa fa-line-chart"></i> Customer Due</a></li>
                            <li><a href="{{ route('member.report.sharer_due_collection', 'customer') }}"><i class="fa fa-line-chart"></i> Customer Due Collection</a></li>
                            <li><a href="{{ route('member.report.inventory_due', 'sale') }}"><i class="fa fa-line-chart"></i> Sale's Due </a></li>
                        </ul>
                    </div>

                    <div class="col-md-6">
                        <h3>Summary Reports</h3>
                        <ul>
                           <li><a href="{{ route('member.report.daily_stocks') }}"><i class="fa fa-bar-chart"></i> Daily Stocks</a></li>
                            <li><a href="{{ route('member.report.stocks') }}"><i class="fa fa-bar-chart"></i> Stocks</a></li>
                             <li><a href="{{ route('member.report.balance_sheet')}}"><i class="fa fa-line-chart"></i> Balance Sheet </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

