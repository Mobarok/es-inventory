<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 5/30/2019
 * Time: 3:03 PM
 */
?>


@include('member.reports.print_head')

<body>
<div id="page-wrap">

    @include('member.reports.company')

    <div style="overflow: hidden; clear: both;">
        <table cellspacing="0" width="100%" class="table">
            <thead>
            <tr>
                <th class="text-left">ID</th>
                <th  class="text-left">Product Code</th>
                <th  class="text-left">Product Name</th>
                <th  class="text-left"> Stock</th>
                <th  class="text-left">Branch Name</th>
            </tr>
            </thead>
            <tbody>
            @forelse($stocks as $key=>$value)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $value->item->productCode }}</td>
                    <td>{{ $value->item->item_name }}</td>
                    <td>{{ $value->stock." ".$value->item->unit }}</td>
                    <td>{{ $value->branch ? $value->branch->display_name : "" }}</td>
                </tr>
            @empty
                <tr>
                    <td class="text-left" colspan="4">No Report available for Stock</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>
</body>
</html>




