<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 7/22/2019
 * Time: 11:55 AM
 */


$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('member.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => 'Stocks',
        'href' => route('member.report.daily_stocks'),
    ],
    [
        'name' => 'Purchase Report by Supplier',
    ],
];

$data['data'] = [
    'name' => 'Purchase Report by Supplier',
    'title'=> 'Purchase Report by Supplier',
    'heading' => 'Purchase Report by Supplier',
];

?>
@extends('layouts.back-end.master', $data)

@push('styles')
    <link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/select2/dist/css/select2.min.css')}}">
@endpush

@section('contents')

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Search</h3>
                </div>

            {!! Form::open(['route' => 'member.report.supplier_purchase','method' => 'GET', 'role'=>'form' ]) !!}
            <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">

                        <div class="col-md-3">
                            <label>  Supplier Name </label>
                            {!! Form::select('supplier_id', $suppliers, null,['id'=>'supplier_id','class'=>'form-control select2','placeholder'=>'Select All']); !!}
                        </div>

                        <div class="col-md-3">
                            <label>  Product Name </label>
                            {!! Form::select('item_id', $products, null,['id'=>'item_id','class'=>'form-control select2','placeholder'=>'Select All']); !!}
                        </div>

                        <div class="col-md-3">
                            <label> From Date </label>
                            <input class="form-control date" name="from_date" value="" autocomplete="off"/>
                        </div>
                        <div class="col-md-3">
                            <label> To Date</label>
                            <input class="form-control date" name="to_date" value="" autocomplete="off"/>
                        </div>

                        <div class="col-md-3 margin-top-23">
                            <label></label>
                            <input class="btn btn-info" value="Search" type="submit"/>
                            <a href="{{ route(Route::current()->getName()) }}" class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i> Reload</a>

                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                {{--<div class="box-body">--}}

                {{----}}
                {{--</div>--}}

                {!! Form::close() !!}
            </div>

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"> Purchase Report by Supplier</h3>
                    <a class="btn btn-sm  btn-primary pull-right" href="{{ route(Route::current()->getName()) == $full_url ? route(Route::current()->getName())."?" : $full_url.'&' }}type=print" id="btn-print"> <i class="fa fa-print"></i> Print </a>
                    <a class="btn btn-sm btn-success pull-right" href="{{ route(Route::current()->getName()) == $full_url ? route(Route::current()->getName())."?" : $full_url.'&' }}type=download" > <i class="fa fa-download"></i> Download </a>
                </div>

                <div class="box-body">

                    <div class="col-lg-12">
                        <table class="table table-striped" id="dataTable">

                            <tbody>
                            <tr>
                                <th>ID</th>
                                <th>Date</th>
                                <th>Supplier Name</th>
                                <th>Product Code</th>
                                <th>Product Name</th>
                                <th>Unit</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-right">Price</th>
                                <th class="text-right">Total Price</th>
                            </tr>
                            @php
                                $last_date = 0;
                                $purchase_total_price = $total_qty = 0;
                                $product_name = $last_unit = '';
                            @endphp
                            @foreach($purchases as $key => $value)
                                @if( !$loop->first && ($last_date!=0 ||  $last_date != db_date_month_year_format($value->purchases->date)) &&  $product_name!=$value->item->item_name)
                                    <tr class=" margin-bottom-20">
                                        <th colspan="6" class="text-right">Total</th>
                                        <th colspan="1" class="text-right">{{ $total_qty." ".$last_unit }}</th>
                                        <th colspan="2" class="text-right">{{ create_money_format($purchase_total_price) }}</th>
                                    </tr>
                                    @php
                                        $purchase_total_price = $total_qty = 0;
                                    @endphp
                                @endif
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ db_date_month_year_format($value->purchases->date) }}</td>
                                    <td>{{ $value->purchases->supplier->name }}</td>
                                    <td>{{ $value->item->productCode }}</td>
                                    <td>{{ $value->item->item_name }}</td>
                                    <td>{{ $value->unit }}</td>
                                    <td class="text-right padding-right-20">{{ $value->qty }}</td>
                                    <td class="text-right">{{ create_money_format($value->price) }}</td>
                                    <td class="text-right">{{ create_money_format($value->qty*$value->price) }}</td>
                                </tr>
                                @php
                                    $last_date = db_date_month_year_format($value->purchases->date);
                                    $purchase_total_price += $value->qty*$value->price;
                                    $product_name = $value->item->item_name;
                                    $total_qty += $value->qty;
                                    $last_unit = $value->unit;
                                @endphp

                                @if( $loop->last)
                                    <tr class=" margin-bottom-20">
                                        <th colspan="6" class="text-right">Total</th>
                                        <th colspan="1" class="text-right">{{ $total_qty." ".$last_unit }}</th>
                                        <th colspan="2" class="text-right">{{ create_money_format($purchase_total_price) }}</th>
                                    </tr>
                                    @php
                                        $purchase_total_price = 0;
                                    @endphp
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-12 col-sm-12 col-md-12 text-right">
                        {{ $purchases->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('scripts')

    <script src="{{ asset('public/adminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <script type="text/javascript">

        $(document).ready( function(){
            $('.select2').select2();
            $('.date').datepicker({
                "setDate": new Date(),
                "format": 'mm/dd/yyyy',
                "endDate": "+0d",
                "todayHighlight": true,
                "autoclose": true
            });


        });
    </script>
@endpush

