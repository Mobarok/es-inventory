<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 7/11/2019
 * Time: 2:01 PM
 */
?>

@include('member.reports.print_head')

<body>
<div id="page-wrap">

    @include('member.reports.company')

    <div style="width: 100%; display: flex; flex-wrap: nowrap;">
        <table class="table table-striped" id="dataTable">
            <tbody>

            <tr>
                <td> Total Purchase</td>
                <td class="text-right"> {{ create_money_format($total_purchase) }}</td>
            </tr>
            <tr>
                <td> Total Due Purchase (+)</td>
                <td class="text-right"> {{ create_money_format($total_due_purchase) }}</td>
            </tr>
            <tr>
                <td> Total Advance Purchase (-)</td>
                <td class="text-right"> {{ create_money_format($total_advance_purchase) }}</td>
            </tr>
            <tr >
                <th> Total</th>
                <th class="text-right" > @php $total = $total_purchase+$total_due_purchase-$total_advance_purchase @endphp {{ create_money_format($total) }}</th>
            </tr>
            <tr>
                <td> Total Sale (+)</td>
                <td class="text-right"> {{ create_money_format($total_sale) }}</td>
            </tr>
            <tr>
                <td> Total Due Sale (-)</td>
                <td class="text-right"> {{ create_money_format($total_due_sale) }}</td>
            </tr>
            <tr>
                <th> Total</th>
                <th class="text-right" > @php $sub_total = $total+$total_sale-$total_due_sale @endphp {{ create_money_format($sub_total) }}</th>
            </tr>
            <tr>
                <td> Total Due Collection (+)</td>
                <td class="text-right"> {{ create_money_format($total_due_collection_amount) }}</td>
            </tr>
            <tr>
                <th> Total</th>
                <th class="text-right" > {{ create_money_format($sub_total+$total_due_collection_amount) }}</th>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>


