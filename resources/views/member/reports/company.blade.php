<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 9/4/2019
 * Time: 4:23 PM
 */
?>

<div style="width: 100%;">
    <table style="margin-bottom: 10px; width: 100%;  margin-top: 5mm;">
        <tr>
            @if($company_logo)
                <th style="text-align: right; border: 0 !important;" width="30%">
                    <img height="100px" width="100px" src="{{ $company_logo }}" alt="{{ $company_name }}" id="logo"/>
                </th>
            @endif
            <td style="text-align: {{ $company_logo ? 'left; padding-left: 70px':  'center' }};  border: 0 !important;" width="{{ $company_logo ? 70 : 100 }}%" >
                <h3 style="margin-bottom: 5px;"> {{ $company_name }} </h3>
                <p> Address: {{ $company_address }}</p>
                <p> {{ $company_city.', '.$company_country }}</p>
                <p> Phone: {{ $company_phone }}</p>
            </td>
        </tr>
        <tr>
            <th colspan="{{$company_logo ? 2 :'' }}" style="border: 0 !important; padding-top: 5px; text-align: center;">
                <h2>{{ $report_title }}</h2>
            </th>
        </tr>
    </table>
</div>
