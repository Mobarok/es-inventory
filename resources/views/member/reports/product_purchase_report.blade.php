<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 7/18/2019
 * Time: 3:32 PM
 */


$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('member.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => 'Stocks',
        'href' => route('member.report.daily_stocks'),
    ],
    [
        'name' => 'Product Purchase Report',
    ],
];

$data['data'] = [
    'name' => 'Product Purchase Report',
    'title'=> 'Product Purchase Report',
    'heading' => 'Product Purchase Report',
];

?>
@extends('layouts.back-end.master', $data)

@push('styles')
    <link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/select2/dist/css/select2.min.css')}}">
@endpush

@section('contents')

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Search</h3>
                </div>

            {!! Form::open(['route' => 'member.report.product_purchase_report','method' => 'GET', 'role'=>'form' ]) !!}
            <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        @if(Auth::user()->hasRole(['super-admin', 'admin', 'developer']))
                            <div class="col-md-3">
                                <label>  Branch Name </label>
                                {!! Form::select('branch_id', $branches, null,['id'=>'branch_id','class'=>'form-control select2','placeholder'=>'Select All']); !!}
                            </div>
                        @endif
                        <div class="col-md-3">
                            <label>  Product Name </label>
                            {!! Form::select('item_id', $products, null,['id'=>'item_id','class'=>'form-control select2','placeholder'=>'Select All']); !!}
                        </div>

                        <div class="col-md-3">
                            <label> From Date </label>
                            <input class="form-control date" name="from_date" value="" autocomplete="off"/>
                        </div>
                        <div class="col-md-3">
                            <label> To Date</label>
                            <input class="form-control date" name="to_date" value="" autocomplete="off"/>
                        </div>

                        <div class="col-md-3 margin-top-23">
                            <label></label>
                            <input class="btn btn-info" value="Search" type="submit"/>
                            <a href="{{ route(Route::current()->getName()) }}" class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i> Reload</a>

                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                {{--<div class="box-body">--}}

                {{----}}
                {{--</div>--}}

                {!! Form::close() !!}
            </div>

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Product Purchase Report</h3>
                    <a href="{{ route(Route::current()->getName()) == $full_url ? route(Route::current()->getName())."?" : $full_url.'&' }}type=print" class="btn btn-sm btn-primary pull-right" id="btn-print"> <i class="fa fa-print"></i> Print </a>
                    <a href="{{ route(Route::current()->getName()) == $full_url ? route(Route::current()->getName())."?" : $full_url.'&' }}type=download" class="btn btn-sm btn-success pull-right"> <i class="fa fa-download"></i> Download </a>
                </div>

                <div class="box-body">

                    <div class="col-lg-12">
                        <table class="table table-striped" id="dataTable">

                            <tbody>
                            <tr>
                                <th>ID</th>
                                <th>Date</th>
                                <th>Product Code</th>
                                <th>Product Name</th>
                                <th>Unit</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-right">Price</th>
                                <th class="text-right">Total Price</th>
                            </tr>
                            @php
                                $last_date = 0;
                                $purchase_total_price = $total_qty = 0;
                            @endphp
                            @foreach($purchases as $key => $value)
                                @if($last_date!=0 && $last_date!=db_date_month_year_format($value->purchases->date))
                                    <tr class=" margin-bottom-20">
                                        <th colspan="5" class="text-right">Total</th>
                                        <th colspan="3" class="text-right">{{ create_money_format($purchase_total_price) }}</th>
                                    </tr>
                                    @php
                                        $purchase_total_price = 0;
                                    @endphp
                                @endif
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ db_date_month_year_format($value->purchases->date) }}</td>
                                    <td>{{ $value->item->productCode }}</td>
                                    <td>{{ $value->item->item_name }}</td>
                                    <td>{{ $value->unit }}</td>
                                    <td class="text-center">{{ $value->qty }}</td>
                                    <td class="text-right">{{ create_money_format($value->price) }}</td>
                                    <td class="text-right">{{ create_money_format($value->qty*$value->price) }}</td>
                                </tr>
                                @php
                                    $last_date = db_date_month_year_format($value->purchases->date);
                                    $purchase_total_price += $value->qty*$value->price;
                                    $total_qty += $value->qty;
                                @endphp

                                @if( $loop->last)
                                    <tr class=" margin-bottom-20">
                                        <th colspan="{{ $item ? 5 : 6 }}" class="text-right">Total</th>
                                        @if($item)<th colspan="1" class="text-right">{{ $total_qty." ".$value->unit }}</th>@endif
                                        <th colspan="3" class="text-right">{{ create_money_format($purchase_total_price) }}</th>
                                    </tr>
                                    @php
                                        $purchase_total_price = 0;
                                    @endphp
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-12 col-sm-12 col-md-12 text-right">
                        {{ $purchases->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('scripts')

    <script src="{{ asset('public/adminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <script type="text/javascript">

        $(document).ready( function(){
            $('.select2').select2();
            $('.date').datepicker({
                "setDate": new Date(),
                "format": 'mm/dd/yyyy',
                "endDate": "+0d",
                "todayHighlight": true,
                "autoclose": true
            });
        });
    </script>
@endpush
