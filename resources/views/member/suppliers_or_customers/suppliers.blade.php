<?php
/**
 * Created by PhpStorm.
 * User: R-Creation
 * Date: 3/13/2019
 * Time: 6:00 PM
 */



$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('admin.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => 'Suppliers',
        'href' => route('member.sharer.index'),
    ],
    [
        'name' => 'Index',
    ],
];

$data['data'] = [
    'name' => 'Suppliers',
    'title'=>'List Of Suppliers',
    'heading' => 'List Of Suppliers',
];

?>

@extends('layouts.back-end.master', $data)

@section('contents')


    <div class="row">
        <div class="col-xs-12">

            @include('common._alert')

            <div class="box">
                <div class="portlet light">

                    <div class="box-header">
                        <a href="{{ route('member.sharer.create', 'supplier') }}" class="btn btn-info"> <i class="fa fa-plus"> Add Supplier </i></a>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
                <div class="box-body">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <!-- BEGIN SAMPLE FORM PORTLET-->

                        <div class="table-responsive">
                            {!! $dataTable->table() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.1.0/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('public/vendor/datatables/buttons.server-side.js') }}"></script>
    {!! $dataTable->scripts() !!}
@endpush
