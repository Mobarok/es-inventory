<?php
/**
 * Created by PhpStorm.
 * {{ $sharer_type }}: R-Creation
 * Date: 2/27/2019
 * Time: 12:52 PM
 */

if( $sharer_type =='Supplier')
{
    $route =  'member.sharer.supplier_list';
}else{
    $route =  'member.sharer.customer_list';
}

$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('member.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => $sharer_type,
        'href' => route($route),
    ],
    [
        'name' => 'Create',
    ],
];

$data['data'] = [
    'name' => $sharer_type,
    'title'=>'Create '.$sharer_type,
    'heading' => 'Create '.$sharer_type,
];

?>
@extends('layouts.back-end.master', $data)

@section('contents')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">

        @include('common._alert')
        @include('common._error')


        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Create {{ $sharer_type }}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            {!! Form::open(['route' => 'member.sharer.store', 'method' => 'POST', 'role'=>'form' ]) !!}

                <div class="box-body">

                    @include('member.suppliers_or_customers._form')

                    <div class="box-footer">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->

            {!! Form::close() !!}
        <!-- /.box -->
    </div>
    </div>
</div>
@endsection
