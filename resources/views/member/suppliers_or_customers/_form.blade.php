<?php
/**
 * Created by PhpStorm.
 * User: R-Creation
 * Date: 2/27/2019
 * Time: 4:25 PM
 */

?>

@include('common._error')

<input type="hidden" value="{{ $sharer_type }}" name="customer_type"/>
    <div class="col-md-6">
        <div class="form-group">
            <label for="name">Full Name <span class="text-red"> * </span> </label>
            {!! Form::text('name',null,['id'=>'name','class'=>'form-control','placeholder'=>'Enter Full Name', 'required']); !!}
        </div>
        <div class="form-group">
            <label for="phone">Phone  <span class="text-red"> * </span> </label>
            {!! Form::text('phone',null,['id'=>'phone','class'=>'form-control input-number','placeholder'=>'Enter phone', 'required' ]); !!}
        </div>
        <div class="form-group">
            <label for="email">Email </label>
            {!! Form::email('email',null,['id'=>'email','class'=>'form-control','placeholder'=>'Enter Email']); !!}
        </div>
        <div class="form-group">
            <label for="address">Address  </label>
            {!! Form::textarea('address',null,['id'=>'address','class'=>'form-control','rows'=>'4', 'placeholder'=>'Enter address']); !!}
        </div>
        <div class="form-group">
            <label for="initial_balance">Initial Balance  </label>
            {!! Form::number('initial_balance',null,['id'=>'initial_balance','class'=>'form-control input-number' ,'placeholder'=>'Enter Initial Balance ', isset($model) ? "readonly" : '']); !!}
        </div>
        <div class="form-group">
            <?php if( isset($model)) { ?>
                {!! Form::checkbox('supplier', 1, null ,[ isset($model) && ($model->customer_type == 'both' || $model->customer_type == 'supplier') ? "checked='checked'" : '']) !!} {{ __('Also Save as Supplier') }} <br/>
                {!! Form::checkbox('customer', 1, null ,[ isset($model) && ($model->customer_type == 'both' || $model->customer_type == 'customer') ? "checked='checked'" : '']) !!} {{ __('Also Save as Customer') }}
            <?php }else{ ?>
                {!! Form::checkbox('both', 1, null ) !!} {{ __('Also Save as') }} {{ $sharer_type == "Supplier" ? "Customer" : "Supplier" }}
            <?php } ?>
        </div>
        <div class="form-group">
            {!! Form::checkbox('account_head', 1, null ) !!} {{ __('Create Account Head') }}  <br/>
            {!! Form::checkbox('cash_bank', 1, null ) !!} {{ __('Create Cash and Bank Account') }}
        </div>
{{--        <div class="form-group">--}}
{{--            <label for="balance_limit"> Balance Limit  </label>--}}
{{--            {!! Form::number('balance_limit',null,['id'=>'balance_limit','class'=>'form-control input-number' ,'placeholder'=>'Enter Balance Limit']); !!}--}}
{{--        </div>--}}
{{--        <div class="form-group">--}}
{{--            <label for="purchase_limit"> Purchase Limit  </label>--}}
{{--            {!! Form::number('purchase_limit',null,['id'=>'purchase_limit','class'=>'form-control input-number' ,'placeholder'=>'Enter Purchase Balance Limit']); !!}--}}
{{--        </div>--}}
{{--        <div class="form-group">--}}
{{--            <label for="sale_limit"> Sale Limit  </label>--}}
{{--            {!! Form::number('sale_limit',null,['id'=>'sale_limit','class'=>'form-control input-number' ,'placeholder'=>'Enter Sale Balance Limit']); !!}--}}
{{--        </div>--}}
        <div class="form-group">
            <label for="status">Status <span class="text-red"> * </span>  </label>
            {!! Form::select('status',['active'=>'Active', 'inactive'=>'Inactive'], null,['id'=>'status','class'=>'form-control', 'required' ]); !!}
        </div>

    </div>
