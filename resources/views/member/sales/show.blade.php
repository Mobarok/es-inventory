<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 5/25/2019
 * Time: 10:24 AM
 */

$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('member.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => 'sales',
        'href' => route('member.sales.index'),
    ],
    [
        'name' => 'Create',
    ],
];

$data['data'] = [
    'name' => 'Invoice',
    'title'=> 'Invoice',
    'heading' => 'Invoice',
];

?>
@extends('layouts.back-end.master', $data)


@section('contents')

    <div class="row text-right">
        <div class="col-md-12">
            <div class="box">
                @include('common._alert')
                <div class="box-body">
                    <a href="{{ route('member.sales.print_sale', $sales->id) }}" class="btn btn-xs btn-primary" id="btn-print"><i class="fa fa-print"></i> Print</a>

                    <a href="{{ route('member.sales.sales_return', $sales->id) }}" class="btn btn-xs btn-info">
                        <i class="fa fa-reply"></i> Sale Return
                    </a>

                    <a href="{{ route('member.sales.edit', $sales->id) }}" class="btn btn-xs btn-success">
                        <i class="fa fa-pencil"></i> Edit
                    </a>
                    <a href="{{ route('member.sales.create') }}" class="btn btn-xs btn-danger"><i class="fa fa-plus"></i> Add Sale</a>
{{--                    <a href="{{ route('member.sales.whole_sale_create') }}" class="btn btn-xs btn-warning"><i class="fa fa-plus"></i> Whole Sale</a>--}}

                </div>
            </div>
        </div>
    </div>

    <div class="row" id="custom-print">
        <div class="col-md-12">
            <div class="box">

                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                                <i class="fa fa-globe"></i> RC-Accounting
                                <small class="pull-right">Date: {{ $sales->date_format }}</small>
                            </h2>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- title row -->

                    <!-- info row -->
                    <div style="margin-bottom: 10px; " class="row invoice-info">

                        <div class="col-md-6 ">
                            <div style="border: 1px solid #d2d1d1; padding: 10px;" class="invoice-col">

                                @php print_r($sale_barcode) @endphp<br>
                                <b style="margin-left: 13px;">{{ $sales->sale_code }}</b><br>
                                <br>
                                <b>Account:</b> {{ $sales->cash_or_bank->title }}<br>
                                <b>Payment Method:</b> {{ $sales->payment_method->name }}<br>
                                <b>Delivery System:</b> {{ $sales->delivery_type->display_name }}
                            </div>
                        </div>
                        @if($sales->customer)
                            <div class="col-md-6 ">
                                <div style="border: 1px solid #d2d1d1; padding: 10px;" class="invoice-col">
                                    <h4>Customer Info:</h4>
                                    <b>Name: {{ $sales->customer->name }}</b><br>
                                    <b>Address:</b> {{ $sales->customer->address }}<br>
                                    <b>Phone:</b> {{ $sales->customer->phone }}
                                </div>
                            </div>
                        @endif
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-md-12 ">
                            <table style="width: 100%" class="sales_table">
                                <thead>
                                <tr>
                                    <th>SL. No</th>
                                    {{--<th>Item Code</th>--}}
                                    <th>Item Name </th>
                                    <th>Description</th>
                                    <th>Unit</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th class="text-center">Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $total = 0;
                                @endphp
                                @foreach( $sales->sale_details as $key=>$sale)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        {{--<td>{{ $sale->item }}</td>--}}
                                        <td>{{ $sale->item->item_name }}</td>
                                        <td>{{ $sale->description }}</td>
                                        <td>{{ $sale->unit }}</td>
                                        <td>{{ $sale->qty }}</td>
                                        <td>{{ $sale->total_price>0 ? create_float_format($sale->total_price/$sale->qty, 3) : create_money_format($sale->price) }}</td>
                                        <td class="text-right">{{ $sale->total_price>0 ? create_money_format($sale->total_price) : create_money_format($sale->qty*$sale->price) }}</td>
                                    </tr>

                                    @php
                                        $total += $sale->total_price;
                                    @endphp
                                @endforeach

                                <tr>
                                    <td colspan="4" rowspan="7">Notes: </td>
                                    <td class="text-right" colspan="2">Sub Total:</td>
                                    <td class="text-right" >{{ create_money_format($total) }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right" colspan="2">Discount {{ $sales->discount_type=="fixed" ? "(Fixed)" : "(".$sales->discount."%)" }} :</td>
                                    <td class="text-right">(-) {{ create_money_format($sales->total_discount) }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right" colspan="2">Shipping Charge:</td>
                                    <td class="text-right">{{ create_money_format($sales->shipping_charge) }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right" colspan="2">Total Amount:</td>
                                    <td class="text-right">{{ create_money_format($sales->grand_total) }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right" colspan="2"> Amount to Pay:</td>
                                    <td class="text-right">{{ create_money_format($sales->amount_to_pay) }}</td>
                                </tr>
                                <tr>
                                    <th class="text-right"colspan="2">Paid Amount:</th>
                                    <th class="text-right">{{ create_money_format($sales->paid_amount) }} </th>
                                </tr>
                                <tr>
                                    <th class="text-right" colspan="2">Due:</th>
                                    <th class="text-right"> {{ create_money_format($sales->due) }}</th>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

@endsection

