<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 5/22/2019
 * Time: 11:29 AM
 */

$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('member.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => 'sales',
        'href' => route('member.sales.index'),
    ],
    [
        'name' => 'Create',
    ],
];

$data['data'] = [
    'name' => 'Create sales',
    'title'=> 'Create sales',
    'heading' => 'Create Sale',
];

?>
@extends('layouts.back-end.master', $data)


@push('styles')

    <link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/select2/dist/css/select2.min.css')}}">

@endpush

@section('contents')

<div class="box box-default">

    @include('common._alert')

    <div class="box-header with-border">
        <h3 class="box-title">New Sales Order Entry</h3>
    </div>

    {!! Form::open(['route' => 'member.sales.store','method' => 'POST', 'files'=>true, 'id'=>'sale_form', 'role'=>'form' ]) !!}

    <div class="box-body">
        <div class="row">
            <div class="col-md-2 grid-width-20">
                <div class="form-group">
                    <label for="inputPassword" >Order Date</label>
                    {{--<div class="col-sm-8">--}}
                        <input type="text" id="date" class="form-control" name="date">
                    {{--</div>--}}
                </div>
            </div>

            <div class="col-md-2 grid-width-20">
                <div class="form-group ">
                    <label for="inputPassword" >Customer Name</label>
                    {{--<div class="col-sm-8">--}}
                        {!! Form::select('customer_id', $customers, null,['id'=>"customer_id", 'class'=>'form-control select2', 'placeholder'=>'Select Customer']); !!} </br>
                        <button class="btn btn-primary btn-xs" type="button" data-toggle="modal" data-target="#addCustomer"> <i class="fa fa-plus-circle"></i> Add Customer </button>
                    {{--</div>--}}
                </div>
            </div>

            <div class="col-md-2 grid-width-20">
                <div class="form-group ">
                    <label for="inputPassword" >Last Credit Amount</label>
                    {{--<div class="col-sm-8">--}}
                        <input type="text" class="form-control" id="last_credit" value="" readonly>
                    {{--</div>--}}
                </div>
                <!-- /.form-group -->
            </div>

            <div class="col-md-2 grid-width-20">
                <div class="form-group ">
                    <label for="inputPassword" >Membership Card No</label>
                    {{--<div class="col-sm-8">--}}
                        <input type="text" class="form-control" id="inputPassword" name="membership_card" placeholder="">
                </div>
            </div>


            <div class="col-md-2 grid-width-20">
                <div class="form-group ">
                    <label for="inputPassword" >Product Barcode Search</label>
                    {{--<div class="col-sm-9">--}}
                    <input type="text" id="barcode_search" class="form-control" name="barcode_search">
                    {{--</div>--}}
                </div>
            </div>
        </div>

            <div class="col-md-12">
                <div class="form-group" id="customer_details">

                </div>
            </div>

            {{--<div class="col-md-5 col-form-label">--}}
                {{--<button type="button" class="btn btn-primary">Product Search</button>--}}
            {{--</div>--}}
        </div>
        <!-- /.row -->
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-sx-12  new-table-responsive text-center">
                <h4>Sales Order Item</h4>

                <table class="sales_table" id="items">


                    <thead>
                    <tr>
                        <th>Item</th>
                        <th>Description</th>
                        <th>Available Stock</th>
                        <th>Last Sales Qty</th>
                        <th>Unit</th>
                        <th>Qty</th>
                        <th>Price Per Qty</th>
                        <th>Total Price</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="item-row">
                        <td class="item-name">
                            {!! Form::select('product_id[]', $products, null,['id'=>'product_id_0', 'data-option'=>'0', 'class'=>'form-control select2 item-name','required', 'placeholder'=>'Select Item Name']); !!}
                        </td>
                        <td class="description">
                            {!! Form::text('description[]',null,['class'=>'form-control']); !!}
                        </td>
                        <td>{!! Form::text('available_stock[]',null,['id'=>'stock_0','class'=>'form-control', 'readonly']); !!}</td>
                        <td>{!! Form::number('last_sale_qty[]',null,['id'=>'last_sale_qty_0','class'=>'form-control', 'readonly']); !!}</td>
                        <td>
                            {!! Form::text('unit[]',null,['id'=>'unit_0','class'=>'form-control', 'disabled']); !!}
                        </td>
                        <td>{!! Form::number('qty[]',null,['id'=>'qty_0','class'=>'form-control qty', 'required']); !!}</td>
                        <td>{!! Form::number('price[]',null,['id'=>'price_0','class'=>'form-control price',  'step'=>"any", 'required']); !!}</td>
                        <td>{!! Form::number('total_price[]',null,['id'=>'total_price_0','class'=>'form-control total_price', 'readonly']); !!}</td>
                        <td><a class="btn btn-success add-row pull-right margin-top-3" href="#" ><i class="fa fa-plus-circle"></i> </a></td>
                    </tr>
                </tbody>
            </table>

        </div>

    </div>

        <div style="margin-top: 20px; " class="row">
            <div  style="margin-bottom: 10px" class="col-lg-6 col-md-6 col-sm-12 col-sx-12 float-right amount-info">
                <table style="width: 100%" class="sales_table_2">
                    <tr>
                        <td  class="total-line">Sub Total</td>
                        <td  class="total-value text-right">
                            {!! Form::number('sub_total',0,['id'=>'sub_total','class'=>'form-control input-number', 'readonly']); !!}
                        </td>
                    </tr>
                    <tr>
                        <td  class="total-line">Shipping Charge</td>
                        <td  class="total-value text-right">
                            {!! Form::number('shipping_charge',0,['id'=>'shipping_charge','class'=>'form-control input-number']); !!}
                        </td>
                    </tr>
                    <tr>
                        <td class="total-line">Discount Type</td>
                        <td  class="total-value text-right">
                            {!! Form::select('discount_type', ['fixed'=>'Fixed', 'percentage'=>'Percentage'], null,['id'=>'discount_type','class'=>'form-control']); !!}
                        </td>
                    </tr>
                    <tr>
                        <td class="total-line">Discount</td>
                        <td  class="total-value text-right">
                            {!! Form::number('discount',0,['id'=>'discount', 'step'=>"any", 'class'=>'form-control input-number']); !!}
                        </td>
                    </tr>
                    <tr>
                        <td  class="total-line">Total Amount</td>
                        <td  class="total-value text-right">
                            {!! Form::number('total_amount',0,['id'=>'total_amount','class'=>'form-control input-number', 'step'=>"any", 'readonly']); !!}
                        </td>
                    </tr>
                    <tr>
                        <td  class="total-line">Amount to Pay</td>
                        <td  class="total-value text-right">
                            {!! Form::number('amount_to_pay',0,['id'=>'amount_to_pay','class'=>'form-control input-number', 'step'=>"any", 'readonly']); !!}
                        </td>
                    </tr>
                    <tr>
                        <td  class="total-line">Paid Amount</td>
                        <td  class="total-value text-right">
                            {!! Form::number('paid_amount',null,['id'=>'paid_amount','class'=>'form-control input-number',  'step'=>"any",'required']); !!}
                        </td>
                    </tr>
                    <tr>
                        <td  class="total-line">Due Amount</td>
                        <td  class="total-value text-right">
                            {!! Form::number('due',null,['id'=>'due_amount','class'=>'form-control input-number',  'step'=>"any", 'readonly']); !!}
                        </td>
                    </tr>

                </table>

            </div>
            <div  class="col-lg-6 col-md-6 col-sm-12 col-sx-12 float-right payment-info">
                <table style="width: 100%" class="sales_table_2">

                    <tr>
                        <td  class="total-line ">Account Name </td>
                        <td  class="total-value">
                            {!! Form::select('cash_or_bank_id', $banks, null,['class'=>'form-control select2 ','required']); !!}
                        </td>
                    </tr>
                    <tr>
                        <td   class="total-line ">Memo No </td>
                        <td  class="total-value">
                            {!! Form::text('memo_no',null,['id'=>'memo_no','class'=>'form-control']); !!}
                        </td>
                    </tr>
                    <tr>
                        <td  class="total-line ">Chalan  No </td>
                        <td  class="total-value">
                            {!! Form::text('chalan_no',null,['id'=>'chalan_no','class'=>'form-control']); !!}
                        </td>
                    </tr>
                    <tr>
                        <td   class="total-line ">Payment Method </td>
                        <td  class="total-value">
                            {!! Form::select('payment_method_id', $payment_methods, null,['class'=>'form-control select2 ','required']); !!}
                        </td>
                    </tr>
                    <tr>

                        <td class="total-line ">Delivery System </td>
                        <td  class="total-value">
                            {!! Form::select('delivery_type_id', $delivery_types, null,['class'=>'form-control select2 ','required']); !!}
                        </td>
                    </tr>
                    <tr>
                        <td  class="total-line " colspan="2">
                            <label> Shopping Bags</label><br/>
                            @foreach($bags as $value)
                                <div class="col-md-4" style="padding-left: 5px; padding-right: 5px;"><label id="bags_{{$value->id}}"> {{ $value->item_name }}</label>
                                    {!! Form::number("shopping_bags_".$value->id,null,['class'=>'form-control bags_qty','data-option'=>$value->id]); !!}</div>
                                @endforeach

                        </td>
                    </tr>
                    <tr>
                        <td  class="total-line">Comment </td>
                        <td  class="total-value">
                            {!! Form::text('notation',null,['id'=>'notation','class'=>'form-control']); !!}
                        </td>
                    </tr>
                </table>

            </div>

        </div>


        <div style="margin-top: 20px; margin-bottom: 20px" class="row pull-right">

        <div class="col-lg-12 col-md-12 ">
            <table class="new-table-3">
                <tr>
                    <td>
                        <button style="width: 100px" type="reset" class="btn btn-block btn-warning">Cancel</button>
                    </td>
                    <td>
                        <button style="width: 100px" type="submit" class="btn btn-block btn-primary">Save</button>
                    </td>
                </tr>
            </table>
        </div>
    </div>


    {!! Form::close() !!}


    </div>
</div>


@include('member.sales._model_add_customer')

@push('scripts')
    <script type="text/javascript">

        var editItem = '';
    </script>
    @include('member.sales.scripts')
@endpush


@endsection

