<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 7/7/2019
 * Time: 6:01 PM
 */


$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('admin.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => 'Sales',
        'href' => route('member.sales.index'),
    ],
    [
        'name' => 'Sales Due',
    ],
];

$data['data'] = [
    'name' => 'Sale Due',
    'title'=>'List Of Sale Due',
    'heading' => 'List Of Sale Due',
];

?>

@extends('layouts.back-end.master', $data)

@section('contents')


    <div class="row">
        <div class="col-xs-12">

            @include('common._alert')

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Sale Due List</h3>
                </div>

                <div class="box-body">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <!-- BEGIN SAMPLE FORM PORTLET-->

                        <div class="table-responsive">
                            <div class="col-lg-12">
                                <table class="table table-striped" id="dataTable">

                                    <tbody>
                                    <tr>
                                        <th> #SL</th>
                                        <th> Sale Code</th>
                                        <th> Total Amount</th>
                                        <th> Due Amount</th>
                                        <th> Manage</th>
                                    </tr>
                                    @foreach($modal as $key=>$value)
                                        <tr>
                                            <td> {{ $key+1 }} </td>
                                            <td> {{ $value->sale_code }} </td>
                                            <td> {{ $value->total_price }} </td>
                                            <td> {{ $value->due }} </td>
                                            <td>
                                                <a href="{{ route('member.sales.due_payment', $value->id) }}" class="btn btn-success btn-sm"> <i class="fa fa-money"></i> Pay Due</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-md-12 text-right">
                                {{ $modal->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

