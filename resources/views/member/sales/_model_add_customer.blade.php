<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 5/26/2019
 * Time: 10:49 AM
 */
?>

<!-- Modal -->
<div id="addCustomer" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Customer</h4>
            </div>
            <div class="modal-body">
                <div class="form-group ">
                    <label for="name">Customer Name:  <span class="text-red"> * </span> </label>
                    {!! Form::text('name', null,['id'=>'name','class'=>'form-control','placeholder'=>'Enter Name', 'required']); !!}
                </div>
                <div class="form-group">
                    <label for="phone">Phone Number: <span class="text-red"> * </span> </label><br/>
                    {!! Form::text('phone', null,['id'=>'phone','class'=>'form-control','placeholder'=>'Enter phone number', 'required']); !!}
                </div>
                <div class="form-group">
                    <label for="address">Address: <span class="text-red"> * </span> </label><br/>
                    {!! Form::text('address', null,['id'=>'address','class'=>'form-control','placeholder'=>'Enter Address', 'required']); !!}
                </div>
                <div class="form-group">
                    <label for="email">Email:  </label><br/>
                    {!! Form::text('email', null,['id'=>'phone','class'=>'form-control','placeholder'=>'Enter Email']); !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="save-customer"> Save </button>
            </div>
        </div>

    </div>
</div>
