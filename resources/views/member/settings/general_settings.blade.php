<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 8/8/2019
 * Time: 12:14 PM
 */



$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('member.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => 'General Settings',
        'href' => route('member.users.index'),
    ],
    [
        'name' => 'Create',
    ],
];

$data['data'] = [
    'name' => 'General Settings',
    'title'=>'General Settings',
    'heading' => 'General Settings',
];

?>
@extends('layouts.back-end.master', $data)

@push('styles')

    <link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/select2/dist/css/select2.min.css')}}">

@endpush

@section('contents')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">

        @include('common._alert')

        <!-- general form elements -->
            <div class="box box-primary">

                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
{{--                        <li><a href="#settings" data-toggle="tab">Settings</a></li>--}}
                        <li class="active"><a href="#print_page_system" data-toggle="tab">Print Page Setup</a></li>

                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="print_page_system">
                            @include('member.settings.print_page_system')
                        </div>
                    </div>
                </div>


            <!-- /.box -->
            </div>


        </div>
    </div>
@endsection

@push('scripts')

    <script src="{{ asset('public/adminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <script type="text/javascript">

        $(function () {
            $('.select2').select2()
        });

    </script>

@endpush
