<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 5/13/2019
 * Time: 4:16 PM
 */


$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('member.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => 'Purchases',
        'href' => route('member.purchase.index'),
    ],
    [
        'name' => $purchase['memo_no'],
    ],
];

$data['data'] = [
    'name' => 'Purchase Memo no: '.$purchase['memo_no'],
    'title'=>'Memo no: '.$purchase['memo_no'],
    'heading' => 'Purchase Memo No: '.$purchase['memo_no'],
];

?>


@extends('layouts.back-end.master', $data)

@section('contents')

    <div class="row text-right">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <a href="javascript:void(0)" class="btn btn-xs btn-primary" id="print"><i class="fa fa-print"></i> Print</a>

                    <a href="{{ route('member.purchase_return.edit', $purchase->id) }}" class="btn btn-xs btn-info">
                        <i class="fa fa-reply"></i> Purchase Return
                    </a>

                    <a href="{{ route('member.purchase.edit',  $purchase->id) }}" class="btn btn-xs btn-success">
                        <i class="fa fa-pencil"></i> Edit
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">

            @include('common._alert')

            <div class="box">

                <div class="box-body">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <!-- BEGIN SAMPLE FORM PORTLET-->

                        <div class="table-responsive" id="custom-print">
                            <table class="table table-responsive table-striped table-bordered ">
                                <thead class="text-center">
                                    <tr>
                                        <th colspan="6">Purchase Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th colspan="6" class="text-center">@php print_r($purchase_barcode) @endphp </th>
                                </tr>
                                    <tr>
                                        <th>Memo No</th>
                                        <td>{{ $purchase['memo_no'] }}</td>
                                        <th>Chalan</th>
                                        <td>{{ $purchase['chalan'] }}</td>
                                        <th>Date</th>
                                        <td>{{ $purchase['date_format'] }}</td>
                                    </tr>
                                    <tr>
                                        <th>Supplier</th>
                                        <td>{{ $purchase->supplier->name }}</td>
                                        <th>Account Name</th>
                                        <td>{{ $purchase->cash_or_bank->title }}</td>
                                        <th>Payment Method</th>
                                        <td>{{ $purchase->payment_method->name }}</td>
                                    </tr>

                                </tbody>
                            </table>
                            <table class="table table-responsive table-bordered margin-top-30 float-right" >
                                <tbody>
                                    <tr>
                                        <th>Item Name</th>
                                        <th>Description</th>
                                        <th>Unit</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th class="text-center">Total Price</th>
                                    </tr>
                                    @php $total = 0; @endphp
                                    @foreach($purchase->purchase_details as $value)
                                    <tr>
                                        <td>{{ $value->item->item_name }}</td>
                                        <td>{{ $value->description }}</td>
                                        <td>{{ $value->unit }}</td>
                                        <td> {{ $value->qty }}</td>
                                        <td> {{ create_money_format($value->price) }}</td>
                                        <td class="text-right" >{{ create_money_format($value->qty*$value->price) }}</td>
                                    </tr>
                                        @php
                                            $total += ($value->qty*$value->price);
                                        @endphp
                                    @endforeach
                                </tbody>

                            </table>
                            <table class="margin-top-30" style="width: 700px; float: left;">
                                <tr>
                                    <th> Notes: </th>
                                </tr>
                                <tr>
                                    <td>@php print_r($purchase->notation) @endphp
                                            <a href="{{ $purchase->file ? $purchase->attach_file_path : '#' }}" target="_blank" > Attached File</a>
                                    </td>
                                </tr>
                            </table>
                            <table class=" margin-top-30 pull-right" width="400px">

                                <tr>
                                    <th class="text-right" colspan="5"> Sub Total</th>
                                    <th class="text-right" > {{ create_money_format($total) }} </th>
                                </tr>
                                <tr>
                                    <td class="text-right" colspan="5">Transport Cost</td>
                                    <td class="text-right" > {{ create_money_format($purchase->transport_cost) }} </td>
                                </tr>
                                <tr>
                                    <td class="text-right" colspan="5">Unload Cost</td>
                                    <td class="text-right" > {{ create_money_format($purchase->unload_cost) }} </td>
                                </tr>
                                <tr>
                                    <th class="text-right" colspan="5">Grand Total</th>
                                    <th class="text-right" > {{ create_money_format($purchase->total_amount) }} </th>
                                </tr>
                                <tr>
                                    <td class="text-right" colspan="5">Discount ({{ $purchase->discount_type == "Percentage" ? $purchase->discount."%" : $purchase->discount_type }})</td>
                                    <td class="text-right" > {{ $purchase->total_discount > 0 ? "- (".create_money_format($purchase->total_discount).")" : create_money_format(0.00) }} </td>
                                </tr>
                                <tr>
                                    <td class="text-right" colspan="5">Amount To Pay</td>
                                    <td class="text-right" > {{ create_money_format($purchase->amt_to_pay) }} </td>
                                </tr>
                                <tr>
                                    <th class="text-right" colspan="5">Paid Amount</th>
                                    <th class="text-right" > {{ create_money_format($purchase->paid_amount) }} </th>
                                </tr>
                                <tr>
                                    <td class="text-right" colspan="5">Due Amount</td>
                                    <td class="text-right" > {{ create_money_format($purchase->due_amount) }} </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
