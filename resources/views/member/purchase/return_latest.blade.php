<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 5/29/2019
 * Time: 3:51 PM
 */


$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('member.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => 'Purchases',
        'href' => route('member.purchase.index'),
    ],
    [
        'name' => 'Return',
    ],
];

$data['data'] = [
    'name' => 'Purchase Return',
    'title'=> 'Purchase Return',
    'heading' => 'Purchase Return',
];

?>
@extends('layouts.back-end.master', $data)


@push('styles')

    <link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/select2/dist/css/select2.min.css')}}">

@endpush

@section('contents')

    <div class="box box-default">

        @include('common._alert')

        <div class="box-header with-border">
            <h3 class="box-title">Return Purchase</h3>
        </div>

        {!! Form::model($modal, ['route' => ['member.purchase_return.update', $modal],  'method' => 'put', 'role'=>'form', 'files'=>true,'id'=>'update']) !!}

        <div class="box-body">
            <div class="row">
                <div class="box">
                    <div class="box-body">
                        <div class="col-md-2">
                            <label for="suppliers"> Date </label>
                            {!! Form::text('date', month_date_year_format($modal->date),['id'=>'date', 'class'=>'form-control','required', 'readonly']); !!}
                        </div>
                        <div class="col-md-3">
                            <label for="supplier_id"> Supplier Name </label>
                            {!! Form::select('supplier_id', $suppliers, null,['id'=>'supplier_id', 'class'=>'form-control select2','required', 'placeholder'=>'Select Supplier Name', 'readonly']); !!}
                            <div id="current-balance">  </div>
                        </div>
                        <div class="col-md-2">
                            <label for="memo_no"> Memo No. </label>
                            {!! Form::text('memo_no', $memo_no, [ 'class'=>'form-control','required', 'readonly']); !!}
                        </div>
                        <div class="col-md-2">
                            <label for="chalan_no"> Chalan No. </label>
                            {!! Form::text('chalan', $chalan_no, ['class'=>'form-control','required', 'readonly']); !!}
                        </div>
                        <div class="col-md-3">
                            <label for="invoice_no"> Invoice No. </label>
                            {!! Form::text('invoice_no', null, ['class'=>'form-control']); !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 col-sx-12  new-table-responsive text-center">
                    <h4>Purchase Order Item</h4>

                    <table class="sales_table" id="items">


                        <thead>
                        <tr>
                            <th>Item</th>
                            <th>Description</th>
                            <th>Available Stock</th>
                            <th>Last Purchase Qty</th>
                            <th>Unit</th>
                            <th>Purchase Qty</th>
                            <th>Purchase Price</th>
                            <th>Purchase Total Price</th>
                            <th>Return Qty</th>
                            <th>Return Price</th>
                            <th>Return Total Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($modal->purchase_details as $value)
                            <tr class="item-row">
                                <td class="item-name">
                                    <input type="hidden" value="{{ $value->id }}" name="Purchase_details_id[]">
                                    {!! Form::select('product_id[]', $products, $value->item_id,['id'=>'product_id_0', 'data-option'=>'0', 'class'=>'form-control select2 item-name','required', 'placeholder'=>'Select Item Name', "readonly"]); !!}
                                </td>
                                <td class="description" width="100px">
                                    {!! Form::text('description[]',$value->description,['class'=>'form-control']); !!}
                                </td>
                                <td>{!! Form::text('available_stock[]',$value->available_stock,['id'=>'stock_0','class'=>'form-control', 'readonly']); !!}</td>
                                <td>{!! Form::number('last_Purchase_qty[]',$value->last_Purchase_qty,['id'=>'last_Purchase_qty_0','class'=>'form-control', 'readonly']); !!}</td>
                                <td>
                                    {!! Form::text('unit[]',$value->unit,['id'=>'unit_0','class'=>'form-control', 'disabled']); !!}
                                </td>
                                <td>{!! Form::number('qty[]',$value->qty,['id'=>'qty_0','class'=>'form-control qty', 'required', "readonly"]); !!}</td>
                                <td>{!! Form::number('price[]',$value->price,['id'=>'price_0','class'=>'form-control price', 'readonly', 'required']); !!}</td>
                                <td>{!! Form::number('total_price[]',$value->price*$value->qty,['id'=>'total_price_0','class'=>'form-control total_price', 'readonly']); !!}</td>
                                <td>{!! Form::number('return_qty[]',null,['id'=>'return_qty_0','class'=>'form-control qty']); !!}</td>
                                <td>{!! Form::number('return_price[]',null,['id'=>'return_price_0','class'=>'form-control price']); !!}</td>
                                <td>{!! Form::number('return_total_price[]', null,['id'=>'return_total_price_0','class'=>'form-control total_price', 'readonly']); !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>

            </div>

            <div style="margin-top: 20px; " class="row">
                <div  style="margin-bottom: 10px" class="col-lg-6 col-md-6 col-sm-12 col-sx-12 float-right amount-info">
                    <table style="width: 100%" class="sales_table_2">
                        <tr>
                            <td  class="total-line">Amount to Pay</td>
                            <td  class="total-value text-right">
                                {!! Form::number('amt_to_pay',null,['id'=>'amount_to_pay','class'=>'form-control input-number', 'readonly']); !!}
                            </td>
                        </tr>
                        <tr>
                            <td  class="total-line">Paid Amount</td>
                            <td  class="total-value text-right">
                                {!! Form::number('paid_amount',null,['id'=>'paid_amount','class'=>'form-control input-number', 'required', 'readonly']); !!}
                            </td>
                        </tr>
                        <tr>
                            <td  class="total-line">Due Amount</td>
                            <td  class="total-value text-right">
                                {!! Form::number('due_amount',null,['id'=>'due_amount','class'=>'form-control input-number', 'readonly']); !!}
                            </td>
                        </tr>
                        <tr>
                            <td  class="total-line">Return Amount</td>
                            <td  class="total-value text-right">
                                {!! Form::number('return_amount',null,['id'=>'return_amount','class'=>'form-control input-number text-bold', 'required', 'readonly']); !!}
                            </td>
                        </tr>

                    </table>

                </div>
                <div  class="col-lg-6 col-md-6 col-sm-12 col-sx-12 float-right payment-info">
                    <table style="width: 100%" class="sales_table_2">

                        <tr>
                            <td  class="total-line ">Account Name </td>
                            <td  class="total-value">
                                {!! Form::select('cash_or_bank_id', $banks, null,['class'=>'form-control select2 ','required','readonly']); !!}
                            </td>
                        </tr>
                        <tr>
                            <td  class="total-line">Comment </td>
                            <td  class="total-value">
                                {!! Form::text('notation',null,['id'=>'notation','class'=>'form-control']); !!}
                            </td>
                        </tr>
                    </table>

                </div>

            </div>


            <div style="margin-top: 20px; margin-bottom: 20px" class="row pull-right">

                <div class="col-lg-12 col-md-12 ">
                    <table class="new-table-3">
                        <tr>
                            <td>
                                <button style="width: 100px" type="submit" class="btn btn-block btn-primary">Save Return</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>


            {!! Form::close() !!}


        </div>
    </div>

    @push('scripts')

        <script src="{{ asset('public/adminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
        <script type="text/javascript">

            $(function () {
                $('#date').datepicker({
                    "setDate": new Date(),
                    "format": 'mm/dd/yyyy',
                    "endDate": "+0d",
                    "todayHighlight": true,
                    "autoclose": true
                });
                var today = moment().format('MM\DD\YYYY');
                $('#date').datepicker('setDate', today);
                $('#date').datepicker('update');
                $('.date').datepicker('setDate', today);

                $('.select2').select2();

            });
            $(document).on('keyup','.qty, .price', function(e) {
                e.preventDefault();
                price_calculate();
            });

            $(document).on('keyup','.qty', function(e) {
                e.preventDefault();

                var $div = $(this).parent().parent();
                var stock = $div.find('td:nth-child(6)').find('input').val();
                if(parseInt(stock)<$(this).val())
                {
                    $(this).val('');
                    bootbox.alert("Purchases return quantity can't cross Purchase qty");
                    return false;
                }
            });

            $(document).on('keyup','.price', function(e) {
                e.preventDefault();

                var $div = $(this).parent().parent();
                var price = $div.find('td:nth-child(7)').find('input').val();
                if(parseInt(price)<$(this).val())
                {
                    $(this).val('');
                    $div.find('td:nth-child(11)').find('input').val('');
                    bootbox.alert("Purchases return price can't cross Purchase price");
                    return false;
                }
            });

            function price_calculate(){

                var $tr = $('.sales_table tbody');
                var total_bill = 0;
                for(var i = 1; i<=$tr.find('tr').length; i++) {
                    var qty = $tr.find('tr:nth-child('+i+') td:nth-child(9) input').val();
                    var price = $tr.find('tr:nth-child('+i+') td:nth-child(10) input').val();
                    qty =  qty == undefined || qty == "" ? 0 : parseInt(qty);
                    price =  price == undefined || price == "" ? 0 : parseFloat(price);
                    var total_price = parseFloat(qty*price);
                    total_bill = total_bill+total_price;
                    $tr.find('tr:nth-child('+i+') td:nth-child(11) input').val(total_price);
                }
                $('#return_amount').val(total_bill);
            }

        </script>
    @endpush


@endsection

