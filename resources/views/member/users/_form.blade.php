<?php
/**
 * Created by PhpStorm.
 * User: R-Creation
 * Date: 2/27/2019
 * Time: 4:25 PM
 */

?>

@include('common._error')

    <div class="col-md-7">
        <div class="form-group">
            <label for="full_name">Full Name <span class="text-red"> * </span> </label>
            {!! Form::text('full_name',null,['id'=>'full_name','class'=>'form-control','placeholder'=>'Enter Full Name', 'required']); !!}
        </div>
        <div class="form-group">
            <label for="email">Email <span class="text-red"> * </span> </label>
            {!! Form::text('email',null,['id'=>'email','class'=>'form-control','placeholder'=>'Enter Email', 'required']); !!}
        </div>
        <div class="form-group">
            <label for="phone">Phone  </label>
            {!! Form::text('phone',null,['id'=>'phone','class'=>'form-control input-number','placeholder'=>'Enter phone']); !!}
        </div>

    </div>
    <div class="col-md-12">
        <div class="form-group ">
            <label for="role">{{ isset($assignRole)?'Assigned ':'' }} Roles  </label><br/>

            @foreach( $roles as $key => $role)
                <div class="col-md-3">
                    @if(isset($assignRole))
                        {!! Form::checkbox('roles[]', $key, null, ['checked' => in_array($key, $assignRole )] ) !!} {{ $role }}
                    @else
                        {!! Form::checkbox('roles[]', $key, null ) !!} {{ $role }}
                    @endif
                </div>
            @endforeach
        </div><br/>
    </div>

