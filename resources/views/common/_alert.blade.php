<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 2/28/2019
 * Time: 2:12 PM
 */
?>
@if (session('status'))
    <div class="alert alert-{{ session('status')['type'] }} alert-out text-left">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> {{ ucfirst(session('status')['type']) }}</h4>
        {{ session('status')['message'] }}
    </div>

@endif

{{--@if(\Illuminate\Support\Facades\Session::get('dg-message'))--}}

{{--    <div class="alert alert-danger alert-out text-left">--}}
{{--        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>--}}
{{--        <h4><i class="icon fa fa-check"></i> Un-successfull </h4>--}}
{{--        {{ session('dg-message') }}--}}
{{--    </div>--}}
{{--@endif--}}

<?php
//\Illuminate\Support\Facades\Session::forget('status');

