<?php
/**
 * Created by PhpStorm.
 * User: R-Creation
 * Date: 2/28/2019
 * Time: 12:42 PM
 */
?>

{{--<a href="javascript:void(0);" class="btn btn-xs btn-info ajax-show" data-target="{{ route($route . '.show', $model->id) }}">--}}
    {{--<i class="fa fa-info-circle"></i>--}}
{{--</a>--}}

<a href="{{ route($route . '.edit', $model->id) }}" class="btn btn-xs btn-success">
    <i class="fa fa-pencil"></i>
</a>

<a href="javascript:void(0);" class="btn btn-xs btn-danger delete-confirm" data-target="{{ route($route . '.destroy', $model->id) }}">
    <i class="fa fa-times"></i>
</a>
