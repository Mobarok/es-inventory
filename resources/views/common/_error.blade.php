<?php
/**
 * Created by PhpStorm.
 * User: R-Creation
 * Date: 3/11/2019
 * Time: 10:49 AM
 */
?>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
