<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 9/19/2019
 * Time: 12:43 PM
 */
?>


<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">


<div style=" text-align:left; width: 700px; min-height: 500px; background-repeat: no-repeat; background-size: 100%;">
    <div style="color: #000; padding: 100px 40px 70px; font-family: Roboto;">
        <h1>{{  human_words(config('app.name')) }}</h1>
        <strong>Dear {{$name}},</strong>
        <p> Your {{$report_title}} is Ready. Please check our attachment file. </p>
        <p>Regards</p>
        <p>Support Center</p>
        <br/>
        <samp>*Note: This is auto generated mail. Don't Reply to this email.</samp>
    </div>
</div>

