<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 5/14/2019
 * Time: 1:15 PM
 */

?>

<a href="{{ route($route . '.show', $model->id) }}" class="btn btn-xs btn-info ">
    <i class="fa fa-info-circle"></i>
</a>

<a href="{{ route($route . '.edit', $model->id) }}" class="btn btn-xs btn-success">
    <i class="fa fa-pencil"></i>
</a>

{{--<a href="javascript:void(0);" class="btn btn-xs btn-danger delete-confirm" data-target="{{ route($route . '.destroy', $model->id) }}">--}}
    {{--<i class="fa fa-times"></i>--}}
{{--</a>--}}
