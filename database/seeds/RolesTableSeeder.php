<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'Super Admin' => 'super-admin',
            'Admin' => 'admin',
            'Accountant' => 'accountant',
            'User' => 'user',
        ];

        foreach ($roles as $key => $role){

            $data = array();
            $data['name'] = $role;
            $data['display_name'] = $key;

            $saveRole = \App\Models\Role::create($data);
            $saveRole->save();
        }
    }
}
