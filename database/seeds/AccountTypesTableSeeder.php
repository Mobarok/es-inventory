<?php

use Illuminate\Database\Seeder;
use App\Models\AccountType;

class AccountTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        AccountType::truncate();

        $array = [
//            'Assets' => Null,
//            'Current Assets' => 1,
            'Bank' => 2,
            'Cash' => 2,
//            'Accounts Receivable' => 2,
//            'Inventory' => 2,
//            'Fixed Assets' => 1,
//            'Other Assets' => 1,
//            'Liabilities' => Null,
//            'Current Liabilities' => 9,
//            'Credit Accounts' => 10,
//            'Accounts Payable' => 10,
//            'Long-term Liabilities' => 9,
//            'Equity' => Null,
//            'Invested Capital' => 14,
//            'Owner Distribution' => 14,
//            'Retained Earnings' => 14,
            'Sales' => Null,
//            'Other Revenue' => Null,
//            'Interest Income' => 19,
//            'Cost of Goods Sold' => Null,
//            'Expenses' => Null,
//            'Advertising' => 22,
//            'Software & Subscriptions' => 22,
//            'Office Expenses' => 22,
//            'Insurance' => 22,
//            'Accounting Services' => 22,
//            'Meals & Entertainment' => 22,
//            'Travel' => 22,
//            'Depreciation Expense' => 22,
//            'Other Expenses' => Null,
//            'Interest Expense' => 31,
//            'In Transit' => 2,
//            'Ask My Accountant' => Null,
            'Purchase' => Null

        ];

        foreach ($array as $key => $item) {
            $accountType = AccountType::create([
                'display_name' => $key,
                'name' => snake_case($key),
                'parent_id' => $item,
                'member_id' => 1
            ]);

            $accountType->save();
        }
    }
}
