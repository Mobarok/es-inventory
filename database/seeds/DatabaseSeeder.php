<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         // All Table Truncate Before DB Seed
         $this->call(AllTableTruncateSeeder::class);

         // Start Seeding
         $this->call(MembershipTableSeeder::class);
         $this->call(CountryTableSeeder::class);
         $this->call(MemberTableSeeder::class);
         $this->call(RolesTableSeeder::class);
         $this->call(PermissionsTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(PaymentMethodTableSeeder::class);
         $this->call(TransactionCategoriesTableSeeder::class);
         $this->call(AccountTypesTableSeeder::class);
         $this->call(CompanyTableSeeder::class);
         $this->call(CashAndBankTableSeeder::class);
         $this->call(SharerTableSeeder::class);
         $this->call(FiscalYearTableSeeder::class);
         $this->call(SettingsTableSeeder::class);
         $this->call(DeliverySystemTableSeeder::class);
    }
}
