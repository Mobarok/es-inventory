<?php

use Illuminate\Database\Seeder;
use App\Models\Membership;

class MembershipTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $membership = Membership::create([
           'name' => snake_case('Main'),
           'display_text' => 'Main',
           'description' => 'Company Main Control',
           'price' => 0,
           'time_period' => 0,
        ]);

        $membership->save();
    }
}
