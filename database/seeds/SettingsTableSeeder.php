<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Company;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Save users company
        $user  = User::where('id', 1)->first();
        $user->company_id = 1;
        $user->save();
    }
}
