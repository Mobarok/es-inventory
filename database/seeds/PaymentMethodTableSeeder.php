<?php


use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;

class PaymentMethodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $array = [
//            'Cash', 'Paypal', 'Check', 'ATM Withdrawal', 'Credit Card', 'Debit', 'BKash', 'Internet Banking', 'Master Card'
//        ];

        $array = [
            'Cash', 'Check','BKash'
        ];

        foreach ($array as $key => $item) {
            $payment_method = PaymentMethod::create([
                'name' => $item,
                'short_name' => strtolower(str_replace(' ','_', $item)),
                'status' => 'active'
            ]);


            $payment_method->save();
        }
    }
}
